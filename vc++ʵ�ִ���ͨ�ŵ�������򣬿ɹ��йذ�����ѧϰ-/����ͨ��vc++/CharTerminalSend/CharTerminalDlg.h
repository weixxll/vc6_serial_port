// CharTerminalDlg.h : header file
#include "mscomm.h"

#if !defined(AFX_CHARTERMINALDLG_H__8038144A_1476_4E76_8806_B38F94BA4EBC__INCLUDED_)
#define AFX_CHARTERMINALDLG_H__8038144A_1476_4E76_8806_B38F94BA4EBC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CCharTerminalDlg dialog

class CCharTerminalDlg : public CDialog
{
// Construction
protected:
	CMSComm m_Com;

public:
	CCharTerminalDlg(CWnd* pParent = NULL);	// standard constructor
	void CCharTerminalDlg::OnCommSend();

// Dialog Data
	//{{AFX_DATA(CCharTerminalDlg)
	enum { IDD = IDD_CHARTERMINAL_DIALOG };
	CString	m_strSend;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCharTerminalDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	afx_msg void OnCommMscomm();
    DECLARE_EVENTSINK_MAP()

protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CCharTerminalDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSend1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHARTERMINALDLG_H__8038144A_1476_4E76_8806_B38F94BA4EBC__INCLUDED_)
