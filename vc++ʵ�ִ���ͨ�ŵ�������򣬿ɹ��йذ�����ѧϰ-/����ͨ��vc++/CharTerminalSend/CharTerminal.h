// CharTerminal.h : main header file for the CHARTERMINAL application
//

#if !defined(AFX_CHARTERMINAL_H__FA760C7F_B789_4EE2_96BC_06811102539A__INCLUDED_)
#define AFX_CHARTERMINAL_H__FA760C7F_B789_4EE2_96BC_06811102539A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCharTerminalApp:
// See CharTerminal.cpp for the implementation of this class
//

class CCharTerminalApp : public CWinApp
{
public:
	CCharTerminalApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCharTerminalApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCharTerminalApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHARTERMINAL_H__FA760C7F_B789_4EE2_96BC_06811102539A__INCLUDED_)
