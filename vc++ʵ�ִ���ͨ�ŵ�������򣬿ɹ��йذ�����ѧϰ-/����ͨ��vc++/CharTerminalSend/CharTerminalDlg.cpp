// CharTerminalDlg.cpp : implementation file
//

#include "stdafx.h"
#include "mscomm.h"
#include "CharTerminal.h"
#include "CharTerminalDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCharTerminalDlg dialog

CCharTerminalDlg::CCharTerminalDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCharTerminalDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCharTerminalDlg)
	m_strSend = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCharTerminalDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCharTerminalDlg)
	DDX_Text(pDX, IDC_SEND, m_strSend);
	DDV_MaxChars(pDX, m_strSend, 100);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCharTerminalDlg, CDialog)
	//{{AFX_MSG_MAP(CCharTerminalDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_SEND1, OnSend1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CCharTerminalDlg, CDialog)
	ON_EVENT(CCharTerminalDlg, ID_COMMCTRL, 1, OnCommMscomm, VTS_NONE)
END_EVENTSINK_MAP()


/////////////////////////////////////////////////////////////////////////////
// CCharTerminalDlg message handlers

BOOL CCharTerminalDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	DWORD style=WS_VISIBLE|WS_CHILD;
	if (!m_Com.Create(NULL,style,CRect(0,0,0,0),this,ID_COMMCTRL))
	{
		TRACE0("Failed to create OLE Communications Control\n");
	    return -1; //fail to create　　
	}

	m_Com.SetCommPort(2); //选择COM1
	m_Com.SetInBufferSize(1024); //设置输入缓冲区的大小，Bytes
	m_Com.SetOutBufferSize(512); //设置输入缓冲区的大小，Bytes

	if(!m_Com.GetPortOpen()) //打开串口   
	m_Com.SetPortOpen(TRUE);
	m_Com.SetInputMode(1); //设置输入方式为二进制方式
	m_Com.SetSettings("9600,n,8,1"); //设置波特率等参数
	m_Com.SetRThreshold(1); //为1表示有一个字符引发一个事件　　　　
	m_Com.SetInputLen(0);

	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCharTerminalDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCharTerminalDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCharTerminalDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


void CCharTerminalDlg::OnCommMscomm()
{
    VARIANT vResponse;
    int k;
    if(m_Com.GetCommEvent()==2) 
    {
		k=m_Com.GetInBufferCount(); //接收到的字符数目
		if(k > 0)
		{
			vResponse=m_Com.GetInput(); //read
			//对数据进行其他处理 
 //           m_strReceive = (const char *)(unsigned char*) vResponse.parray->pvData;
		}    
		// 接收到字符，MSComm控件发送事件
    }
    // 处理其他MSComm控件
}
void CCharTerminalDlg::OnCommSend() 
{
   
}


void CCharTerminalDlg::OnSend1() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	UpdateData(FALSE);

	char TxData[100];
    int Count = m_strSend.GetLength();

	for(int i = 0; i < Count; i++)
		TxData[i] = m_strSend.GetAt(i);


    CByteArray array;     
    array.RemoveAll();
    array.SetSize(Count);

    for(i=0;i<Count;i++)
	    array.SetAt(i, TxData[i]);

    m_Com.SetOutput(COleVariant(array)); // 发送数据	
}
