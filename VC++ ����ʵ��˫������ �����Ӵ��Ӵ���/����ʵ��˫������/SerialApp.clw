; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CConfigDlg
LastTemplate=CRichEditCtrl
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SerialApp.h"

ClassCount=7
Class1=CSerialAppApp
Class2=CSerialAppDlg
Class3=CAboutDlg

ResourceCount=5
Resource1=IDD_ABOUTBOX
Resource2=IDD_SERIALAPP_DIALOG
Class4=CConfigDlg
Resource4=IDD_CONFIGDIALOG
Class5=CExpandingDialog
Class6=CInputWnd
Class7=CShowWnd
Resource5=IDR_MAINFRAME

[CLS:CSerialAppApp]
Type=0
HeaderFile=SerialApp.h
ImplementationFile=SerialApp.cpp
Filter=N
LastObject=CSerialAppApp

[CLS:CAboutDlg]
Type=0
HeaderFile=SerialAppDlg.h
ImplementationFile=SerialAppDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_SERIALAPP_DIALOG]
Type=1
Class=CSerialAppDlg
ControlCount=17
Control1=IDC_DETAILS,button,1342242816
Control2=IDC_MSG,RICHEDIT,1352728580
Control3=IDC_BTNSEND,button,1342242816
Control4=IDC_SENDFILE_RADIO,button,1342177289
Control5=IDC_RECFILE_RADIO,button,1342177289
Control6=IDC_EDIT_SENDFILE,edit,1350631552
Control7=IDC_EDIT_RECFILE,edit,1350631552
Control8=IDC_SENDFILECHOOSE,button,1342242816
Control9=IDC_RECFILECHOOSE,button,1342242816
Control10=IDC_FILESTART,button,1342242816
Control11=IDC_CHAT,RICHEDIT,1352730628
Control12=IDC_DEFAULTBOX,static,1073745927
Control13=IDC_PROGRESS,msctls_progress32,1350565888
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,button,1342177287
Control16=IDC_STATIC,button,1342177287
Control17=IDC_RIGHT,button,1342177287

[DLG:IDD_CONFIGDIALOG]
Type=1
Class=CConfigDlg
ControlCount=15
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,button,1342177287
Control4=IDC_STATIC,static,1342308354
Control5=IDC_STATIC,static,1342308354
Control6=IDC_STATIC,static,1342308354
Control7=IDC_STATIC,static,1342308354
Control8=IDC_STATIC,static,1342308354
Control9=IDC_BAUDRATECOMBO,combobox,1344341123
Control10=IDC_PARITYCOMBO,combobox,1344348163
Control11=IDC_DATABITSCOMBO,combobox,1344339971
Control12=IDC_STOPBITSCOMBO,combobox,1344341251
Control13=IDC_SENDBUFFERCOMBO,combobox,1344339970
Control14=IDC_STATIC,static,1342308352
Control15=IDC_COMMCOMBO,combobox,1344339971

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_CONNECT
Command2=ID_DISCONNECT
Command3=ID_CONFIG
CommandCount=3

[CLS:CConfigDlg]
Type=0
HeaderFile=ConfigDlg.h
ImplementationFile=ConfigDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=IDOK
VirtualFilter=dWC

[CLS:CSerialAppDlg]
Type=0
HeaderFile=SerialAppDlg.h
ImplementationFile=SerialAppDlg.cpp
BaseClass=CExpandingDialog
Filter=D
VirtualFilter=dWC
LastObject=CSerialAppDlg

[CLS:CExpandingDialog]
Type=0
HeaderFile=ExpandingDialog.h
ImplementationFile=ExpandingDialog.cpp
BaseClass=CDialog
Filter=D

[CLS:CInputWnd]
Type=0
HeaderFile=InputWnd.h
ImplementationFile=InputWnd.cpp
BaseClass=CRichEditCtrl
Filter=W
LastObject=CInputWnd

[CLS:CShowWnd]
Type=0
HeaderFile=ShowWnd.h
ImplementationFile=ShowWnd.cpp
BaseClass=CRichEditCtrl
Filter=W
LastObject=CShowWnd

