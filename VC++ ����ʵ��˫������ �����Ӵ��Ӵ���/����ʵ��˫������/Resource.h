//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SerialApp.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SerialAppDlg_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDD_SERIALAPP_DIALOG            129
#define IDD_CONFIGDIALOG                132
#define IDC_COMMCOMBO                   1003
#define IDC_EDITMSG                     1004
#define IDC_SENDMSG                     1006
#define IDC_SENDFILE_RADIO              1011
#define IDC_PROGRESS1                   1012
#define IDC_PROGRESS                    1012
#define IDC_EDIT1                       1013
#define IDC_EDIT_SENDFILE               1013
#define IDC_FILECHOOSE                  1014
#define IDC_SENDFILECHOOSE              1014
#define IDC_SENDFILE_RADIO2             1015
#define IDC_RECFILE_RADIO               1015
#define IDC_FILERCV                     1016
#define IDC_EDIT_RECFILE                1016
#define IDC_FILECHOOSE2                 1017
#define IDC_RECFILECHOOSE               1017
#define IDC_FILESTARTSEND               1018
#define IDC_FILESTART                   1018
#define IDC_RIGHT                       1019
#define IDC_FILESTARTREC                1020
#define IDC_CONFIGBUTTON1               1022
#define IDC_CONFIGBUTTON2               1023
#define IDC_CONFIGBUTTON3               1024
#define IDC_CONFIGBUTTON4               1025
#define IDC_BAUDRATECOMBO               1026
#define IDC_PARITYCOMBO                 1027
#define IDC_DATABITSCOMBO               1028
#define IDC_STOPBITSCOMBO               1029
#define IDC_EVENTSCOMBO                 1030
#define IDC_SENDBUFFEREDIT              1031
#define IDC_SENDBUFFERCOMBO             1043
#define IDC_BTNSEND                     1050
#define IDC_LIST                        1051
#define IDC_CHAT                        1052
#define IDC_MSG                         1053
#define IDC_DETAILS                     1054
#define IDC_DEFAULTBOX                  1055
#define ID_SEND                         32571
#define ID_RECV                         32572
#define ID_SETTING                      32573
#define ID_CONFIG                       32574
#define ID_CONNECT                      32575
#define ID_DISCONNECT                   32576
#define ID_CHAT                         32577

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
