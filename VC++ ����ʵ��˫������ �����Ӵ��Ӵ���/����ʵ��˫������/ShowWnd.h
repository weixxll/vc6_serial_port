#if !defined(AFX_SHOWWND_H__753103D2_AD6C_443C_92E2_567905DDC8E4__INCLUDED_)
#define AFX_SSHOWWND_H__753103D2_AD6C_443C_92E2_567905DDC8E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ShowWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CShowWnd window

class CSerialAppDlg;

class CShowWnd : public CRichEditCtrl
{
// Construction
public:
	CShowWnd();

// Attributes
public:
	CBrush Brush;
	CFont Font;
	CSerialAppDlg* m_pDlg;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShowWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	void ShowMessage(CString name,CString msg,COLORREF color=RGB(0,0,255));
	void ShowMessageThis(CString msg);
	void ShowMessageThat(CString msg);

	virtual ~CShowWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CShowWnd)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	afx_msg HBRUSH CtlColor(CDC *pDC,UINT nCtlColor);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHOWWND_H__753103D2_AD6C_443C_92E2_567905DDC8E4__INCLUDED_)
