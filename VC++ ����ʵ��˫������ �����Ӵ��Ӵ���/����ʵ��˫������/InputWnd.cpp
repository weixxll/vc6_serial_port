// InputWnd.cpp : implementation file
//

#include "stdafx.h"
#include "SerialAppDlg.h"
#include "InputWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInputWnd


CInputWnd::CInputWnd(CSerialAppDlg* pDlg)
{	
	m_pDlg = pDlg;
	Brush.CreateSolidBrush(RGB(255, 255, 255));
//	Font.CreateFont(15, 15, 0, 0, FW_BOLD, FALSE, FALSE,0,0,0,0,0,0, "Verdana");
	
}

CInputWnd::CInputWnd()
{	
	m_pDlg = NULL;
	Brush.CreateSolidBrush(RGB(255, 255, 255));
//	Font.CreateFont(15, 15, 0, 0, FW_BOLD, FALSE, FALSE,0,0,0,0,0,0, "Verdana");
}

CInputWnd::~CInputWnd()
{
}


BEGIN_MESSAGE_MAP(CInputWnd, CRichEditCtrl)
	//{{AFX_MSG_MAP(CInputWnd)
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInputWnd message handlers

void CInputWnd::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	if(nChar != VK_RETURN){
		CRichEditCtrl::OnChar(nChar,nRepCnt,nFlags);
	}
	else{
		((CSerialAppDlg*)GetOwner())->OnBtnsend();
	}
	
}

