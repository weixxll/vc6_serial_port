#if !defined(AFX_SerialAppDlg_H__E02C6BD7_9E0C_4588_A6B1_05E437BFF74B__INCLUDED_)
#define AFX_SerialAppDlg_H__E02C6BD7_9E0C_4588_A6B1_05E437BFF74B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SerialAppDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
#include "ShowWnd.h"
#include "InputWnd.h"
#include "Resource.h"
#include "ExpandingDialog.h"
#include "SerialPort.h"
#include "ConfigDlg.h"
#include <io.h>
#include <memory.h>

#define WM_MSG_READ					WM_USER+20

const	BYTE	STR[1]={0x3};
const	BYTE    SYN[1]={0x1};
const	BYTE	ACK[1]={0x2};
const	BYTE    RESEND[1]={0x4};
const	BYTE	BUSY[1]={0x7};
const	BYTE	BYE[3]={6,0,6};

const WORD	nPackSize=0x1000;
const int	nSendBuffer=nPackSize*10;
const DWORD TIMELIMIT=30000; // 0.01 Second
const DWORD	MAXTIME=300000;
const DWORD SYNTIME=300000;		//for send SYN
const DWORD RSDTIME=30000;		//for resend


// CSerialAppDlg dialog

class CSerialAppDlg : public CExpandingDialog
{
// Construction
public:
	CSerialAppDlg(CWnd* pParent = NULL);   // standard constructor
	~CSerialAppDlg();

// Dialog Data
	//{{AFX_DATA(CSerialAppDlg)
	enum { IDD = IDD_SERIALAPP_DIALOG };
	CProgressCtrl	m_ProgressCtrl;
	CInputWnd m_wndInput;
//	CButton	m_btnSend;
	CShowWnd m_wndShow;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialAppDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void WriteMsg(CString str);
	BYTE* FormatPack(BYTE* buf,long& buflen);
	BOOL SendFile();
	int WriteCommBlock(unsigned char* pBlock, int nLength);
	BOOL ReceiveFile();
	int ReadCommBlock(unsigned char* pBlock, int nMaxLength);

	HICON m_hIcon;
	CToolBar m_wndToolBar;

	CSerialPortEx	m_Port;

	CString m_strReceived;

	static int file;  //file!=0 means start file transfer
	static int msg;	//msg==1 means start message transfer

	//TO DEBUG
	FILE* fsend;
	FILE* frec;

// Attributes
public:
	static UINT SendThread(LPVOID pParam);
	static UINT ReceiveThread(LPVOID pParam);
	int m_nComm;
	int m_nBandRate;
	char m_cParity;
	int m_nDataBits;
	int m_nStopBits;
	DWORD m_dwCommEvents;
	int m_nSendBuffer;

	// Generated message map functions
	//{{AFX_MSG(CSerialAppDlg)
	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	afx_msg void OnConfig();
	afx_msg void OnConnect();
	afx_msg void OnDisconnect();
	afx_msg void OnBtnsend();
	afx_msg LONG OnReceiveByte(UINT, LONG);
	afx_msg LONG OnCTSDetected(UINT, LONG);
	afx_msg LONG OnReadProcess(UINT, LONG);
	afx_msg void OnSendfileRadio();
	afx_msg void OnFilestart();
	afx_msg void OnSendfilechoose();
	afx_msg void OnRecfilechoose();
	afx_msg void OnRecfileRadio();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SerialAppDlg_H__E02C6BD7_9E0C_4588_A6B1_05E437BFF74B__INCLUDED_)
