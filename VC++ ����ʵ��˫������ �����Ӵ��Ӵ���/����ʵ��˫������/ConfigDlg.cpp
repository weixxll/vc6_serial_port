/*
**	FILENAME			ConfigDlg.cpp
**
**	PURPOSE				Here the ports can be configured
**
**	CREATION DATE		15-09-1997
**	LAST MODIFICATION	12-11-1997
**
**	AUTHOR				Remon Spekreijse
**
*/


#include "stdafx.h"
#include "resource.h"
#include "ConfigDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConfigDlg dialog


CConfigDlg::CConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CConfigDlg)
	m_strBaudRate = _T("");
	m_strDataBits = _T("");
	m_strParity = _T("");
	m_strStopBits = _T("");
	m_strSendBuffer = _T("");
	m_strComm = _T("COM1");
	//}}AFX_DATA_INIT
}

CConfigDlg::CConfigDlg(CWnd* pParent, DCB dcb)
	: CDialog(CConfigDlg::IDD, pParent)
{
	m_dcb = dcb;
}

CConfigDlg::CConfigDlg(CWnd* pParent, CSerialPortEx* pPort)
	: CDialog(CConfigDlg::IDD, pParent)
{
	m_dcb = pPort->GetDCB();
	char temp[10];
	m_strComm=_T(CString("COM")+itoa(pPort->m_nPortNr,temp,10));

}


void CConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConfigDlg)
	DDX_CBString(pDX, IDC_BAUDRATECOMBO, m_strBaudRate);
	DDX_CBString(pDX, IDC_DATABITSCOMBO, m_strDataBits);
	DDX_CBString(pDX, IDC_PARITYCOMBO, m_strParity);
	DDX_CBString(pDX, IDC_STOPBITSCOMBO, m_strStopBits);
	DDX_CBString(pDX, IDC_SENDBUFFERCOMBO, m_strSendBuffer);
	DDX_CBString(pDX, IDC_COMMCOMBO, m_strComm);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CConfigDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfigDlg message handlers

BOOL CConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString strTemp;
	strTemp.Format("%d", m_dcb.BaudRate);
	((CComboBox*)GetDlgItem(IDC_BAUDRATECOMBO))->SelectString(0, strTemp);

	((CComboBox*)GetDlgItem(IDC_PARITYCOMBO))->SetCurSel(m_dcb.Parity);

	((CComboBox*)GetDlgItem(IDC_STOPBITSCOMBO))->SetCurSel(m_dcb.StopBits);

	strTemp.Format("%d", m_dcb.ByteSize);
	((CComboBox*)GetDlgItem(IDC_DATABITSCOMBO))->SelectString(0, strTemp);

	CComboBox* pcommChoice=(CComboBox*)this->GetDlgItem(IDC_COMMCOMBO);
	switch(m_strComm[3])
	{
	case '1':
		pcommChoice->SetCurSel(0);
		break;
	case '2':
		pcommChoice->SetCurSel(1);
		break;
	case '3':
		pcommChoice->SetCurSel(2);
		break;
	case '4':
		pcommChoice->SetCurSel(3);
		break;
	default:
		break;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
																  


void CConfigDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}
