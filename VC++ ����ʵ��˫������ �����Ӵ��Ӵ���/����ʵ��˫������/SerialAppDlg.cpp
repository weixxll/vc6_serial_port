// SerialAppDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SerialAppDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int CSerialAppDlg::file=0;
int CSerialAppDlg::msg=0;
 

/////////////////////////////////////////////////////////////////////////////
// CSerialAppDlg dialog


CSerialAppDlg::CSerialAppDlg(CWnd* pParent /*=NULL*/)
	: CExpandingDialog(CSerialAppDlg::IDD, pParent,IDC_DEFAULTBOX,IDC_DETAILS,
			_T("Show >>"),_T("Hide <<"))
{
	//{{AFX_DATA_INIT(CSerialAppDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_strReceived.Empty();
	m_nComm=0;
	m_nBandRate=19200;
	m_nDataBits=8;
	m_nStopBits=1;
	m_cParity='N';


	fsend=fopen("C:\\fsend","wb");
	frec=fopen("C:\\frec","wb");
}


CSerialAppDlg::~CSerialAppDlg()
{
	CExpandingDialog::~CExpandingDialog();
	
	//to debug
	fclose(fsend);
	fclose(frec);
}

void CSerialAppDlg::DoDataExchange(CDataExchange* pDX)
{
	CExpandingDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSerialAppDlg)
	DDX_Control(pDX, IDC_PROGRESS, m_ProgressCtrl);
	DDX_Control(pDX, IDC_MSG, m_wndInput);
//	DDX_Control(pDX, IDC_BTNSEND, m_btnSend);
	DDX_Control(pDX, IDC_CHAT, m_wndShow);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSerialAppDlg, CExpandingDialog)
	//{{AFX_MSG_MAP(CSerialAppDlg)
	ON_WM_PAINT()
	ON_COMMAND(ID_CONFIG, OnConfig)
	ON_COMMAND(ID_CONNECT, OnConnect)
	ON_COMMAND(ID_DISCONNECT, OnDisconnect)
	ON_BN_CLICKED(IDC_BTNSEND, OnBtnsend)
	ON_MESSAGE(WM_COMM_RXCHAR, OnReceiveByte)
	ON_MESSAGE(WM_COMM_CTS_DETECTED, OnCTSDetected)
	ON_MESSAGE(WM_MSG_READ, OnReadProcess)
	ON_BN_CLICKED(IDC_SENDFILE_RADIO, OnSendfileRadio)
	ON_BN_CLICKED(IDC_FILESTART, OnFilestart)
	ON_BN_CLICKED(IDC_SENDFILECHOOSE, OnSendfilechoose)
	ON_BN_CLICKED(IDC_RECFILECHOOSE, OnRecfilechoose)
	ON_BN_CLICKED(IDC_RECFILE_RADIO, OnRecfileRadio)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerialAppDlg message handlers

void CSerialAppDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
//		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CExpandingDialog::OnPaint();
	}

}

BOOL CSerialAppDlg::OnInitDialog() 
{
	CExpandingDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
		ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon


	// TODO: Add extra initialization here

	CenterWindow();

	//create the tool bar
	if(!m_wndToolBar.Create(this,
				WS_CHILD|WS_VISIBLE|CBRS_TOP|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE|CBRS_SIZE_DYNAMIC)
				||!m_wndToolBar.LoadToolBar(IDR_MAINFRAME)){
		TRACE0("Failed to create the tool bar!\n");
		return -1;
	}
	m_wndToolBar.GetToolBarCtrl().ModifyStyle(0,TBSTYLE_FLAT);
	m_wndToolBar.MoveWindow(6,4,385,30);

	DWORD dwExStyle = TBSTYLE_EX_DRAWDDARROWS;
	m_wndToolBar.GetToolBarCtrl().SendMessage(TB_SETEXTENDEDSTYLE, 0, (LPARAM)dwExStyle);
	
	DWORD dwStyle = m_wndToolBar.GetButtonStyle(m_wndToolBar.CommandToIndex(ID_CONNECT));
	dwStyle |= TBSTYLE_DROPDOWN;
	m_wndToolBar.SetButtonStyle(m_wndToolBar.CommandToIndex(ID_CONNECT), dwStyle);


	// init the port
	if (m_Port.InitPort(this, 1, 9600 ))
		;
//		m_Port.StartMonitoring();
	else
	{
		AfxMessageBox("Can't init COM1");
		return FALSE;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSerialAppDlg::OnConfig() 
{
	// TODO: Add your command handler code here
	CConfigDlg* dlg = new CConfigDlg(this, &m_Port);
	dlg->m_strSendBuffer.Format("%d", m_Port.GetWriteBufferSize());
	
	DWORD dwCommEvents = m_Port.GetCommEvents();

	if (dlg->DoModal() == IDOK)
	{

		switch(dlg->m_strComm[3])
		{
		case '1':
			m_nComm=0;
			break;
		case '2':
			m_nComm=1;
			break;
		case '3':
			m_nComm=2;
			break;
		case '4':
			m_nComm=3;
			break;
		default:
			break;
		}
		m_Port.m_nPortNr=m_nComm+1;
		
		this->m_nBandRate=	m_Port.m_dcb.BaudRate=	atoi(dlg->m_strBaudRate);
		this->m_cParity=	dlg->m_strParity[0];
		switch(m_cParity)
		{
		case 'N':
			m_Port.m_dcb.Parity=NOPARITY;
			break;
		case 'O':
			m_Port.m_dcb.Parity=ODDPARITY;
			break;
		case 'E':
			m_Port.m_dcb.Parity=EVENPARITY;
			break;
		case 'M':
			m_Port.m_dcb.Parity=MARKPARITY;
			break;
		case 'S':
			m_Port.m_dcb.Parity=SPACEPARITY;
			break;
		default:
			break;		
		}

		this->m_nDataBits=	m_Port.m_dcb.ByteSize=	atoi(dlg->m_strDataBits);
		this->m_nStopBits=	m_Port.m_dcb.StopBits=	atoi(dlg->m_strStopBits);
		this->m_dwCommEvents=	dwCommEvents;
		this->m_nSendBuffer=m_Port.m_nWriteBufferSize=	atoi(dlg->m_strSendBuffer);
		if(m_Port.m_szWriteBuffer)
			delete m_Port.m_szWriteBuffer;
		m_Port.m_szWriteBuffer=new BYTE[m_Port.m_nWriteBufferSize];

		if(!(m_Port.InitPort(this,m_nComm+1,m_nBandRate,m_cParity,m_nDataBits,m_nStopBits)))
		{
			char error[100];
			sprintf(error,"COM%d被占用或没有该COM口,请改用其它COM口！",m_nComm);
			AfxMessageBox(error);
		}
	}
	
	delete dlg;
}

void CSerialAppDlg::OnConnect() 
{
	// TODO: Add your command handler code here

	if(m_Port.m_bThreadAlive) return;
	if (m_Port.InitPort(this, m_nComm + 1, m_nBandRate, m_cParity, m_nDataBits, m_nStopBits ))
	{
		m_Port.StartMonitoring();
		m_wndShow.ShowMessage("Status: ","串口已打开");
	}
	else
	{
		// port not found
		AfxMessageBox("该COM口不可用，请改用其它COM口");
	}
}

void CSerialAppDlg::OnDisconnect() 
{
	// TODO: Add your command handler code here
	m_Port.StopMonitoring();
}

void CSerialAppDlg::OnBtnsend() 
{
	// TODO: Add your control notification handler code here
	CString str;
	m_wndInput.GetWindowText(str);
	WriteMsg(str);
	m_wndShow.ShowMessageThis(str);
	m_wndInput.SetWindowText("");
}

LONG CSerialAppDlg::OnReceiveByte(UINT wParam,LONG lParam)
{
	if(CSerialAppDlg::file==1)
	{
		m_strReceived+=(char)wParam;
		return 0;
	}
	switch((char)wParam)
	{
	case 3:				//STR[0]:
		if(msg==0)
		{
			msg=1;
			m_strReceived.Empty();	//to clear the buffer ;maybe need edit
		}
		else if(msg==1)
		{
			msg=0;
			::SendMessage(m_hWnd, WM_MSG_READ, (WPARAM) 0, (LPARAM) 0);
		}
		break;
	case 1:			//SYN[0]:	//message中不可能出现SYN[0],所以未加if(msg==0)语句校验
		if(file==0)
		{
			m_strReceived.Empty();
			m_strReceived+=(char)wParam;
			CSerialAppDlg::file=1;
//			this->WriteCommBlock((unsigned char*)ACK,1);
			CWinThread * fileThread=AfxBeginThread(CSerialAppDlg::ReceiveThread,this,THREAD_PRIORITY_HIGHEST); 
		}
		break;
	
	default:
		m_strReceived+=(char)wParam;
		break;
	}	
	return 0;
}


LONG CSerialAppDlg::OnReadProcess(UINT wParam,LONG lParam)
{
	UINT length;
	if(wParam==0)
		length=nSendBuffer;
	else
		length=wParam;
	BYTE* pBlock=new BYTE[length];
	memset(pBlock,0,length);

	int nRead=ReadCommBlock(pBlock,length);
	m_wndShow.ShowMessageThat(CString(pBlock));
	return nRead;
}

LONG CSerialAppDlg::OnCTSDetected(UINT wParam,LONG lParam)
{
	return 0;
}


int CSerialAppDlg::ReadCommBlock(unsigned char* pBlock, int nMaxLength)
{
	//maybe need delay
//?	memset(pBlock,0,nMaxLength);
	if(!m_Port.m_bBlockRead)
	{
		if(m_strReceived.GetLength()==0) return 0;
	
		int nRec=m_strReceived.GetLength();
		int nLength = (nMaxLength>nRec)? nRec : nMaxLength;
		CString strCopy= m_strReceived;
		char *temp=strCopy.GetBuffer(m_strReceived.GetLength()+1);
		memcpy((char*)pBlock,temp,nLength);

		//to debug
		fwrite(pBlock,1,nLength,frec);
	
		strCopy.ReleaseBuffer();

		m_strReceived.Delete(0,nLength);
		return nLength;
	}
	else
	{
		int templen=nPackSize+2;
		BYTE* pRead=CSerialPortEx::ReadBlock(&m_Port,templen);
		memcpy(pBlock,pRead,templen);

		fwrite(pBlock,1,templen,frec);
		
		delete pRead;
		return templen;
	}
}


//DEL void CSerialAppDlg::TransFile(char *pBlock, int length)
//DEL {
//DEL 	
//DEL }

BOOL CSerialAppDlg::ReceiveFile()
{
	CString path;
	CWnd* pWnd = GetDlgItem(IDC_EDIT_RECFILE);
	pWnd->GetWindowText(path);

	if(path=="")
	{
		CFileDialog dlg(FALSE);
		if(dlg.DoModal()==IDOK)
		{
			path = dlg.GetPathName();
			CWnd* pWnd = GetDlgItem(IDC_EDIT_RECFILE);
			pWnd->SetWindowText(path);
		}
	}
	if(path=="") return FALSE;

	const	int	MAXBLOCK=nPackSize+2; 

	int		step;
	FILE	*RSF;
	long	readlen,templen;
	WORD	check;

	DWORD	nLength,start,ReSendSum,ci;

	BOOL	bTimeout;

	BYTE    abIn[ MAXBLOCK],*buf,*bufp,*old_bufp;

	BOOL    bRet=TRUE; 
   	WORD    nRecord=0;
	WORD	totalRec=0;
	WORD	initRec=10;
	
	ReSendSum=3; 


   	RSF=fopen(path,"wb");//"wb" for append
	if(!RSF)
	{
		return FALSE;
	}
	buf=(BYTE*)new BYTE[MAXBLOCK*initRec];//
	if(!buf)
	{
		fclose(RSF);
		return FALSE;
	}

    step=0;
	totalRec=0;
	
	bufp=buf;
	nLength=0;
	start=GetTickCount();

	bTimeout=FALSE;

	while((nLength==0)&&(!bTimeout)&&(!m_Port.m_bBlockRead))
	{
		nLength=m_strReceived.GetLength();
		bTimeout = (GetTickCount()-start>=SYNTIME);
	}

	if (bTimeout) {
//		SendCommMsg(CSM_TIMEOUT,0);
//		return FALSE;
		bRet=FALSE;
		goto clearrub;
	}

	do{
		switch(step)
		{
			case 0:	
				    nLength = ReadCommBlock( (unsigned char*) abIn, MAXBLOCK );
					if((abIn[0]==SYN[0])) 
					{
						WriteCommBlock( (unsigned char*)ACK, 1);
						step=1;
						ReSendSum=3; 
						start=GetTickCount();					
					}
					break;

			case 1:
					bTimeout=FALSE;
					nLength=0;
					while((nLength<6)&&(!bTimeout))
					{
						nLength = m_strReceived.GetLength();
						bTimeout = (GetTickCount()-start>=RSDTIME);
					}
					if (bTimeout) 
					{
//						SendCommMsg(CSM_TIMEOUT,0);
//						return FALSE;
						bRet=FALSE;
						goto clearrub;
					}

					nLength = ReadCommBlock(abIn, MAXBLOCK );
					if( (nLength==6) &&(abIn[0]==0) && ( (abIn[1]==0) || (abIn[1]==1) )) 
					{	
						
						start=GetTickCount();	
						check=0;
						check+=abIn[2]+(abIn[2]&0x80?0xff00:0);
						check+=abIn[3]+(abIn[3]&0x80?0xff00:0);
						
						if((BYTE(check>>8)==abIn[4])&&(BYTE(check&0xff)==abIn[5]))
						{
							ReSendSum=3; 
							memcpy(bufp,abIn,6);
							bufp+=6;

							if(abIn[1]==1)
							{
								if((abIn[2]==0)&&(abIn[3]==0)&&(abIn[4]==0)&&(abIn[5]==0)) 
								{
									step=4;		//完结
								}
								else
								{
									step=1;
									totalRec=((WORD)abIn[2]<<8)+(WORD)abIn[3];
									if(totalRec>initRec)
									{
										BYTE* newbuf=new BYTE[totalRec*MAXBLOCK];
										if(!newbuf)
										{
											bRet=FALSE;
											goto clearrub;
										}
										memcpy(newbuf,buf,initRec*MAXBLOCK);
										bufp=newbuf+(bufp-buf);
										delete buf;
										buf=newbuf;
										newbuf=NULL;

										this->m_ProgressCtrl.SetRange(0,totalRec);
									}
								}
							}
							
							else
							{
								//当传输只有一个字节的文件时将出现问题
//								if((abIn[3]==1)&&(abIn[4]==0)&&(abIn[5]==1)) 
//								{
//									step=4;
	//?								fwrite("\0\0\0\x1\0\x1",1,6,RSF);
//									//完结
//								}
//								else
//								{	
									step=3;
									templen=(((long)abIn[2]<<8)+(long)abIn[3])+(long)2;
									m_Port.m_bBlockRead=TRUE;
//								}
							}

							WriteCommBlock((unsigned char*)ACK, 1);
							break;
						}
					}
					if(ReSendSum==0)
					{
//						AfxMessageBox("Fail");,0);
//						return FALSE;
						bRet=FALSE;
						goto clearrub;
					}
					step=1;
					ReSendSum--;
					WriteCommBlock((unsigned char*)RESEND, 1);
					break;

					
			//开始下一包，并记录进度
			case 2:
					nLength = ReadCommBlock(abIn, MAXBLOCK );
					if(abIn[0]==SYN[0])
					{	
						start=GetTickCount();
						ReSendSum=3;
						step=1;

						nRecord++;
						if(nRecord)				   
							this->m_ProgressCtrl.SetPos(nRecord);	 
						
						WriteCommBlock( (unsigned char*)ACK, 1);
					}
					break;
			
			//读包内数据
			case 3:
					step=2;
					check=0;
					readlen=0;
					old_bufp=bufp;
					start=GetTickCount();
					do{	
						nLength = ReadCommBlock( (unsigned char*) abIn, MAXBLOCK );
						if(nLength>0)
						{	
							start=GetTickCount();
							memcpy(bufp,abIn,nLength);
							bufp+=(long)nLength;
							for(ci=0;ci<nLength;ci++) 
								check+=abIn[ci]+(abIn[ci]&0x80?0xff00:0);
							readlen+=(long)nLength;
						}

						if((GetTickCount()-start)>TIMELIMIT)
						{
//							SendCommMsg(CSM_TIMEOUT,0);
//							return FALSE;
							::AfxMessageBox("RSDTIME TOO SHORT");
							bRet=FALSE;
							goto clearrub;
						}
						
					}while(readlen<templen);

					m_Port.m_bBlockRead=FALSE;

					check-=(*(bufp-1)+(*(bufp-1)&0x80?0xff00:0));
					check-=(*(bufp-2)+(*(bufp-2)&0x80?0xff00:0));

					if((readlen!=templen)||(check>>8)!=*(bufp-2)||(check&0xff)!=*(bufp-1))
					{	
						bufp=old_bufp;
						step=3;
						if (ReSendSum>0)
						{
							ReSendSum--;
							WriteCommBlock((unsigned char*) RESEND,1);
						}
						else
						{
//							AfxMessageBox("Fail");,0);
//							return FALSE;
							char error[100];
							sprintf(error,"readlen %d\n",readlen);
							::AfxMessageBox(error);
							bRet=FALSE;
							goto clearrub;
						}
						break;
					}

					fwrite(old_bufp,1,templen-2,RSF);

					WriteCommBlock((unsigned char*) ACK, 1);
					break;

				case 4:
						Sleep(20);
						nLength = ReadCommBlock( abIn, MAXBLOCK );
						if((nLength==3)&&(abIn[0]==6)&&(abIn[1]==0)&&(abIn[2]==6))
						{	
							start=GetTickCount();
							step=5;
				   			WriteCommBlock( (unsigned char*)ACK, 1);
						};
						break;
				default:
						break;
			};
			bTimeout = (GetTickCount()-start>=SYNTIME);

	}while((step<5)&&(!bTimeout));


clearrub:
	m_Port.m_bBlockRead=FALSE;
	free(buf);
//	if(step!=5)
//		fwrite("\0\0\0\x1\0\x1",1,6,RSF);
	fclose(RSF);    

	return 	bRet;
}

int CSerialAppDlg::WriteCommBlock(unsigned char *pBlock, int nLength)
{
	int count=0;
	m_Port.m_nWriteBufferSize=nPackSize+2;
	if(m_Port.m_szWriteBuffer)
		delete m_Port.m_szWriteBuffer;
	m_Port.m_szWriteBuffer=new BYTE[m_Port.m_nWriteBufferSize];
	while(nLength > m_Port.m_nWriteBufferSize)
	{
		m_Port.WriteToPort(pBlock+count,m_Port.m_nWriteBufferSize);
		nLength-=m_Port.m_nWriteBufferSize;
		count+=m_Port.m_nWriteBufferSize;
//		Sleep(10);
	}
	m_Port.WriteToPort(pBlock+count,nLength);
	fwrite(pBlock,1,count+nLength,fsend);
	return 0;
}


//DEL void CSerialAppDlg::OnFiletransfer() 
//DEL {
//DEL 	// TODO: Add your command handler code here
//DEL 	CWinThread* sendThread=AfxBeginThread(CSerialAppDlg::SendThread,this);
//DEL }

UINT CSerialAppDlg::ReceiveThread(LPVOID pParam)
{
	DWORD start=::GetTickCount();

	CSerialAppDlg* pDlg=(CSerialAppDlg*)pParam;
	CWnd* pWnd=pDlg->GetDlgItem(IDC_MSG);
	pWnd->EnableWindow(FALSE);
	
	BOOL bSuccess=pDlg->ReceiveFile();
	CSerialAppDlg::file=0;

	if(bSuccess)
	{
		AfxMessageBox("Receive File Complete");
		char temp[100];
		sprintf(temp,"Receive time:%d",::GetTickCount()-start);
		pDlg->m_wndShow.ShowMessage("Status: ",CString(temp));
	}
	else
		AfxMessageBox("Receive File Fail");

	pDlg->m_ProgressCtrl.SetPos(0);

	pWnd->EnableWindow(TRUE);

	return 0;
}

UINT CSerialAppDlg::SendThread(LPVOID pParam)
{
	DWORD start=::GetTickCount();

	file=1;
	CSerialAppDlg* pDlg=(CSerialAppDlg*)pParam;

	CWnd* pWnd=pDlg->GetDlgItem(IDC_MSG);
	pWnd->EnableWindow(FALSE);

	BOOL bSuccess=pDlg->SendFile();
	file=0;
	if(bSuccess)
	{
		AfxMessageBox("Send File Complete");
		char temp[100];
		sprintf(temp,"Send time:%d",::GetTickCount()-start);
		pDlg->m_wndShow.ShowMessage("Status: ",CString(temp));
	}
	else
		AfxMessageBox("Send File Fail");

	pDlg->m_ProgressCtrl.SetPos(0);
	pWnd->EnableWindow(TRUE);
//	pDlg->m_btnSend.EnableWindow(TRUE);
	

	return 0;
}

BOOL CSerialAppDlg::SendFile()
{

	CString path;
	CWnd* pWnd = GetDlgItem(IDC_EDIT_SENDFILE);
	pWnd->GetWindowText(path);

	if(path=="")
	{
		CFileDialog dlg(TRUE);
		if(dlg.DoModal()==IDOK)
		{
			path = dlg.GetPathName();
			CWnd* pWnd = GetDlgItem(IDC_EDIT_SENDFILE);
			pWnd->SetWindowText(path);
		}
	}
	if(path=="") return FALSE;

	int		step,old_step;
	BYTE	*buf,*bufp,*old_bufp;
	BOOL	bTimeout;
	BOOL    bRet=TRUE;
	WORD    RecordNum=0;
	WORD	totalRec=0;
	FILE	*RSF;

	DWORD    start;
	int      nLength;
	const	 int	MAXBLOCK=nPackSize+10; 
	BYTE     abIn[ MAXBLOCK];
	long	filelen,templen,totallen;//,readlen;
//	WORD	check,ci;



    RSF=fopen(path,"rb");
	if(!RSF)
	{
		AfxMessageBox("Fail");
		return FALSE;
	}
	filelen=_filelength(_fileno(RSF));
	buf=new BYTE[filelen];
	if(!buf)
	{
		AfxMessageBox("Fail");
		fclose(RSF);
		return FALSE;
	}
	fread(buf,1,filelen,RSF);
	fclose(RSF);

	totalRec=filelen/nPackSize+1;

	m_ProgressCtrl.SetRange(0,totalRec);

	buf=FormatPack(buf,filelen);

	step=0;
	bufp=buf;
	old_step=step;
	old_bufp=bufp;
	totallen=0;

	start=GetTickCount();
	nLength=0;
	bTimeout=FALSE;

	while((nLength==0)&&(!bTimeout))
	{
		WriteCommBlock((unsigned char*)SYN,1);
		Sleep(50);
		nLength=m_strReceived.GetLength();
		bTimeout = (GetTickCount()-start>=SYNTIME);
	}

	if (bTimeout) {
//		SendCommMsg(CSM_TIMEOUT,0);
		AfxMessageBox("TimeOut 1");
		return FALSE;
	}


	do{
		bTimeout=FALSE;
		nLength=0;
		start=GetTickCount();
		while((nLength==0)&&(!bTimeout))
		{
			nLength=m_strReceived.GetLength();
			bTimeout = (GetTickCount()-start>=SYNTIME);
		}
		if (bTimeout) {
//			SendCommMsg(CSM_TIMEOUT,0);
			AfxMessageBox("TimeOut 2");
			return FALSE;
		}

		nLength = ReadCommBlock( (unsigned char*) abIn, MAXBLOCK);
		
		start=GetTickCount();
	    if((abIn[0]==RESEND[0]) && (step!=0))					
		{
			bufp=old_bufp;
			step=old_step;
		}

		if((abIn[0]==ACK[0])||(abIn[0]==RESEND[0]))
		{
			switch(step)
			{
				case 0:		//发帧头

						old_step=step;
						old_bufp=bufp;

						templen=(((long)bufp[2]<<8)+bufp[3])+2;
						TRACE("bufp:%d",bufp);
						
//						check=bufp[2]+(bufp[2]&0x80?0xff00:0)+bufp[3]+(bufp[3]&0x80?0xff00:0);
//						bufp[4]=check>>8;
//						bufp[5]=check&0xff;

						WriteCommBlock((unsigned char*)bufp,6);
						
						bufp+=6;
						start=GetTickCount();
						
						step=1;
					
						if((templen==2) && (old_bufp[1]==1)) 
							step=4;			//ASK						
						else if((old_bufp[0]==0) && (old_bufp[1]==1))
							step=0;

					break;

				case 1:		//发数据,包括校验和

					old_step=step;
					old_bufp=bufp;
					
//					check=0;
					if(templen>0)
					{
						WriteCommBlock(bufp,templen);
//						for(ci=0;ci<templen-2;ci++) 
//							check+=bufp[ci]+(bufp[ci]&0x80?0xff00:0);
					}
					bufp+=templen;

//					abIn[0]=check>>8;
//					abIn[1]=check&0xff;
	
//					WriteCommBlock((unsigned char*)abIn,2);
					start=GetTickCount();

					step=2;
					break;


				//设置该状态已便于计算进程
				
				case 2: 
				
					old_step=step;
					old_bufp=bufp;
				
					RecordNum++;
					//TO ADD : REPORT THE PROGRESS
					this->m_ProgressCtrl.SetPos(RecordNum);
//?						SendCommMsg(CSM_RUNNING,RecordNum);

					start=GetTickCount();
					nLength=0;
					bTimeout=FALSE;
					while((nLength==0)&&(!bTimeout))
					{
						WriteCommBlock((unsigned char*)SYN,1);
						Sleep(50);
						nLength=m_strReceived.GetLength();
						bTimeout = (GetTickCount()-start>=SYNTIME);
					}
					if (bTimeout) {
//						SendCommMsg(CSM_TIMEOUT,0);
						AfxMessageBox("TimeOut 3");
						return FALSE;
					}
					step=0;
					break;

				case 4: 
					old_step=step;
					old_bufp=bufp;
					WriteCommBlock((unsigned char*)BYE,3);
					step=5;
					break;
				
				case 5:
					break;

				default:
					break;
			};
		}

		else
		{
			WriteCommBlock((unsigned char*)ACK,1);
		}

		if((GetTickCount()-start)>MAXTIME)
		{
//			SendCommMsg(CSM_TIMEOUT,0);  
			AfxMessageBox("TimeOut 4");
			bRet=FALSE;
			goto finish;
		}

	}while((step<5)&&(bRet));

finish:
	delete buf;
    if(step==5)
	{
		bRet=TRUE;
	}
	else
		bRet=FALSE;

	return bRet;
}



void CSerialAppDlg::OnSendfileRadio() 
{
	// TODO: Add your control notification handler code here
	CButton* pButton = (CButton*)GetDlgItem(IDC_RECFILE_RADIO);
	pButton->SetCheck(BST_UNCHECKED);
}


void CSerialAppDlg::OnRecfileRadio() 
{
	// TODO: Add your control notification handler code here	
	CButton* pButton = (CButton*)GetDlgItem(IDC_SENDFILE_RADIO);
	pButton->SetCheck(BST_UNCHECKED);
}


void CSerialAppDlg::OnFilestart() 
{
	// TODO: Add your control notification handler code here
	if(file==1)
		return;
	CButton* pWnd = ( CButton*) GetDlgItem(IDC_SENDFILE_RADIO);
	BOOL bSend = pWnd->GetCheck();
	if(bSend)
	{
		CWinThread* sendThread=AfxBeginThread(CSerialAppDlg::SendThread,this,THREAD_PRIORITY_HIGHEST);
//		m_btnSend.EnableWindow(FALSE);
//		m_btnSend.SetWindowText("stop");
	}
//		else
//			CWinThread* recThread=AfxBeginThread(CSerialAppDlg::ReceiveThread,this);
}

void CSerialAppDlg::OnSendfilechoose() 
{
	// TODO: Add your control notification handler code here
	CFileDialog dlg(TRUE);
	if(dlg.DoModal()==IDOK)
	{
		CWnd* pWnd = GetDlgItem(IDC_EDIT_SENDFILE);
		pWnd->SetWindowText(dlg.GetPathName());
	}
}

void CSerialAppDlg::OnRecfilechoose() 
{
	// TODO: Add your control notification handler code here
	CFileDialog dlg(FALSE);
	if(dlg.DoModal()==IDOK)
	{
		CWnd* pWnd = GetDlgItem(IDC_EDIT_RECFILE);
		pWnd->SetWindowText(dlg.GetPathName());
	}
}



//DEL void CSerialAppDlg::OnDetails() 
//DEL {
//DEL 	// TODO: Add your control notification handler code here
//DEL 	int i=3;
//DEL }

//DEL void CSerialAppDlg::FormatFile(CString &path)
//DEL {
//DEL 	CString newpath=path+"SerialAppTempFormat";
//DEL 	CopyFile(path,newpath,FALSE);
//DEL 	char* temp = newpath.GetBuffer(newpath.GetLength()+1);
//DEL 	FILE* RSF = fopen((const char*)temp,"rwb");
//DEL 	newpath.ReleaseBuffer();
//DEL 	int nLength=_filelength(_fileno(RSF));
//DEL 	int count=0;
//DEL 	while(nLength>nPackSize)
//DEL 	{
//DEL 	
//DEL 		break;
//DEL 	};
//DEL 
//DEL 	fclose(RSF);
//DEL }


BYTE* CSerialAppDlg::FormatPack(BYTE* buf,long& buflen)
{
	int i=0;
	WORD check=0;
	BYTE head[6] ;
	BYTE end[2] ;
	memset(head,0,6);
	memset(end,0,2);

//	CString str;

	long count = buflen;
	
	//加入总包数纪录
	WORD packCount= buflen/nPackSize;
	if(nPackSize*packCount < buflen)
		packCount++;

	long newlen = 6+(packCount-1)*(nPackSize+8) + buflen-(packCount-1)*nPackSize+8+6;
	BYTE* pnewbuf=new BYTE[newlen];
	BYTE* ptemp=pnewbuf;

	head[1]=1;
	head[2]=HIBYTE(packCount);
	head[3]=LOBYTE(packCount);

	check=head[2]+(head[2]&0x80?0xff00:0)+head[3]+(head[3]&0x80?0xff00:0);
	head[4]=check>>8;
	head[5]=check&0xff;

	memcpy(ptemp,head,6);
	ptemp+=6;

	head[1]=0;


	//分包
	while(count > 0)
	{
		
		WORD realsize =(WORD)( count > nPackSize ? nPackSize : count );

		//加包头
		head[2]=HIBYTE(realsize);
		head[3]=LOBYTE(realsize);		

		check=head[2]+(head[2]&0x80?0xff00:0)+head[3]+(head[3]&0x80?0xff00:0);
		head[4]=check>>8;
		head[5]=check&0xff;

		memcpy(ptemp,head,6);
		ptemp+=6;

		//添加包中数据
		check=0;
		for(int ci=0;ci<realsize;ci++) 
			check+=(buf[buflen-count+ci]+(buf[buflen-count+ci]&0x80?0xff00:0));
		memcpy(ptemp,buf+buflen-count,realsize);
		ptemp+=realsize;
		count-=realsize;

		//加校验和
		end[0]=check>>8;
		end[1]=check&0xff;
		memcpy(ptemp,end,2);
		ptemp+=2;


		TRACE("%d\n",count);
	}

	
	head[2]=head[4]=head[3]=head[5]=0;
	head[1]=1;

	memcpy(ptemp,head,6);
	ptemp+=6;

/*	i=0;
	while(i<6)
	{
		str.Insert(str.GetLength(),head[i]);
		i++;
	}
*/
	delete buf;

	buflen=ptemp-pnewbuf;

	ASSERT(newlen==ptemp-pnewbuf);

	return pnewbuf;
}

void CSerialAppDlg::WriteMsg(CString str)
{
	str.Insert(0,STR[0]);
	str.Insert(str.GetLength(),STR[0]);
	while(str.GetLength()>m_Port.m_nWriteBufferSize)
	{
		CString part=str.Left(m_Port.m_nWriteBufferSize);
		str.Delete(0,m_Port.m_nWriteBufferSize);
		m_Port.WriteToPort(part);
	};
	m_Port.WriteToPort(str);
}
