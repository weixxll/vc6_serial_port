// ShowWnd.cpp : implementation file
//

#include "stdafx.h"
#include "ShowWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CShowWnd

CShowWnd::CShowWnd()
{
	Brush.CreateSolidBrush(RGB(255, 255, 255));
	Font.CreateFont(15, 15, 0, 0, FW_BOLD, FALSE, FALSE,0,0,0,0,0,0, "Verdana");
}

CShowWnd::~CShowWnd()
{
}


BEGIN_MESSAGE_MAP(CShowWnd, CRichEditCtrl)
	//{{AFX_MSG_MAP(CShowWnd)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShowWnd message handlers

int CShowWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CRichEditCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	SetReadOnly();	
	return 0;
}

HBRUSH CShowWnd::CtlColor(CDC* pDC,UINT nCtlColor)
{
	pDC->SetBkColor(RGB(255,255,255));
	return (HBRUSH)Brush;
}


void CShowWnd::ShowMessage(CString name,CString msg,COLORREF color)
{

	CString message;
	message = name;
	message += ": ";
	message += msg;

	message += "\r\n";
	int iTotalLength = GetWindowTextLength();
	SetSel(iTotalLength, iTotalLength);
	ReplaceSel(message);
	int iStartPos = iTotalLength;
	CHARFORMAT cf;

	cf.cbSize		= sizeof (CHARFORMAT);  
	cf.dwMask		= CFM_FACE | CFM_SIZE | CFM_BOLD |
 		CFM_ITALIC | CFM_UNDERLINE | CFM_STRIKEOUT | CFM_PROTECTED | CFM_COLOR ;
	cf.dwEffects	= 0;//(unsigned long)~(CFE_AUTOCOLOR | CFE_UNDERLINE | CFE_BOLD | CFE_ITALIC | CFE_STRIKEOUT);

	cf.crTextColor	= color;
	
	cf.dwEffects = (cf.dwEffects | CFE_BOLD) ;

	cf.dwEffects = (cf.dwEffects& ~CFE_ITALIC);

	cf.dwEffects = (cf.dwEffects& ~CFE_UNDERLINE);
	
	cf.dwEffects = (cf.dwEffects& ~CFE_STRIKEOUT);

	cf.dwMask |= CFM_SIZE;

	cf.yHeight = 12 * 20;

	::lstrcpy(cf.szFaceName, "system"); 

	int iEndPos = GetWindowTextLength();
	SetSel(iStartPos, iEndPos);
	SetSelectionCharFormat(cf);
	HideSelection(TRUE, FALSE);	

	LineScroll(1);

}

void CShowWnd::ShowMessageThis(CString msg)
{
	ShowMessage(" ",msg,RGB(0,255,0));
}

void CShowWnd::ShowMessageThat(CString msg)
{
	ShowMessage(" >>",msg,RGB(255,0,0));
}
