#if !defined(AFX_INPUTWND_H__0A4E996B_87D0_4BD1_A66F_EA5C1968522A__INCLUDED_)
#define AFX_INPUTWND_H__0A4E996B_87D0_4BD1_A66F_EA5C1968522A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InputWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInputWnd window
class CSerialAppDlg;

class CInputWnd : public CRichEditCtrl
{
// Construction
public:
	CInputWnd(CSerialAppDlg* pDlg);
	CInputWnd();

// Attributes
public:

// Operations
public:
	CBrush Brush;
	CFont Font;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInputWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	void DefaultCharFormat();
	CSerialAppDlg* m_pDlg;
	virtual ~CInputWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CInputWnd)
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INPUTWND_H__0A4E996B_87D0_4BD1_A66F_EA5C1968522A__INCLUDED_)
