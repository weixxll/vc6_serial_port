//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CommWizard.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_COMMWIZARD_DIALOG           102
#define CG_IDR_POPUP_HEX_EDIT           103
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDC_COM                         1000
#define IDC_COMSETTING                  1001
#define IDC_SEND                        1002
#define IDC_TIME                        1003
#define IDC_RECEIVE                     1003
#define IDC_SETTINGS                    1004
#define IDC_CLEAR                       1005
#define IDC_TYPE                        1006
#define IDC_EDIT_RECEIVE                1007
#define IDC_COMMCTRL                    1008
#define IDC_MANUALSEND                  1009
#define IDC_ADDRESS_CHECK               1010
#define IDC_HEX_CHECK                   1011
#define IDC_ASCII_CHECK                 1012
#define IDC_48_CHECK                    1013
#define IDC_SLIDER1                     1014
#define IDC_SENDSTRING                  1020

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
