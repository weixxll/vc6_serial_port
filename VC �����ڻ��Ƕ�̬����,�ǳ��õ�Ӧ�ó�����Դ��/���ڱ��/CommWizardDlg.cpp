// CommWizardDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CommWizard.h"
#include "CommWizardDlg.h"
#include "SettingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	afx_msg void OnEditUndo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCommWizardDlg dialog

CCommWizardDlg::CCommWizardDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCommWizardDlg::IDD, pParent)
{
	bSend = FALSE;
	bReceive = FALSE;

	m_nPort = 1;
	m_strSettings = _T("9600,n,8,1");
	m_strSendString = _T("");
//	m_strReceive=_T("");
	m_nTime = 1000;

	m_nOutPutMode = 0;

	//{{AFX_DATA_INIT(CCommWizardDlg)
	m_48 = TRUE;
	m_hex = TRUE;
	m_address = TRUE;
	m_ascii = TRUE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCommWizardDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCommWizardDlg)
	DDX_Control(pDX, IDC_EDIT_RECEIVE, m_Edit);
	DDX_Control(pDX, IDC_SLIDER1, m_slider);
	DDX_Control(pDX, IDC_RECEIVE, m_ctrlReceive);
	DDX_Control(pDX, IDC_SEND, m_ctrlSend);
	DDX_Control(pDX, IDC_TYPE, m_ctrlDataType);
	DDX_Control(pDX, IDC_COMMCTRL, m_Com);
	DDX_Check(pDX, IDC_48_CHECK, m_48);
	DDX_Check(pDX, IDC_HEX_CHECK, m_hex);
	DDX_Check(pDX, IDC_ADDRESS_CHECK, m_address);
	DDX_Check(pDX, IDC_ASCII_CHECK, m_ascii);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCommWizardDlg, CDialog)
	//{{AFX_MSG_MAP(CCommWizardDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_RECEIVE, OnReceive)
	ON_BN_CLICKED(IDC_SEND, OnSend)
	ON_BN_CLICKED(IDC_SETTINGS, OnSettings)
	ON_CBN_SELCHANGE(IDC_TYPE, OnSelchangeType)
	ON_BN_CLICKED(IDC_CLEAR, OnClear)
	ON_BN_CLICKED(IDC_MANUALSEND, OnManualsend)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_HEX_CHECK, OnHexCheck)
	ON_BN_CLICKED(IDC_48_CHECK, On48Check)
	ON_BN_CLICKED(IDC_ADDRESS_CHECK, OnAddressCheck)
	ON_BN_CLICKED(IDC_ASCII_CHECK, OnAsciiCheck)
	ON_WM_HSCROLL()
	ON_EN_CHANGE(IDC_SENDSTRING, OnChangeSendstring)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCommWizardDlg message handlers

BOOL CCommWizardDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	m_ctrlDataType.AddString(_T("按ASCII码"));
	m_ctrlDataType.AddString(_T("按16进制"));
	m_ctrlDataType.SetCurSel(m_nOutPutMode);

	m_slider.SetRange(1, 32);
	m_slider.SetPos(8);

	OpenPort();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCommWizardDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCommWizardDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCommWizardDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CCommWizardDlg::OnReceive() 
{
	// TODO: Add your control notification handler code here
	m_Com.GetInput();//先预读缓冲区以清除残留数据
	bReceive = !bReceive;
	if(bReceive)
		m_ctrlReceive.SetWindowText(_T("停止接收"));
	else
		m_ctrlReceive.SetWindowText(_T("开始接收"));
}

void CCommWizardDlg::OnSend() 
{
	// TODO: Add your control notification handler code here
	bSend = !bSend;
	if(bSend)
	{
		m_ctrlSend.SetWindowText(_T("停止发送"));
		SetTimer(1,m_nTime,NULL);//时间为1000毫秒
	}
	else
	{
		m_ctrlSend.SetWindowText(_T("自动发送"));
		KillTimer(1);  //取消定时
	}	
}

void CCommWizardDlg::OnSettings() 
{
	// TODO: Add your control notification handler code here
	CSettingDlg setDlg;
	
	setDlg.m_nPort = m_nPort;
	setDlg.m_nTime = m_nTime;
	setDlg.m_strSettings = m_strSettings;

	if(setDlg.DoModal() == IDOK)
	{
		m_nPort = setDlg.m_nPort;
		m_nTime = setDlg.m_nTime;
		m_strSettings = setDlg.m_strSettings;
	}
	OpenPort();
}

void CCommWizardDlg::OnSelchangeType() 
{
	// TODO: Add your control notification handler code here
	m_nOutPutMode = m_ctrlDataType.GetCurSel();
}

void CCommWizardDlg::OnClear() 
{
	// TODO: Add your control notification handler code here
//	m_strReceive = _T("");
	m_Edit.Clear();
	m_Edit.RedrawWindow();
}

BEGIN_EVENTSINK_MAP(CCommWizardDlg, CDialog)
    //{{AFX_EVENTSINK_MAP(CCommWizardDlg)
	ON_EVENT(CCommWizardDlg, IDC_COMMCTRL, 1 /* OnComm */, OnOnCommCommctrl, VTS_NONE)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CCommWizardDlg::OnOnCommCommctrl() 
{
	// TODO: Add your control notification handler code here
	VARIANT variant_inp;
	COleSafeArray safearray_inp;
	LONG len,k;
	BYTE rxdata[2048]; //设置BYTE数组 An 8-bit integerthat is not signed.
	CString strtemp;
	if(bReceive)
	{
		if(m_Com.GetCommEvent()==2)     //事件值为2表示接收缓冲区内有字符
		{
			variant_inp = m_Com.GetInput();   //读缓冲区
			safearray_inp = variant_inp;           //VARIANT型变量转换为ColeSafeArray型变量
			len=safearray_inp.GetOneDimSize(); //得到有效数据长度
			for(k=0;k<len;k++)
				safearray_inp.GetElement(&k,rxdata+k);//转换为BYTE型数组
			m_Edit.AppendData(rxdata,len);
//			for(k=0;k<len;k++)             //将数组转换为Cstring型变量
//			{
//				BYTE bt=*(BYTE*)(rxdata+k);      //字符型
				
//				if(m_nOutPutMode == 2)
//					strtemp.Format("%02X ",bt); //将字符以十六进制方式送入临时变量strtemp存放，注意这里加入一个空隔
//				else 
//					strtemp.Format("%c",bt); //将字符送入临时变量strtemp存放

//				m_strReceive += bt;    //加入接收编辑框对应字符串    
//			}
//			m_strReceive += "\r\n";
		}
	}
	UpdateData(FALSE);           //更新编辑框内容
}

void CCommWizardDlg::OpenPort()
{
	if(m_Com.GetPortOpen())
		m_Com.SetPortOpen(FALSE);

	m_Com.SetCommPort(m_nPort);       //选择com1
	if( !m_Com.GetPortOpen())
		m_Com.SetPortOpen(TRUE);//打开串口
	else
		AfxMessageBox("cannot open serial port");

	m_Com.SetSettings(m_strSettings); //波特率9600，无校验，8个数据位，1个停止位
	m_Com.SetRThreshold(1); 
	//参数1表示每当串口接收缓冲区中有多于或等于1个字符时将引发一个接收数据的OnComm事件
	m_Com.SetInputMode(1);
	m_Com.SetInputLen(0);  //设置当前接收区数据长度为0
	m_Com.GetInput();//先预读缓冲区以清除残留数据	
}

void CCommWizardDlg::OnManualsend() 
{
	// TODO: Add your control notification handler code here
//	CString strSend;
//	CEdit* pEditSend =(CEdit*)GetDlgItem(IDC_SENDSTRING);
//	pEditSend->GetWindowText(m_strSendString);
	
	if(m_nOutPutMode > 0)
	{
		CByteArray hexdata;
		int len=String2Hex(m_strSendString ,hexdata);
		m_Com.SetOutput(COleVariant(hexdata));
	}
	else
		m_Com.SetOutput(COleVariant(m_strSendString));//发送数据
}

void CCommWizardDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	OnManualsend();
	CDialog::OnTimer(nIDEvent);
}


int CCommWizardDlg::String2Hex(CString str, CByteArray &senddata)
{
	int hexdata,lowhexdata;
	int hexdatalen=0;
	int len=str.GetLength();
	
	senddata.SetSize(len/2);
	
	for(int i=0;i<len;)
	{
		char lstr,hstr=str[i];
		if(hstr==' ')
		{
			i++;
			continue;
		}
		i++;
		if(i>=len)
			break;
		lstr=str[i];
		hexdata=ConvertHexChar(hstr);
		lowhexdata=ConvertHexChar(lstr);
		if((hexdata==16)||(lowhexdata==16))
			break;
		else 
			hexdata=hexdata*16+lowhexdata;
		i++;
		senddata[hexdatalen]=(char)hexdata;
		hexdatalen++;
	}

	senddata.SetSize(hexdatalen);
	return hexdatalen;
}

//这是一个将字符转换为相应的十六进制值的函数
//功能：若是在0-F之间的字符，则转换为相应的十六进制字符，否则返回-1
char CCommWizardDlg::ConvertHexChar(char ch) 
{
	if((ch>='0')&&(ch<='9'))
		return ch-0x30;
	else if((ch>='A')&&(ch<='F'))
		return ch-'A'+10;
	else if((ch>='a')&&(ch<='f'))
		return ch-'a'+10;
	else return (-1);
}

void CCommWizardDlg::OnHexCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_Edit.SetOptions(m_address, m_hex, m_ascii, m_48);
	m_Edit.RedrawWindow();
}


void CCommWizardDlg::On48Check() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_Edit.SetOptions(m_address, m_hex, m_ascii, m_48);
	m_Edit.RedrawWindow();
}

void CCommWizardDlg::OnAddressCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_Edit.SetOptions(m_address, m_hex, m_ascii, m_48);
	m_Edit.RedrawWindow();
}

void CCommWizardDlg::OnAsciiCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_Edit.SetOptions(m_address, m_hex, m_ascii, m_48);
	m_Edit.RedrawWindow();
}


BOOL CCommWizardDlg::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	if((wParam==1)&&(lParam==0))
		return TRUE;
	return CDialog::OnCommand(wParam, lParam);
}

void CCommWizardDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	//special:not to make m_slider 0
	m_Edit.SetBPR(m_slider.GetPos());
	m_Edit.RedrawWindow();

	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CCommWizardDlg::OnChangeSendstring() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	CEdit* pEditSend =(CEdit*)GetDlgItem(IDC_SENDSTRING);
	pEditSend->GetWindowText(m_strSendString);
	
}



void CAboutDlg::OnEditUndo() 
{
	// TODO: Add your command handler code here
	
}
