; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCommWizardDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "commwizard.h"
LastPage=0
CDK=Y

ClassCount=5
Class1=CCommWizardApp
Class2=CAboutDlg
Class3=CCommWizardDlg
Class4=CHexEdit
Class5=CSettingDlg

ResourceCount=4
Resource1=IDD_COMMWIZARD_DIALOG
Resource2=IDD_ABOUTBOX
Resource3=CG_IDR_POPUP_HEX_EDIT
Resource4=IDD_DIALOG1

[CLS:CCommWizardApp]
Type=0
BaseClass=CWinApp
HeaderFile=CommWizard.h
ImplementationFile=CommWizard.cpp
LastObject=CCommWizardApp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=CommWizardDlg.cpp
ImplementationFile=CommWizardDlg.cpp
LastObject=CAboutDlg
Filter=D
VirtualFilter=dWC

[CLS:CCommWizardDlg]
Type=0
BaseClass=CDialog
HeaderFile=CommWizardDlg.h
ImplementationFile=CommWizardDlg.cpp
LastObject=CCommWizardDlg

[CLS:CHexEdit]
Type=0
BaseClass=CEdit
HeaderFile=HexEdit.h
ImplementationFile=HexEdit.cpp
LastObject=CHexEdit
Filter=W
VirtualFilter=WC

[CLS:CSettingDlg]
Type=0
BaseClass=CDialog
HeaderFile=SettingDlg.h
ImplementationFile=SettingDlg.cpp
LastObject=ID_EDIT_SELECT_ALL

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_COMMWIZARD_DIALOG]
Type=1
Class=CCommWizardDlg
ControlCount=17
Control1=IDC_STATIC,button,1342177287
Control2=IDC_SEND,button,1342275584
Control3=IDC_RECEIVE,button,1342275584
Control4=IDC_SETTINGS,button,1342275584
Control5=IDC_CLEAR,button,1342275584
Control6=IDC_TYPE,combobox,1344339970
Control7=IDC_EDIT_RECEIVE,edit,1353777156
Control8=IDC_COMMCTRL,{648A5600-2C6E-101B-82B6-000000000014},1342242816
Control9=IDC_MANUALSEND,button,1342275584
Control10=IDC_ADDRESS_CHECK,button,1342242819
Control11=IDC_HEX_CHECK,button,1342242819
Control12=IDC_ASCII_CHECK,button,1342242819
Control13=IDC_48_CHECK,button,1342242819
Control14=IDC_SLIDER1,msctls_trackbar32,1342242840
Control15=IDC_SENDSTRING,edit,1350631552
Control16=IDC_STATIC,button,1342177287
Control17=IDC_STATIC,button,1342177287

[DLG:IDD_DIALOG1]
Type=1
Class=CSettingDlg
ControlCount=10
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,button,1342177287
Control4=IDC_STATIC,static,1342308352
Control5=IDC_COM,edit,1350631552
Control6=IDC_STATIC,static,1342308352
Control7=IDC_COMSETTING,edit,1350631552
Control8=IDC_STATIC,static,1342308352
Control9=IDC_TIME,edit,1350631552
Control10=IDC_STATIC,static,1342308352

[MNU:CG_IDR_POPUP_HEX_EDIT]
Type=1
Class=?
Command1=ID_EDIT_UNDO
Command2=ID_EDIT_CUT
Command3=ID_EDIT_COPY
Command4=ID_EDIT_PASTE
Command5=ID_EDIT_CLEAR
Command6=ID_EDIT_SELECT_ALL
CommandCount=6

