// CommWizardDlg.h : header file
//
//{{AFX_INCLUDES()
#include "mscomm.h"
//}}AFX_INCLUDES

#include "HexEdit.h"

#if !defined(AFX_COMMWIZARDDLG_H__7AA90EC4_0429_4FD9_8932_F02710DFFDC7__INCLUDED_)
#define AFX_COMMWIZARDDLG_H__7AA90EC4_0429_4FD9_8932_F02710DFFDC7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CCommWizardDlg dialog

class CCommWizardDlg : public CDialog
{
// Construction
public:
	char CCommWizardDlg::ConvertHexChar(char ch);
	int  String2Hex(CString str, CByteArray &senddata);
	void OpenPort();
	CCommWizardDlg(CWnd* pParent = NULL);	// standard constructor

	BOOL bSend;
	BOOL bReceive;

	int m_nOutPutMode;

	int		m_nPort;
	CString	m_strSettings;
	CString	m_strSendString;

	int		m_nTime;
// Dialog Data
	//{{AFX_DATA(CCommWizardDlg)
	enum { IDD = IDD_COMMWIZARD_DIALOG };
	CHexEdit	m_Edit;
	CSliderCtrl	m_slider;
	CButton	m_ctrlReceive;
	CButton	m_ctrlSend;
	CComboBox	m_ctrlDataType;
	CMSComm	m_Com;
	BOOL	m_48;
	BOOL	m_hex;
	BOOL	m_address;
	BOOL	m_ascii;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommWizardDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CCommWizardDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnReceive();
	afx_msg void OnSend();
	afx_msg void OnSettings();
	afx_msg void OnSelchangeType();
	afx_msg void OnClear();
	afx_msg void OnOnCommCommctrl();
	afx_msg void OnManualsend();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnHexCheck();
	afx_msg void On48Check();
	afx_msg void OnAddressCheck();
	afx_msg void OnAsciiCheck();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnChangeSendstring();
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMWIZARDDLG_H__7AA90EC4_0429_4FD9_8932_F02710DFFDC7__INCLUDED_)
