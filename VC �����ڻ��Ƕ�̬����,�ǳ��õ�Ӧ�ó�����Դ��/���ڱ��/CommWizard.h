// CommWizard.h : main header file for the COMMWIZARD application
//

#if !defined(AFX_COMMWIZARD_H__A7BE4AC1_3982_49CF_9941_97AC310123B7__INCLUDED_)
#define AFX_COMMWIZARD_H__A7BE4AC1_3982_49CF_9941_97AC310123B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCommWizardApp:
// See CommWizard.cpp for the implementation of this class
//

class CCommWizardApp : public CWinApp
{
public:
	CCommWizardApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommWizardApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCommWizardApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMWIZARD_H__A7BE4AC1_3982_49CF_9941_97AC310123B7__INCLUDED_)
