// MscommDemo.h : main header file for the MSCOMMDEMO application
//

#if !defined(AFX_MSCOMMDEMO_H__ADF1F3C5_37E9_4F96_9349_F5130F79D9F9__INCLUDED_)
#define AFX_MSCOMMDEMO_H__ADF1F3C5_37E9_4F96_9349_F5130F79D9F9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMscommDemoApp:
// See MscommDemo.cpp for the implementation of this class
//

class CMscommDemoApp : public CWinApp
{
public:
	CMscommDemoApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMscommDemoApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMscommDemoApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MSCOMMDEMO_H__ADF1F3C5_37E9_4F96_9349_F5130F79D9F9__INCLUDED_)
