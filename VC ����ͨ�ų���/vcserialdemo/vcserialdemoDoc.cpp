// MscommDemoDoc.cpp : implementation of the CMscommDemoDoc class
//

#include "stdafx.h"
#include "MscommDemo.h"

#include "MscommDemoDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMscommDemoDoc

IMPLEMENT_DYNCREATE(CMscommDemoDoc, CDocument)

BEGIN_MESSAGE_MAP(CMscommDemoDoc, CDocument)
	//{{AFX_MSG_MAP(CMscommDemoDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMscommDemoDoc construction/destruction

CMscommDemoDoc::CMscommDemoDoc()
{
	// TODO: add one-time construction code here

}

CMscommDemoDoc::~CMscommDemoDoc()
{
}

BOOL CMscommDemoDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMscommDemoDoc serialization

void CMscommDemoDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMscommDemoDoc diagnostics

#ifdef _DEBUG
void CMscommDemoDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMscommDemoDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMscommDemoDoc commands
