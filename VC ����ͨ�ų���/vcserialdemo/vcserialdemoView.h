// MscommDemoView.h : interface of the CMscommDemoView class
//
/////////////////////////////////////////////////////////////////////////////
#include "mscomm.h"

#if !defined(AFX_MSCOMMDEMOVIEW_H__3967D242_DADC_4B80_96B0_DD89FEC938D6__INCLUDED_)
#define AFX_MSCOMMDEMOVIEW_H__3967D242_DADC_4B80_96B0_DD89FEC938D6__INCLUDED_

#include "mscomm.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMscommDemoView : public CView
{
protected: // create from serialization only
	CMscommDemoView();
	DECLARE_DYNCREATE(CMscommDemoView)

// Attributes
public:
	CMscommDemoDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMscommDemoView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	void SendData();
	CMSComm m_Comm;
	virtual ~CMscommDemoView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMscommDemoView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnComm();
    DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MscommDemoView.cpp
inline CMscommDemoDoc* CMscommDemoView::GetDocument()
   { return (CMscommDemoDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MSCOMMDEMOVIEW_H__3967D242_DADC_4B80_96B0_DD89FEC938D6__INCLUDED_)
