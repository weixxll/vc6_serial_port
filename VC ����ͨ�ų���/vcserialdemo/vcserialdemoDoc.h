// MscommDemoDoc.h : interface of the CMscommDemoDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MSCOMMDEMODOC_H__C5687058_1198_4F3E_8B0F_14F63D7FD247__INCLUDED_)
#define AFX_MSCOMMDEMODOC_H__C5687058_1198_4F3E_8B0F_14F63D7FD247__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMscommDemoDoc : public CDocument
{
protected: // create from serialization only
	CMscommDemoDoc();
	DECLARE_DYNCREATE(CMscommDemoDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMscommDemoDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMscommDemoDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMscommDemoDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MSCOMMDEMODOC_H__C5687058_1198_4F3E_8B0F_14F63D7FD247__INCLUDED_)
