// MscommDemoView.cpp : implementation of the CMscommDemoView class
//

#include "stdafx.h"
#include "MscommDemo.h"

#include "MscommDemoDoc.h"
#include "MscommDemoView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMscommDemoView

IMPLEMENT_DYNCREATE(CMscommDemoView, CView)

BEGIN_MESSAGE_MAP(CMscommDemoView, CView)
	//{{AFX_MSG_MAP(CMscommDemoView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()


BEGIN_EVENTSINK_MAP(CMscommDemoView, CView)
    //{{AFX_EVENTSINK_MAP(CAboutDlg)
	ON_EVENT(CMscommDemoView,IDC_MSCOMM,1,OnComm,VTS_NONE)    
    //}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMscommDemoView construction/destruction

CMscommDemoView::CMscommDemoView()
{
	// TODO: add construction code here

}

CMscommDemoView::~CMscommDemoView()
{
}

BOOL CMscommDemoView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMscommDemoView drawing

void CMscommDemoView::OnDraw(CDC* pDC)
{
	CMscommDemoDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CMscommDemoView printing

BOOL CMscommDemoView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMscommDemoView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMscommDemoView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CMscommDemoView diagnostics

#ifdef _DEBUG
void CMscommDemoView::AssertValid() const
{
	CView::AssertValid();
}

void CMscommDemoView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMscommDemoDoc* CMscommDemoView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMscommDemoDoc)));
	return (CMscommDemoDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMscommDemoView message handlers

int CMscommDemoView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	DWORD style=WS_VISIBLE|WS_CHILD;
	if(!m_Comm.Create(NULL,style,CRect(0,0,0,0),this,IDC_MSCOMM))
	{
		TRACE0("Failed to create OLE Communication Control!\n");
		return -1;
	}

    m_Comm.SetCommPort(1);
    m_Comm.SetInputMode(1);
	m_Comm.SetSettings("9600,n,8,1");
	m_Comm.SetRThreshold(1);
	m_Comm.SetInputLen(0);
	if(!m_Comm.GetPortOpen())
	{
		m_Comm.SetPortOpen(TRUE);
	}
    m_Comm.GetInput();

	return 0;
}
//接收数据
void CMscommDemoView::OnComm()
{
    VARIANT m_input;
    char *str,*str1;
    int k,nEvent,i;
    CString str2,m_RcvData;
    nEvent=m_Comm.GetCommEvent();
	switch(nEvent)
	{
	case 2:
	   k=m_Comm.GetInBufferCount();     //接收缓冲区的字符数目
	   if(k>0)
	   {
		   m_input=m_Comm.GetInput();
		   str=(char*)(unsigned char*)m_input.parray->pvData;
	   }
	   i=0;
	   str1=str;
	   while(i<k)
	   {
		   i++;
		   str1++;
	   }
	   *str1='\0';                               
       str2=(const char*)str;             //清除字符串中的不必要字符
       m_RcvData=(const char *)str;
	   }
	//数据显示处理
}
//发送数据
void CMscommDemoView::SendData()
{
	int i,Count;
	CString m_SendData;
    m_SendData="Hello!";  
	Count=m_SendData.GetLength();
	CByteArray m_Array;
	m_Array.RemoveAll();
	m_Array.SetSize(Count);
    for(i=0;i<Count;i++)
		m_Array.SetAt(i,m_SendData[i]);
	    m_Comm.SetOutput(COleVariant(m_Array));
}
