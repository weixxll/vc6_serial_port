////////////////////////////////////////////////////////////////////////////////////////
// 
//	Project:    Terminal
//	File:       main.cpp
//	Start date: 17.11.1997
//	Update:     01.07.1998
//	Version:    1.2
//	Author:     Patrick Feuser pat@das-netz.de Germany
//	Copyright � 1997. Alle Rechte vorbehalten
//
////////////////////////////////////////////////////////////////////////////////////////

#include <conio.h>     // for _getch()
#include <stdio.h>     // for printf()
#include "comaccess.h" 

void main(int argc, char **argv)
{
	ComAccess  *com;

	char       buffer[256+1];

	int        read_len,
	           read_result, 
	           write_result;


	printf("\nClass ComAccess Test Application Ver. 1.2\n"
	       "Copyright � 1997 all Rights reserved\n"
		   "by Patrick Feuser pat@das-netz.de Germany\n\n");


	//
	// check nums arguments
	//
	if ( argc < 4 )
	{
		printf("Usage:\n"
		       "comaccess comX stringToSend lenStringToReceive\n"
			   "Press any key!\n");
		_getch();
		return;
	}


	//
	// construct the communication device object
	//
	if ( ! (com = new ComAccess()) )
	{
		printf("Error: Not enough memory to create ComAccess object!\n");
		return;
	}


	//
	// open and init the communication device with the passed com number
	//
	if ( ! com->Open(argv[1]) )
	{
		printf("Error: Can't open communication device!\n"
		       "%s", com->GetErrorMessage());
		delete com;
		return;
	}


	//
	// write the passed string to the communication device
	//
	write_result = com->WriteData(argv[2], strlen(argv[2]));

	// -1 ? then we got an error and print it
	if ( write_result < 0 )
		printf(com->GetErrorMessage());


	//
	// read incoming data if exist
	//

	buffer[0] = '\0';

	// make sure not to overload buffer
	read_len = ( (read_len=abs(atoi(argv[3]))) > 256 ) ? 256 : read_len; 

	read_result = com->ReadData(buffer, read_len);

	// -1 ? then we got an error and print it
	if ( (read_result < 0)  )
		printf(com->GetErrorMessage());
		
	// set end of received data
	buffer[read_len] = '\0';


	//
	// display results
	//
	printf("%d bytes written\n"
	       "%d bytes read\n"    
	       "%s", 
		   write_result, read_result, buffer);


	// close communication device
	com->Close();

	// destruct the communication device object and free memory 
	delete com;
}
