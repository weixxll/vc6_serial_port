unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, TeEngine, Series, ExtCtrls, TeeProcs, Chart,
  OleCtrls, MSCommLib_TLB;

type
  TForm1 = class(TForm)
    tbshtSignalChart: TPageControl;
    tbshtDebug: TTabSheet;
    rchdtSerialIn: TRichEdit;
    cmbbxSerialIn: TComboBox;
    Label7: TLabel;
    btRecStart: TButton;
    btRecStop: TButton;
    cmbbxBaudIn: TComboBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    btRecSave: TButton;
    cmbbxTypeIn: TComboBox;
    cmbbxSerialOut: TComboBox;
    Label13: TLabel;
    cmbbxBaudOut: TComboBox;
    rchdtSerialOut: TRichEdit;
    btSendOut: TButton;
    dtCode1: TEdit;
    lbCode1: TLabel;
    lbCode2: TLabel;
    dtCode3: TEdit;
    cmbbxTypeOut: TComboBox;
    Label16: TLabel;
    Label51: TLabel;
    tmrSerialIn: TTimer;
    MSCommOut: TMSComm;
    MSCommIn: TMSComm;
    dtCode2: TEdit;
    dtCode4: TEdit;
    lbCode3: TLabel;
    lbCode4: TLabel;
    ImgTrace: TImage;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    lbShowAd0: TLabel;
    lbShowAd1: TLabel;
    lbShowAd4: TLabel;
    lbShowAd5: TLabel;
    lbShowAd2: TLabel;
    lbShowGps5: TLabel;
    lbShowGps1: TLabel;
    lbShowAd7: TLabel;
    lbShowAd6: TLabel;
    lbShowAd3: TLabel;
    Label37: TLabel;
    lbShowGps6: TLabel;
    lbShowGps4: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    lbShowGps0: TLabel;
    btSaveTrace: TButton;
    btLoadTrace: TButton;
    btSaveData: TButton;
    Label1: TLabel;
    lbShowModel: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    lbShowPara0: TLabel;
    lbShowPara1: TLabel;
    lbShowPara2: TLabel;
    lbShowPara3: TLabel;
    lbShowPara4: TLabel;
    lbShowPara5: TLabel;
    lbShowPara6: TLabel;
    lbShowPara7: TLabel;
    Label49: TLabel;
    lbShowGps2: TLabel;
    Label57: TLabel;
    lbShowGps3: TLabel;
    Label59: TLabel;
    cmbbxModel: TComboBox;
    btSetModel: TButton;
    cmbbxParaNo: TComboBox;
    Label38: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    dtParaValue: TEdit;
    btADInit: TButton;
    btSetPara: TButton;
    Label62: TLabel;
    rdgrpDataCode: TRadioGroup;
    dlgSaveData: TSaveDialog;
    dlgLoadTrace: TOpenDialog;
    dlgSaveTrace: TSaveDialog;
    rchdtTmp: TRichEdit;
    Label2: TLabel;
    TabSheet1: TTabSheet;
    chrtAcc: TChart;
    chrtGyro: TChart;
    srsGyroY: TFastLineSeries;
    srsGyroX: TFastLineSeries;
    srsGyroZ: TFastLineSeries;
    srsAcc1: TFastLineSeries;
    srsAcc2: TFastLineSeries;
    chrtHeight: TChart;
    srsHeight: TFastLineSeries;
    Label12: TLabel;
    btCharClear: TButton;
    tbshtSignal: TTabSheet;
    procedure FormCreate(Sender: TObject);
    procedure btRecStartClick(Sender: TObject);
    procedure btRecStopClick(Sender: TObject);
    procedure btRecSaveClick(Sender: TObject);
    procedure btSendOutClick(Sender: TObject);
    procedure tmrSerialInTimer(Sender: TObject);
    procedure btSetModelClick(Sender: TObject);
    procedure btSetParaClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btSaveTraceClick(Sender: TObject);
    procedure btLoadTraceClick(Sender: TObject);
    procedure btADInitClick(Sender: TObject);
    procedure btSaveDataClick(Sender: TObject);
    procedure btCharClearClick(Sender: TObject);
    procedure dtCode1Change(Sender: TObject);
    procedure dtCode2Change(Sender: TObject);
    procedure dtCode3Change(Sender: TObject);
    procedure dtCode4Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    InitADDataFlag:Boolean;    
  end;
 /////////////////////////////////////////////////////////////////
{
串口接受线程
}
  TSerialInThread =class(TThread)
  private
        SerialInput:Variant;           //保存该线程从串口获取得到的数据矩阵
        Seriallength : integer;        //该线程需要处理的数据的个数
        SerialStr:String;              //用于保存处理后数据的字符串变量

  protected
        procedure Execute; override;    //需要重载
        procedure GetData;              //从串口获取数据的过程
        procedure TransToStr;              //转换为适合显示的字符串的过程
        procedure PostData ;            //显示在用户界面的过程
  end;
  /////////////////////////////////////////////////////////////////
  { 
  串口发送线程
  }
  TSerialOutThread =class(TThread)
  private
        DataOrCode:Boolean;             //是否要求输出指令，False表明输出数据
        SerialOutput:String;            //发送数据时的数据矩阵
        Seriallength : integer;         //发送数据时，数据长度
        tmpVar:Variant;                 //发送指令时的数据矩阵
        CheckOk:Boolean;                //用户确定的数据或是指令是否有错误

  protected
        procedure Execute; override;    //需要重载
        procedure GetData;              //从用户界面获取需要输出的数据或是指令代码
        procedure PackData;             //根据协议打包数据
        procedure PackCode;             //根据协议打包指令
        procedure PostData;             //发送给串口控件，实现数据的发送                
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
const
        GpsDataCountMax : integer = 20;         //GPS数据包中数据的个数
        ParaDataCountMax: integer = 9;          //参数数据包中数据的个数
        ADDataCountMax:integer =16;             //传感器数据包中数据的个数

var
        {
        实现线程互斥的句柄。
        }
        hMutex:THandle =0;                      //串口接受数据的线程使用的互斥句柄
        hMutex2:THandle =0;                      //串口发送数据的线程使用的互斥句柄
        {
        用于指令上下传的全局变量
        }
        ReceiveCharFlag:Boolean;                 //是否以字符接受串口，False表示以数据接受
        ParaDataFlag:Boolean;                  //正在截取参数数据包的标志
        GPSDataFlag:Boolean;                    //正在截取GPS数据包的标志
        ADDataFlag:Boolean;                     //正在截取传感器数据包的标志

        ParaNewDataFlag:Boolean;                //截取完参数的标志
        GPSNewDataFlag:Boolean;                 //截取完GPS数据包的标志
        ADNewDataFlag:Boolean;                  //截取完传感器数据包的标志

        ParaDataCount:Integer;                  //截取参数数据包的计数器
        ADDataCount:Integer;                    //截取传感器数据包的计数器
        GpsDataCount:Integer;                   //截取GPS数据包的计数器
        
        DownStr:String;                         //用于保存串口下传的所有数据
        GpsStr:String;                          //保存GPS数据包的数据
        ADStr:String;                           //保存传感器数据包的数据

        ParaDataArray:array[0..8] of Byte;      //接受参数数据包的数组
        GpsDataArray:array[0..19] of Byte;      //接受GPS数据包的数组
        ADDataArray:array[0..15] of Byte;       //接受传感器数据包的数组
        ADDataDblArray:array[0..7] of double;   //传感器实际的采样值        
        ADDataArrayMean:array[0..13] of Byte;   //保存传感器静态均值的数组。
        　　　　　　　　　　　　　　　　　　　　//不考虑系统电压。　

        ParaStrArray:array[0..3] of String;     //显示参数数据的字符串数组
        GpsStrArray:array[0..6] of String;      //显示GPS数据的字符串数组
        ADStrArray:array[0..7] of String;       //显示传感器的字符串数组
        {
        用于指令上下传的全局变量
        }
        CodeOutStr:array[1..4] of String;       //当前上传的指令代码
        CodeInStr:array[0..3] of String;        //下传返回的指令代码      
        {
        用于绘图的全局变量
        }
        gpsMeta: TMetaFile;                             //用于保存轨迹图像的对象
        posXArray,posYArray: array of double;           //保存当前飞行轨迹点的数组
        posXArrayLoad,posYArrayLoad: array of double;   //保存载入的轨迹点的数组
        posLength: Integer;                             //当前飞行轨迹点的总数
        posLengthLoad: Integer;                         //当前飞行轨迹点的总数

      {
      函数hex()是将0～f字符转换为0～15的整数。
      }
      function hex(c:String):Integer ;
       var
         x:integer;
       begin
          if (c='0') or (c=' ') then
            x:=0
          else if c='1' then
            x:=1
          else if c = '2' then
            x:=2
          else if c='3' then
            x:=3
          else if c='4' then
            x:=4
          else if c='5' then
            x:=5
          else if c='6' then
            x:=6
          else if c='7' then
            x:=7
          else if c='8' then
            x:=8
          else if c='9' then
            x:=9
         else if ((c='a') or (c='A')) then
            x:=10
         else if (c='b') or (c='B') then
            x:=11
         else if (c='c') or (c='C') then
            x:=12
         else if (c='d') or (c='D') then
            x:=13
         else if (c='e') or (c='E') then
            x:=14
         else if (c='f') or (c='F') then
            x:=15
         else
            x:=-1;
          Result:=x;
       end;

       function HexToInt(S:String): Integer;
       var
          tmpInt1,tmpInt2:Integer ;
       begin
          if Length(S)=1 then
          begin
             Result:=hex(S[1]);
          end
          else if Length(S)=2 then
          begin
            tmpInt1:=hex(S[1]);
            tmpInt2:=hex(S[2]);
            if (tmpInt1=-1) or (tmpInt2=-1) then
               Result:=-1
            else
               Result:= tmpInt1*16+tmpInt2;
          end
          else
               Result:=-1;
       end;        

function GetMaxValue(x,y,x1,y1: Array of Double;len,len1: Integer): double;
var
  i: Integer;
begin
  result := 0;
  for i:=0 to len-1 do
  begin
    if abs(x[i])>result then
      result := abs(x[i]);
    if abs(y[i]) > result then
      result := abs(y[i]);
  end;
  for i:=0 to len1-1 do
  begin
    if abs(x1[i])>result then
      result := abs(x1[i]);
    if abs(y1[i]) > result then
      result := abs(y1[i]);
  end;
end;
/////////////////////////////////////////////////////////////////
{   绘制飞行轨迹的函数

     红色轨迹是载入的，
     蓝色的轨迹是当前轨迹点
     
 1.  posXArray，posYArray 是当前轨迹点的坐标，
     posLength是当前轨迹点的个数
 2.  posXArrayLoad，posYArrayLoad 是载入的轨迹点的坐标，
     posLengthLoad是载入的轨迹点的个数
     
}
procedure DrawAxis;
var
  i,x,y,wid,h: Integer;
  pstep,ratio,maxV : double;
  can: TMetaFileCanvas;
begin
  can := TMetaFileCanvas.Create(gpsMeta,0);             //建立一个画布对象
  gpsMeta.Width := Form1.ImgTrace.Width;                //确定画布对应图像的大小
  gpsMeta.Height := Form1.ImgTrace.Height;
  wid := gpsMeta.Width;
  h := gpsMeta.Height;
  {
  确定直角坐标的最大值，取10,100,200,500,1000,2000,5000,10000,50000,100000 的最小值
  缺省状态取 100
  }
  maxV := GetMaxValue(posXArray,posYArray,posXArrayLoad,posYArrayLoad,posLength,posLengthLoad);
  if maxV = 0 then
    maxV := 100;
  if maxV <= 10 then
  begin
    maxV := 10;
  end
  else if maxV <= 100 then
  begin
    maxV := 100;
  end
  else if maxV <= 200 then
    maxV := 200
  else if maxV<= 500 then
    maxV := 500
  else if maxV <= 1000 then
  begin
    maxV := 1000;
  end
  else if maxV<= 2000 then
    maxV := 2000
  else if maxV<= 5000 then
    maxV := 5000
  else if maxV <= 10000 then
  begin
    maxV := 10000;
  end
  else if maxV<= 50000 then
    maxV := 50000
  else if maxV <=100000 then
  begin
    maxV := 100000;
  end
  else
  begin
    maxV := 1000000;
  end;
  can.Pen.Color := clBlack;
  can.Pen.Style :=  psSolid;
  can.MoveTo(0,h div 2);
  can.LineTo(wid,h div 2);
  can.MoveTo(wid div 2,0);
  can.LineTo(wid div 2, h);
  can.Font.Height := 10;
  can.Font.Name := 'Arial';
  {
  绘制坐标的最大刻度值
  }
  can.TextOut(0,h div 2 +1,IntToStr(-1*round(maxV)));
  can.TextOut(wid-can.TextWidth(IntToStr(round(maxV))),
                h div 2 +1,IntToStr(round(maxV)));
  can.TextOut(wid div 2+3,
                h-can.TextHeight(IntToStr(-1*round(maxV))),IntToStr(-1*round(maxV)));
  can.TextOut(wid div 2+1,0,IntToStr(round(maxV)));
  {
  绘制坐标的刻度，每个座标轴上有21条刻线
  }
  pstep := wid / 20;
  for i:=0 to 21 do
  begin
    can.MoveTo(round(pstep*i),h div 2 - 2);
    can.LineTo(round(pstep*i),h div 2);
    can.MoveTo(wid div 2,round(pstep*i));
    can.LineTo(wid div 2 + 2, round(pstep*i));
  end;
  {
  绘制载入的轨迹，颜色为红色
  }
  if posLengthLoad > 0 then
  begin
    can.Pen.Color := clRed;
    ratio := h / maxV / 2;
    x := round(posXArrayLoad[0]*ratio + wid/2);
    y := round(h/2-posYArrayLoad[0]*ratio);
    can.MoveTo(x,y);
    can.Pixels[x,y] := clRed;
    for i:= 1 to posLengthLoad-1 do
    begin
      x := round(posXArrayLoad[i]*ratio + wid/2);
      y := round(h/2 - posYArrayLoad[i]*ratio);
      can.LineTo(x,y);
    end;
  end;
  {
  绘制当前试验的轨迹，颜色为蓝色
  }
  if posLength > 0 then
  begin
    can.Pen.Color := clBlue;
    ratio := h / maxV / 2;
    x := round(posXArray[0]*ratio + wid/2);
    y := round(h/2-posYArray[0]*ratio);
    can.MoveTo(x,y);
    can.Pixels[x,y] := $0000ff;
    for i:= 1 to posLength-1 do
    begin
      x := round(posXArray[i]*ratio + wid/2);
      y := round(h/2 - posYArray[i]*ratio);
      can.LineTo(x,y);
    end;
    can.RoundRect(x,y,x+6,y+6,x-6,y-6);//在模型飞机的当前位置绘制一个较小的圆
  end;
  can.Free; //释放 TMetaFileCanvas对象，把绘制的图像传递给gpsMeta
  Form1.ImgTrace.Picture.Assign(gpsMeta);//显示轨迹图像
end;
/////////////////////////////////////////////////////////////////
{
从用户界面的读取发送的数据和指令，保存到线程的内部变量或是全局变量中
}
procedure TSerialOutThread.GetData ;
begin
      DataOrCode:=(Form1.rdgrpDataCode.ItemIndex=1);
      Seriallength:=Length(Form1.RchdtSerialOut.Text);
      SerialOutput:=Form1.RchdtSerialOut.Text;
      CodeOutStr[1]:=Form1.dtCode1.Text ;
      CodeOutStr[2]:=Form1.dtCode2.Text;
      CodeOutStr[3]:=Form1.dtCode3.Text;
      CodeOutStr[4]:=Form1.dtCode4.Text;
end;
/////////////////////////////////////////////////////////////////
{
   用户在编辑栏中输入格式为01_12_34_56_...
   首字符不能空格，必须把高位的0补上，
   即确定了输出16进制的数据0x01,0x12,0x34,0x56,...
}
procedure TSerialOutThread.PackData;
var
     tmpStr:String;
     MaxCount,Count,tmpInt,i:Integer;
begin
     Count:=1;   //动态Byte类型矩阵tmpVar的j记数变量
     MaxCount:=1;//动态Byte类型矩阵tmpVar的大小
     CheckOk:=True;
     {使用互斥实现线程的同步，主要是为了防止数据流顺序的发生错误}
     if WaitForSingleObject(hMutex2,InFinite) =WAIT_OBJECT_0 then
     begin
        if Seriallength >0 then
        begin
           i:=1;
           tmpVar:=VarArrayCreate([1,1],varByte);
           while(i<Seriallength) do
           begin
              tmpStr:=Copy(SerialOutput,i,2);
              tmpStr:=LowerCase(tmpStr);
              tmpInt:=HexToInt(tmpStr);
              if tmpInt<>-1 then
              begin
                 if Count=(MaxCount+1) then
                 begin
                   MaxCount:=MaxCount+1;
                   VarArrayRedim(tmpVar,MaxCount);
                 end;
                 tmpVar[Count]:=tmpInt;
                 Count:=Count+1;
              end else
              begin
                  CheckOk:=False;//数据格式有问题，无法解读
                  break;         //停止while循环
              end;
              i:=i+3;//跳过3个字符
           end;
        end;
     end;
     ReleaseMutex(hMutex2);
end;
/////////////////////////////////////////////////////////////////
{
需要按照协议打包数据，发送几个0x55，然后是0x24,指令代码，结束符号0x0d,0x0a
}
procedure TSerialOutThread.PackCode;
var
    tmpInt,i:Integer;
begin
    tmpVar:=VarArrayCreate([1,22],varByte);
    CheckOk:=True;
    if WaitForSingleObject(hMutex2,InFinite) =WAIT_OBJECT_0 then
    begin
         for i:=1 to 15  do
         begin
                tmpVar[i]:=85;//0x55
            end;
            tmpVar[10]:=12;
            tmpVar[16]:=36;   //0x24=36
            for i:=1 to 4 do
            begin
                tmpInt:=HexToInt(CodeOutStr[i]);
                if tmpInt<>-1 then
                  tmpVar[16+i]:= tmpInt
                else
                begin
                   CheckOk:=False;
                   Break;
                end;
            end;
            tmpVar[21]:=13;//0x0d
            tmpVar[22]:=10;//0x0a
    End;
    ReleaseMutex(hMutex2);                   
end;
/////////////////////////////////////////////////////////////////
{
由于有调试目的，如果要求发送字符格式的数据，就直接发送用户输入的字符串
其他情况下，都是发送打包好的数据矩阵
}
procedure TSerialOutThread.PostData ;
begin
     if CheckOk=True then
     begin
//         Form1.MSCommOut.Output:=tmpVar;     //以16进制发送数据或指令
         Form1.MSCommIn.Output:=tmpVar;     //以16进制发送数据或指令
     end
     else
          ShowMessage('数据格式有问题,发送失败');  //一旦发现有任何错误，都不会发送数据
     Form1.btSendOut.Enabled:=True;             //串口发送线程即将结束，运行新的发送
end;
/////////////////////////////////////////////////////////////////
{
	调用其它函数实现发送串口数据的功能。
        注意函数GetData还没有实现线程的互斥，
       目的是把发送的数据保存在各个线程中
}
procedure TSerialOutThread.Execute;
begin
     FreeOnTerminate:=True; //线程执行完后自动释放资源
//     Synchronize(GetData);
     GetData;
     if DataOrCode=True then
        PackData //发送数据
     else
        PackCode;//发送指令
     {
          提交给串口控件实现数据的发送
          尝试调用Synchronize
     }
//    Synchronize(PostData);
     PostData;
end;
///////////////////////////////////////////////////////////////// 
     {
     保存串口数据，便于后面进行处理
     }
procedure TSerialInThread.GetData ;
begin
      Seriallength:=Form1.MSCommIn.InBufferCount;
      if ReceiveCharFlag then                  //以文本格式接受串口数据
      begin
          SerialStr:=Form1.MSCommIn.Input;
      end
      else
      begin
          SerialInput:=Form1.MSCommIn.Input;     //以数据格式接受串口数据
          SerialStr:='';
      end;
end;
/////////////////////////////////////////////////////////////////
     {
     根据下传的数据转换为适合于显示的字符串形式
     }
procedure TSerialInThread.TransToStr ;
var
    i:Integer;
    tmpSInt,tmpSInt1:SmallInt;
    tmpDbl:double;
begin
    if ADNewDataFlag =True then
    begin
       ADDataDblArray[0]:=0.182*(5.0/4095.0)*((15 and ADDataArray[0])*256+ADDataArray[1]-(15 and ADDataArrayMean[0])*256-ADDataArrayMean[1]);
       ADStrArray[0]:=FloatToStrF(ADDataDblArray[0],ffFixed,4,3);//陀螺X
       ADDataDblArray[1]:=0.182*(5.0/4095.0)*((15 and ADDataArray[2])*256+ADDataArray[3]-(15 and ADDataArrayMean[2])*256-ADDataArrayMean[3]);
       ADStrArray[1]:=FloatToStrF(ADDataDblArray[1],ffFixed,4,3);//陀螺Y
       ADDataDblArray[2]:=0.182*(5.0/4095.0)*((15 and ADDataArray[4])*256+ADDataArray[5]-(15 and ADDataArrayMean[4])*256-ADDataArrayMean[5]);
       ADStrArray[2]:=FloatToStrF(ADDataDblArray[2],ffFixed,4,3);//陀螺Z
       ADDataDblArray[3]:=(5.0/4095.0)*((15 and ADDataArray[6])*256+ADDataArray[7]-(15 and ADDataArrayMean[6])*256-ADDataArrayMean[7]);
       ADStrArray[3]:=FloatToStrF(ADDataDblArray[3],ffFixed,4,3);//加速度0
       ADDataDblArray[4]:=(5.0/4095.0)*((15 and ADDataArray[8])*256+ADDataArray[9]-(15 and ADDataArrayMean[8])*256-ADDataArrayMean[9]);
       ADStrArray[4]:=FloatToStrF(ADDataDblArray[4],ffFixed,4,3);//加速度1
       ADDataDblArray[5]:=(5.0/4095.0)*((15 and ADDataArray[10])*256+ADDataArray[11]-(15 and ADDataArrayMean[10])*256-ADDataArrayMean[11])/0.046;
       ADStrArray[5]:=FloatToStrF(ADDataDblArray[5],ffFixed,4,3);//高度计
       ADDataDblArray[6]:=(5.0/4095.0)*((15 and ADDataArray[12])*256+ADDataArray[13]-(15 and ADDataArrayMean[12])*256-ADDataArrayMean[13]);
       ADStrArray[6]:=FloatToStrF(ADDataDblArray[6],ffFixed,4,3);//空速计
       ADDataDblArray[7]:=0.00244*((15 and ADDataArray[14])*256+ADDataArray[15]);
       ADStrArray[7]:=FloatToStrF(ADDataDblArray[7],ffFixed,4,3);//系统电压
    end;
    if GpsNewDataFlag =True then
    begin
        GpsStrArray[0]:=IntToStr(256*GpsDataArray[0]+GpsDataArray[1]);//是否定位
        GpsStrArray[1]:=IntToStr(GpsDataArray[3])+':'
                +IntToStr(GpsDataArray[4])+':'+IntToStr(GpsDataArray[5]);//时间
        tmpSInt:=GpsDataArray[6]*256+GpsDataArray[7];
        GpsStrArray[2]:=IntToStr(tmpSInt);           //速度,有符号16进制整数
        tmpSInt:=GpsDataArray[8]*256+GpsDataArray[9];
        GpsStrArray[3]:=IntToStr(tmpSInt);           //方位 ,有符号16进制整数
        tmpDbl:=0.1*(GpsDataArray[10]*256+GpsDataArray[11]);//高度,无符号16进制整数
        GpsStrArray[4]:=FloatToStrF(tmpDbl,ffFixed,4,1);;
        tmpSInt:=GpsDataArray[12]*256+GpsDataArray[13];//动向 x，有符号16进制整数
        tmpSInt1:=GpsDataArray[14]*256+GpsDataArray[15];//北向 y，有符号16进制整数
        GpsStrArray[5]:=IntToStr(tmpSInt);
        GpsStrArray[6]:=IntToStr(tmpSInt1);        
        {
        由于传输系统总会有一定的吴码率，那么为了避免轨迹图出现问题，例如坐标范围过大
        需要除去粗大误差，考虑到模型飞机的飞行速度不会超过10m/s，考虑到数子传输系统
        的下传速度，设定如果位置变化超过20m就去掉。
        }
        if (abs(tmpSInt-posXArray[posLength-1])<20) and (abs(tmpSInt1-posYArray[posLength-1])<20) then
        begin
            posXArray[posLength] := tmpSInt;
            posYArray[posLength] := tmpSInt1;
            inc(posLength);
            DrawAxis;
            if posLength = Length(posXArray) then
            begin
                SetLength(posXArray,posLength+1000);
                SetLength(posYArray,posLength+1000);
            end;
        end;
        CodeInStr[0]:=LowerCase(IntToHex(GpsDataArray[16],2));
        CodeInStr[1]:=LowerCase(IntToHex(GpsDataArray[17],2));
        CodeInStr[2]:=LowerCase(IntToHex(GpsDataArray[18],2));
        CodeInStr[3]:=LowerCase(IntToHex(GpsDataArray[19],2));
   end;
end;
///////////////////////////////////////////////////////////////// 
{
    显示数据   
}
procedure TSerialInThread.PostData ;
var
    tmpDbl:double;
    i:integer;
begin
    DownStr:=DownStr+SerialStr;               //保存串口的数据到全局变量中
//    Form1.RchdtSerialIn.Text:=SerialStr;     //文本只是显示最新的数据
    Form1.RchdtSerialIn.Text:=Form1.RchdtSerialIn.Text+chr(13)+char(10)+SerialStr;//显示下传的所有数据
    {
    如果数据量比较大，执行完上面的代码后，控件总是会调整后显示最初的文本。
    为了将最新的数据显示给用户，这里调用了控件的Perform函数，使得控件显
    示当前插入符所在的位置，而事先设置插入符的位置为上一次接受文本的末尾。
    }
    if Seriallength>0 then
    begin
        Form1.RchdtSerialIn.SelStart := Length(Form1.RchdtSerialIn.Text)-Seriallength;
        Form1.RchdtSerialIn.Perform(EM_SCROLLCARET, 0, 0);
    end;        
    if ADNewDataFlag =True then//显示新的传感器数据包
    begin
       ADNewDataFlag:=False;
       Form1.lbShowAd0.Caption:=ADStrArray[0];
       Form1.lbShowAd1.Caption:=ADStrArray[1];
       Form1.lbShowAd2.Caption:=ADStrArray[2];
       Form1.lbShowAd3.Caption:=ADStrArray[3];
       Form1.lbShowAd4.Caption:=ADStrArray[4];
       Form1.lbShowAd5.Caption:=ADStrArray[5];
       Form1.lbShowAd6.Caption:=ADStrArray[6];
       Form1.lbShowAd7.Caption:=ADStrArray[7];

       Form1.srsGyroX .AddY(ADDataDblArray[0]);       
       Form1.srsGyroY .AddY(ADDataDblArray[1]);
       Form1.srsGyroZ .AddY(ADDataDblArray[2]);
       Form1.srsAcc1 .AddY(ADDataDblArray[3]);
       Form1.srsAcc2 .AddY(ADDataDblArray[4]);
       Form1.srsHeight .AddY(ADDataDblArray[5]);
   end;
   if GpsNewDataFlag =True then //显示新的GPS数据包
   begin
       GpsNewDataFlag:=False;
       Form1.lbShowGps0.Caption:=GpsStrArray[0];
       Form1.lbShowGps1.Caption:=GpsStrArray[1];
       Form1.lbShowGps2.Caption:=GpsStrArray[2];
       Form1.lbShowGps3.Caption:=GpsStrArray[3];
       Form1.lbShowGps4.Caption:=GpsStrArray[4];
       Form1.lbShowGps5.Caption:=GpsStrArray[5];
       Form1.lbShowGps6.Caption:=GpsStrArray[6];
       Form1.lbCode1.Caption:=CodeInStr[0];     //显示下传的4个指令代码
       Form1.lbCode2.Caption:=CodeInStr[1];
       Form1.lbCode3.Caption:=CodeInStr[2];
       Form1.lbCode4.Caption:=CodeInStr[3];
   end;
   if ParaNewDataFlag then      //显示新的参数数据包
   begin
       ParaNewDataFlag:=False;
       Form1.lbShowPara0.Caption:=IntToStr(ParaDataArray[0]);
       Form1.lbShowPara1.Caption:=IntToStr(ParaDataArray[1]);
       Form1.lbShowPara2.Caption:=IntToStr(ParaDataArray[2]);
       Form1.lbShowPara3.Caption:=IntToStr(ParaDataArray[3]);
       Form1.lbShowPara4.Caption:=IntToStr(ParaDataArray[4]);
       Form1.lbShowPara5.Caption:=IntToStr(ParaDataArray[5]);
       Form1.lbShowPara6.Caption:=IntToStr(ParaDataArray[6]);
       Form1.lbShowPara7.Caption:=IntToStr(ParaDataArray[7]);
       Form1.lbShowModel.Caption:=IntToHex(ParaDataArray[8],2);
   end;
end;

///////////////////////////////////////////////////////////////// 
{
这是执行数据接受，截取数据包的过程。调用其它函数获取数据、转换格式和进行显示
}
procedure TSerialInThread.Execute;
var
     tmpInt,i:Integer;
     curStr:String;
begin
     FreeOnTerminate:=True;     //线程执行完后自动释放资源
//     Synchronize(GetData);
     GetData;                   //获取串口数据
     if not ReceiveCharFlag then
     begin
        if WaitForSingleObject(hMutex,InFinite) =WAIT_OBJECT_0 then
        begin
           if Seriallength >0 then
           begin
             for i:= 0 to Seriallength-1 do  //逐一处理串口数据
             begin
                  tmpInt:=SerialInput[i];    //获取当前一个数据
                  curStr:=LowerCase(IntToHex(tmpInt,2));//转换为16进制的字符
                  SerialStr :=SerialStr + curStr+' ';   //便于显示和保存
                  {
                  发现参数数据包
                  }
                  if (curStr='fe') and  (not ADDataFlag)
                           and (not GpsDataFlag) and (not ParaDataFlag)then
                  begin
                        ParaDataFlag:=True;
                        ParaDataCount:=0;
                  end
                  {
                  发现GPS数据包
                  }
                  else if (curStr='fc') and  (not ADDataFlag)
                        and (not GpsDataFlag)and (not ParaDataFlag) then
                  begin
                        GpsDataFlag:=True;
                        GpsDataCount:=0;
                        GpsStr:=GpsStr+chr(13) + chr(10);//保存数据
                  end
                  {
                  发现传感器数据包
                  }
                  else if (curStr='fd') and (not GpsDataFlag)
                        and (not ADDataFlag)and (not ParaDataFlag) then
                  begin
                         ADDataFlag:=True;
                         ADDataCount:=0;
                         GpsNewDataFlag:=True;
                         ADNewDataFlag:=True;
                         ADStr:=ADStr+chr(13) + chr(10);//保存数据
                  end
                  {
                    截取参数数据包
                  }
                  else if ParaDataFlag then
                  begin
                         ParaDataArray[ParaDataCount]:=tmpInt;
                         ParaDataCount:=ParaDataCount+1;
                         if ParaDataCount=ParaDataCountMax then
                         begin
                               ParaDataFlag:=False;//截取结束
                               ParaNewDataFlag:=True;
                               ParaDataCount:=0;
                        end;
                  end
                 {
                  截取GPS数据包
                 }
                  else if GpsDataFlag then
                  begin
                         GpsDataArray[GpsDataCount]:=tmpInt;
                         GpsDataCount:=GpsDataCount+1;
                         GpsStr:=GpsStr+curStr+' ';
                         if GpsDataCount=GpsDataCountMax then
                         begin
                                GpsDataFlag:=False;//截取结束
                                GpsNewDataFlag:=True;
                                GpsDataCount:=0;
                         end;
                  end
                {
                  截取传感器数据包
                }
                  else if ADDataFlag then
                  begin
                         ADDataArray[ADDataCount]:=tmpInt;
                         ADDataCount:=ADDataCount+1;
                         ADStr:=ADStr+curStr+' ';
                         if ADDataCount=ADDataCountMax then
                         begin
                                ADDataFlag:=False;//截取结束
                                ADNewDataFlag:=True;
                                ADDataCount:=0;
                         end;
                  end;
                end;
           {
               处理成字符串
           }
           TransToStr;
          end;
        end;
     end;
           {
           显示数据
           }              
//           Synchronize(PostData);
     PostData;
     ReleaseMutex(hMutex);//释放互斥对象的句柄
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
     hMutex:=CreateMutex(nil,False,nil);  //创建互斥对象
     hMutex2:=CreateMutex(nil,False,nil);
     gpsMeta := TMetaFile.Create;
     posLength := 0;                      //初始化轨迹点数组
     SetLength(posXArray,1000);
     SetLength(posYArray,1000);
     posLengthLoad := 0;
     SetLength(posXArrayLOad,0);
     SetLength(posYArrayLoad,0);
     DrawAxis;
end;

procedure TForm1.btRecStartClick(Sender: TObject);
var
   i:Integer;

begin
   //初始化串口
   if MSCommIn.PortOpen then
       MSCommIn.PortOpen :=False;
   MSCommIn.CommPort :=cmbbxSerialIn.ItemIndex +1;
   MSCommIn.Settings :=cmbbxBaudIn.Items[cmbbxBaudIn.ItemIndex]+',n,8,1';
   {
   确定当前数据发送方式
   }
   if (cmbbxTypeIn.ItemIndex=1) then
   begin
           MSCommIn.InputMode  :=0;  //文本形式，只是在调试时
           ReceiveCharFlag :=True;
   end
   else
   begin
           MSCommIn.InputMode  :=1;  //数据形式，默认
           ReceiveCharFlag :=False;
   end;
   MSCommIn.PortOpen := True;

   //初始化保存数据的全局变量
   ADStr:='';
   GpsStr:='';
   DownStr:='';

   //初始化轨迹图像 ，原点为当前点
   posLength:=0;
   posXArray[posLength] := 0;
   posYArray[posLength] := 0;
   inc(posLength);
   DrawAxis;

   //初始化截取数据包的变量        
   ParaDataFlag:=False;
   ParaNewDataFlag:=False;
   ParaDataCount:=0;
   GPSDataFlag:=False;
   GPSNewDataFlag:=False;
   GpsDataCount:=0;
   ADDataFlag:=False;
   ADNewDataFlag:=False;
   ADDataCount:=0;
   for i:=0 to 13 do        //传感器未初试化
        ADDataArrayMean[i]:=0;
   InitADDataFlag:=False;
   
   //初始化参数数据显示
   lbShowPara0.Caption :='0';
   lbShowPara1.Caption :='0';
   lbShowPara2.Caption :='0';
   lbShowPara3.Caption :='0';
   lbShowPara4.Caption :='0';
   lbShowPara5.Caption :='0';
   lbShowPara6.Caption :='0';
   lbShowPara7.Caption :='0';

   //初始化GPS数据显示
   lbShowGps0.Caption :='0';
   lbShowGps1.Caption :='0';
   lbShowGps2.Caption :='0';
   lbShowGps3.Caption :='0';
   lbShowGps4.Caption :='0';
   lbShowGps5.Caption :='0';
   lbShowGps6.Caption :='0';

   //初始化传感器数据显示
   lbShowAd0.Caption :='0';
   lbShowAd1.Caption :='0';
   lbShowAd2.Caption :='0';
   lbShowAd3.Caption :='0';
   lbShowAd4.Caption :='0';
   lbShowAd5.Caption :='0';
   lbShowAd6.Caption :='0';
   lbShowAd7.Caption :='0';

   //初始化模式显示
   lbShowModel.Caption :='0';
   
   //初始化指令码显示
   lbCode1.Caption :='0';
   lbCode2.Caption :='0';
   lbCode3.Caption :='0';
   lbCode4.Caption :='0';
   dtCode1.Text :='0';
   dtCode2.Text :='0';
   dtCode3.Text :='0';
   dtCode4.Text :='0';

   rchdtSerialIn.Text :='';
   rchdtSerialOut.Text :='';
   tmrSerialIn .Enabled :=True;       //打开定时器
   btRecStop .Enabled :=True;         // 激活"停止接受"按钮
   btRecSave .Enabled :=False;       //关闭"保存数据"按钮
   btRecStart .Enabled :=False;     //关闭"开始接收"按钮
end;

procedure TForm1.btRecStopClick(Sender: TObject);
begin
   tmrSerialIn .Enabled :=False;   　//关闭定时器
   btRecStop .Enabled :=False;     　// 关闭"停止接受"按钮
   btRecSave .Enabled :=True;       　//"保存数据"按钮使能
   btRecStart .Enabled :=True;       //"开始接收"按钮使能
end;
/////////////////////////////////////////////////////////////////
{
        如果串口接受到数据，就保存ADStr,GpsStr,DownStr。便于分析机载系统
}        
procedure TForm1.btRecSaveClick(Sender: TObject);
var
   fileName:String;
   i:integer;
begin
    if Length(DownStr)<>0 then
    begin
        fileName := 'Exp';
        if dlgSaveData.Execute then//打开数据保存的对话框
        begin
            fileName:=dlgSaveData .FileName ;
            rchdtTmp .Text :=ADStr;
            rchdtTmp .Lines.SaveToFile(filename+'_AD_All'+'.txt');
            rchdtTmp .Text :=DownStr;
            rchdtTmp .Lines.SaveToFile(filename+'_Down'+'.txt');
            rchdtTmp .Text :=GpsStr;
            rchdtTmp .Lines.SaveToFile(filename+'_Gps'+'.txt');
            rchdtTmp .Text :='';
        end
     end
     else
        ShowMessage('没有数据!');     
end;

procedure TForm1.btSendOutClick(Sender: TObject);
begin
   if MSCommOut.PortOpen then
           MSCommOut.PortOpen :=False;
   MSCommOut.CommPort :=cmbbxSerialOut.ItemIndex +1;
   MSCommOut.Settings :=cmbbxBaudOut.Items[cmbbxBaudIn.ItemIndex]+',n,8,1';

　 //如果直接以文本形式发送数据，
   if (cmbbxTypeOut.ItemIndex=1) then
   begin
           MSCommOut.InputMode :=0;
           MSCommOut.PortOpen := True;
           MSCommOut.Output :=rchdtSerialOut.Text;
   end  else
  //使用串口线程发送数据      
   begin
           MSCommOut.InputMode  :=1;
           MSCommOut.PortOpen := True;
           btSendOut.Enabled :=False;
           TSerialOutThread.Create(False);
   end;
end;

procedure TForm1.tmrSerialInTimer(Sender: TObject);
begin
        TSerialInThread.Create(False);
end;

procedure TForm1.btSetModelClick(Sender: TObject);
begin
        dtCode1.Text :=cmbbxModel.Items[cmbbxModel.ItemIndex];
        dtCode2.Text :='11';
        dtCode3.Text :='66';
        dtCode4.Text :='66';
        btSendOutClick(Sender);
end;

procedure TForm1.btSetParaClick(Sender: TObject);
begin
        dtCode1.Text :='66';
        dtCode2.Text :='11';
        dtCode3.Text :=cmbbxParaNo.Items[cmbbxParaNo.ItemIndex];
        dtCode4.Text :=IntToHex(StrToInt(dtParaValue.Text),2);
        btSendOutClick(Sender);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
        CloseHandle(hMutex);
        CloseHandle(hMutex2);
        gpsMeta.Free;
end;

procedure TForm1.btSaveTraceClick(Sender: TObject);
var
  str: TStrings;
  temp,filename: String;
  i : Integer;

begin
  if dlgSaveTrace.Execute then
  begin
    filename := dlgSaveTrace.FileName;
    str := TStringList.Create;
    for i:=0 to posLength-1 do
    begin
      temp := FloatToStr(posXArray[i])+','+FloatToStr(posYArray[i]);
      str.Add(temp);
    end;
    str.SaveToFile(filename);
    str.Free;
    posLength := 0;
    SetLength(posXArray,1000);
    SetLength(posYArray,1000);    
end;
end;
////////////////////////////////////////////////////////////////////////////////
{
  载入原来保存的飞行轨迹点的文本      
}
procedure TForm1.btLoadTraceClick(Sender: TObject);
var
  str: TStrings;
  temp,filename: String;
  i,st : Integer;
begin
  if dlgLoadTrace.Execute then
  begin
    str := TStringList.Create;
    filename := dlgLoadTrace.FileName;
    str.LoadFromFile(filename);
    posLengthLoad := str.Count;              //轨迹点的个数
    setLength(posXArrayLoad,str.Count);      //设置数据的大小
    setLength(posYArrayLoad,str.Count);
    for i:=0 to str.Count-1 do
    begin
      st := pos(',',str[i]);
      temp := copy(str[i],0,st-1);               //截取X坐标值
      posXArrayLoad[i] := StrToFloat(temp);
      temp := copy(str[i],st+1,length(str[i])-st);
      posYArrayLoad[i] := StrToFloat(temp);       //截取Y坐标值
    end;
    str.Free;
    DrawAxis;
  end;
end;

procedure TForm1.btADInitClick(Sender: TObject);
var
    i:integer;
begin
    {
    如果已经初试化
    }
    if InitADDataFlag then
    begin
        InitADDataFlag:=False;
        btADInit.Caption:='传感器初始化';
        for i:=0 to 13 do
            ADDataArrayMean[i]:=0;
    end else
    {
    如果未初试化
    }
    begin
        InitADDataFlag:=True;    
        btADInit.Caption:='放弃初始化';
        for i:=0 to 13 do
            ADDataArrayMean[i]:=ADDataArray[i];
    end;
end;

procedure TForm1.btSaveDataClick(Sender: TObject);
begin
        btRecSaveClick(Sender);
end;

procedure TForm1.btCharClearClick(Sender: TObject);
begin
        srsGyroX.Clear;
        srsGyroY.Clear;
        srsGyroZ.Clear;
        srsAcc1.Clear;
        srsAcc2.Clear;
        srsHeight.Clear;
end;

procedure TForm1.dtCode1Change(Sender: TObject);
begin
    if Length(dtCode1.Text)>2 then
       dtCode1.Text:='ff';
end;

procedure TForm1.dtCode2Change(Sender: TObject);
begin
    if Length(dtCode2.Text)>2 then
       dtCode2.Text:='ff';
end;

procedure TForm1.dtCode3Change(Sender: TObject);
begin
    if Length(dtCode3.Text)>2 then
       dtCode3.Text:='ff';
end;

procedure TForm1.dtCode4Change(Sender: TObject);
begin
    if Length(dtCode4.Text)>2 then
       dtCode4.Text:='ff';
end;

end.
