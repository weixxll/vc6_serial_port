object Form1: TForm1
  Left = 349
  Top = 120
  Width = 467
  Height = 572
  Caption = #27169#22411#39134#26426#27979#25511#24179#21488
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object tbshtSignalChart: TPageControl
    Left = 0
    Top = 0
    Width = 459
    Height = 545
    ActivePage = tbshtDebug
    Align = alClient
    TabIndex = 0
    TabOrder = 0
    object tbshtDebug: TTabSheet
      Caption = #20018#21475#35843#35797
      ImageIndex = 1
      object Label7: TLabel
        Left = 8
        Top = 48
        Width = 57
        Height = 13
        Caption = #36873#25321#20018#21475'   '
      end
      object Label8: TLabel
        Left = 120
        Top = 48
        Width = 45
        Height = 13
        Caption = #27874#29305#29575'   '
      end
      object Label9: TLabel
        Left = 248
        Top = 48
        Width = 87
        Height = 13
        Caption = #25968#25454#26174#31034#26684#24335'     '
      end
      object Label10: TLabel
        Left = 48
        Top = 16
        Width = 396
        Height = 13
        Caption = #65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#25509#25910#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293
      end
      object Label11: TLabel
        Left = 48
        Top = 272
        Width = 396
        Height = 13
        Caption = #65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#21457#36865#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293
      end
      object Label13: TLabel
        Left = 136
        Top = 304
        Width = 45
        Height = 13
        Caption = #27874#29305#29575'   '
      end
      object lbCode1: TLabel
        Left = 40
        Top = 424
        Width = 6
        Height = 13
        Caption = '0'
      end
      object lbCode2: TLabel
        Left = 112
        Top = 424
        Width = 6
        Height = 13
        Caption = '0'
      end
      object Label16: TLabel
        Left = 272
        Top = 304
        Width = 57
        Height = 13
        Caption = #21457#36865#26684#24335'   '
      end
      object Label51: TLabel
        Left = 16
        Top = 304
        Width = 57
        Height = 13
        Caption = #36873#25321#20018#21475'   '
      end
      object lbCode3: TLabel
        Left = 192
        Top = 424
        Width = 6
        Height = 13
        Caption = '0'
      end
      object lbCode4: TLabel
        Left = 264
        Top = 424
        Width = 6
        Height = 13
        Caption = '0'
      end
      object Label62: TLabel
        Left = 8
        Top = 424
        Width = 24
        Height = 13
        Caption = #25351#20196
      end
      object Label2: TLabel
        Left = 8
        Top = 408
        Width = 372
        Height = 13
        Caption = #65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293#65293
      end
      object rchdtSerialIn: TRichEdit
        Left = 0
        Top = 80
        Width = 449
        Height = 137
        Font.Charset = GB2312_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object cmbbxSerialIn: TComboBox
        Left = 72
        Top = 48
        Width = 41
        Height = 21
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 1
        Text = '1'
        Items.Strings = (
          '1'
          '2'
          '3')
      end
      object btRecStart: TButton
        Left = 48
        Top = 232
        Width = 75
        Height = 25
        Caption = #24320#22987#25509#25910
        TabOrder = 2
        OnClick = btRecStartClick
      end
      object btRecStop: TButton
        Left = 168
        Top = 232
        Width = 75
        Height = 25
        Caption = #20572#27490#25509#25910
        Enabled = False
        TabOrder = 3
        OnClick = btRecStopClick
      end
      object cmbbxBaudIn: TComboBox
        Left = 176
        Top = 48
        Width = 57
        Height = 21
        ItemHeight = 13
        TabOrder = 4
        Text = '2400'
        Items.Strings = (
          '1200'
          '2400'
          '4800'
          '9600')
      end
      object btRecSave: TButton
        Left = 296
        Top = 232
        Width = 75
        Height = 25
        Caption = #25968#25454#20445#23384
        Enabled = False
        TabOrder = 5
        OnClick = btRecSaveClick
      end
      object cmbbxTypeIn: TComboBox
        Left = 344
        Top = 48
        Width = 57
        Height = 21
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 6
        Text = 'Hex'
        Items.Strings = (
          'Hex'
          'Char')
      end
      object cmbbxSerialOut: TComboBox
        Left = 80
        Top = 304
        Width = 41
        Height = 21
        ItemHeight = 13
        ItemIndex = 1
        TabOrder = 7
        Text = '2'
        Items.Strings = (
          '1'
          '2'
          '3')
      end
      object cmbbxBaudOut: TComboBox
        Left = 192
        Top = 304
        Width = 57
        Height = 21
        ItemHeight = 13
        ItemIndex = 1
        TabOrder = 8
        Text = '2400'
        Items.Strings = (
          '1200'
          '2400'
          '4800'
          '9600')
      end
      object rchdtSerialOut: TRichEdit
        Left = 0
        Top = 336
        Width = 465
        Height = 65
        Font.Charset = GB2312_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
      end
      object btSendOut: TButton
        Left = 360
        Top = 472
        Width = 81
        Height = 25
        Caption = #21457#36865
        TabOrder = 10
        OnClick = btSendOutClick
      end
      object dtCode1: TEdit
        Left = 40
        Top = 448
        Width = 49
        Height = 21
        TabOrder = 11
        Text = '0'
        OnChange = dtCode1Change
      end
      object dtCode3: TEdit
        Left = 192
        Top = 448
        Width = 49
        Height = 21
        TabOrder = 12
        Text = '0'
        OnChange = dtCode3Change
      end
      object cmbbxTypeOut: TComboBox
        Left = 344
        Top = 304
        Width = 57
        Height = 21
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 13
        Text = 'Hex'
        Items.Strings = (
          'Hex'
          'Char')
      end
      object dtCode2: TEdit
        Left = 112
        Top = 448
        Width = 49
        Height = 21
        TabOrder = 14
        Text = '0'
        OnChange = dtCode2Change
      end
      object dtCode4: TEdit
        Left = 264
        Top = 448
        Width = 49
        Height = 21
        TabOrder = 15
        Text = '0'
        OnChange = dtCode4Change
      end
      object rdgrpDataCode: TRadioGroup
        Left = 360
        Top = 408
        Width = 81
        Height = 49
        Caption = #21457#36865
        ItemIndex = 0
        Items.Strings = (
          #25351#20196
          #25968#25454)
        TabOrder = 16
      end
      object rchdtTmp: TRichEdit
        Left = 8
        Top = 480
        Width = 305
        Height = 17
        Font.Charset = GB2312_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 17
        Visible = False
      end
    end
    object tbshtSignal: TTabSheet
      Caption = #26426#36733#20449#21495
      object ImgTrace: TImage
        Left = 0
        Top = 16
        Width = 209
        Height = 209
      end
      object Label17: TLabel
        Left = 16
        Top = 264
        Width = 68
        Height = 13
        Caption = #38464#34746'X(rad/s)  '
      end
      object Label18: TLabel
        Left = 16
        Top = 291
        Width = 68
        Height = 13
        Caption = #38464#34746'Y(rad/s)  '
      end
      object Label19: TLabel
        Left = 16
        Top = 373
        Width = 72
        Height = 13
        Caption = #21152#36895#24230'(m/s2)  '
      end
      object Label20: TLabel
        Left = 16
        Top = 401
        Width = 56
        Height = 13
        Caption = #39640#24230#35745'(m)  '
      end
      object Label21: TLabel
        Left = 16
        Top = 318
        Width = 68
        Height = 13
        Caption = #38464#34746'Z(rad/s)  '
      end
      object Label22: TLabel
        Left = 16
        Top = 346
        Width = 72
        Height = 13
        Caption = #21152#36895#24230'(m/s2)  '
      end
      object Label23: TLabel
        Left = 16
        Top = 428
        Width = 66
        Height = 13
        Caption = #31354#36895#35745'(m/s)  '
      end
      object Label24: TLabel
        Left = 16
        Top = 456
        Width = 70
        Height = 13
        Caption = #31995#32479#30005#21387'(V)   '
      end
      object Label25: TLabel
        Left = 320
        Top = 32
        Width = 48
        Height = 13
        Caption = #23450#20301#21542#12288
      end
      object Label26: TLabel
        Left = 320
        Top = 144
        Width = 24
        Height = 13
        Caption = #39640#24230
      end
      object lbShowAd0: TLabel
        Left = 112
        Top = 264
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowAd1: TLabel
        Left = 112
        Top = 291
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowAd4: TLabel
        Left = 112
        Top = 373
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowAd5: TLabel
        Left = 112
        Top = 401
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowAd2: TLabel
        Left = 112
        Top = 318
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowGps5: TLabel
        Left = 392
        Top = 172
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowGps1: TLabel
        Left = 392
        Top = 60
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowAd7: TLabel
        Left = 112
        Top = 456
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowAd6: TLabel
        Left = 112
        Top = 428
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowAd3: TLabel
        Left = 112
        Top = 346
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object Label37: TLabel
        Left = 216
        Top = 8
        Width = 223
        Height = 13
        Caption = #65293#65293#65293#65293#65293#22788#29702'Gps'#30340#25968#25454#65293#65293#65293#65293#65293#65293#65293
      end
      object lbShowGps6: TLabel
        Left = 392
        Top = 200
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowGps4: TLabel
        Left = 392
        Top = 144
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object Label53: TLabel
        Left = 320
        Top = 60
        Width = 36
        Height = 13
        Caption = #26102#38388#12288
      end
      object Label54: TLabel
        Left = 320
        Top = 172
        Width = 49
        Height = 13
        Caption = #19996#21521'_X'#12288
      end
      object Label55: TLabel
        Left = 320
        Top = 200
        Width = 49
        Height = 13
        Caption = #21271#21521'_Y'#12288
      end
      object lbShowGps0: TLabel
        Left = 392
        Top = 32
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object Label1: TLabel
        Left = 216
        Top = 240
        Width = 144
        Height = 13
        Caption = #65293#65293#65293#65293#26426#36733#21442#25968#65293#65293#65293#65293
      end
      object lbShowModel: TLabel
        Left = 256
        Top = 256
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object Label3: TLabel
        Left = 192
        Top = 256
        Width = 54
        Height = 13
        Caption = #25511#21046#27169#24335'  '
      end
      object Label4: TLabel
        Left = 200
        Top = 283
        Width = 42
        Height = 13
        Caption = #21442#25968'_0  '
      end
      object Label5: TLabel
        Left = 200
        Top = 310
        Width = 42
        Height = 13
        Caption = #21442#25968'_1  '
      end
      object Label6: TLabel
        Left = 200
        Top = 338
        Width = 42
        Height = 13
        Caption = #21442#25968'_2  '
      end
      object Label14: TLabel
        Left = 200
        Top = 365
        Width = 42
        Height = 13
        Caption = #21442#25968'_3  '
      end
      object Label15: TLabel
        Left = 200
        Top = 393
        Width = 42
        Height = 13
        Caption = #21442#25968'_4  '
      end
      object Label39: TLabel
        Left = 200
        Top = 420
        Width = 42
        Height = 13
        Caption = #21442#25968'_5  '
      end
      object Label40: TLabel
        Left = 200
        Top = 448
        Width = 42
        Height = 13
        Caption = #21442#25968'_6  '
      end
      object lbShowPara0: TLabel
        Left = 256
        Top = 283
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowPara1: TLabel
        Left = 256
        Top = 310
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowPara2: TLabel
        Left = 256
        Top = 338
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowPara3: TLabel
        Left = 256
        Top = 365
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowPara4: TLabel
        Left = 256
        Top = 393
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowPara5: TLabel
        Left = 256
        Top = 420
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowPara6: TLabel
        Left = 256
        Top = 448
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object lbShowPara7: TLabel
        Left = 256
        Top = 472
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object Label49: TLabel
        Left = 200
        Top = 472
        Width = 42
        Height = 13
        Caption = #21442#25968'_7  '
      end
      object lbShowGps2: TLabel
        Left = 392
        Top = 88
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object Label57: TLabel
        Left = 320
        Top = 88
        Width = 24
        Height = 13
        Caption = #36895#24230
      end
      object lbShowGps3: TLabel
        Left = 392
        Top = 116
        Width = 6
        Height = 13
        Caption = '0'
        Color = clBtnHighlight
        ParentColor = False
      end
      object Label59: TLabel
        Left = 320
        Top = 116
        Width = 24
        Height = 13
        Caption = #26041#20301
      end
      object Label38: TLabel
        Left = 8
        Top = 240
        Width = 144
        Height = 13
        Caption = #65293#65293#65293#20256#24863#22120#36755#20986#65293#65293#65293#65293
      end
      object Label60: TLabel
        Left = 352
        Top = 352
        Width = 72
        Height = 13
        Caption = #21442#25968#24207#21495'0'#65374'7'
      end
      object Label61: TLabel
        Left = 352
        Top = 408
        Width = 72
        Height = 13
        Caption = #21442#25968#20540'0'#65374'255'
      end
      object btSaveTrace: TButton
        Left = 224
        Top = 32
        Width = 75
        Height = 25
        Caption = #20445#23384#36335#24452
        TabOrder = 0
        OnClick = btSaveTraceClick
      end
      object btLoadTrace: TButton
        Left = 224
        Top = 64
        Width = 75
        Height = 25
        Caption = #36733#20837#36335#24452
        TabOrder = 1
        OnClick = btLoadTraceClick
      end
      object btSaveData: TButton
        Left = 224
        Top = 168
        Width = 81
        Height = 57
        Caption = #25968#25454#20445#23384
        TabOrder = 2
        OnClick = btSaveDataClick
      end
      object cmbbxModel: TComboBox
        Left = 352
        Top = 256
        Width = 57
        Height = 21
        ItemHeight = 13
        TabOrder = 3
        Text = '0'
        Items.Strings = (
          '00'
          '11'
          '22'
          '33'
          '44')
      end
      object btSetModel: TButton
        Left = 352
        Top = 288
        Width = 65
        Height = 25
        Caption = #35774#32622#27169#24335
        TabOrder = 4
        OnClick = btSetModelClick
      end
      object cmbbxParaNo: TComboBox
        Left = 352
        Top = 376
        Width = 57
        Height = 21
        ItemHeight = 13
        TabOrder = 5
        Text = '0'
        Items.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7')
      end
      object dtParaValue: TEdit
        Left = 352
        Top = 432
        Width = 57
        Height = 21
        TabOrder = 6
        Text = '0'
      end
      object btADInit: TButton
        Left = 32
        Top = 480
        Width = 97
        Height = 25
        Caption = #20256#24863#22120#21021#22987#21270
        TabOrder = 7
        OnClick = btADInitClick
      end
      object btSetPara: TButton
        Left = 352
        Top = 464
        Width = 65
        Height = 25
        Caption = #35774#32622#21442#25968
        TabOrder = 8
        OnClick = btSetParaClick
      end
    end
    object TabSheet1: TTabSheet
      Caption = #20449#21495#20998#26512
      ImageIndex = 2
      object Label12: TLabel
        Left = 8
        Top = 376
        Width = 60
        Height = 13
        Caption = #20256#24863#22120#39640#24230
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object chrtAcc: TChart
        Left = 0
        Top = 184
        Width = 449
        Height = 146
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.Maximum = 10
        LeftAxis.Minimum = -10
        Legend.Alignment = laLeft
        Legend.ShadowSize = 0
        Legend.TopPos = 3
        View3D = False
        TabOrder = 0
        object srsAcc1: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          Title = #21152#36895#24230'1'
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object srsAcc2: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clGreen
          Title = #21152#36895#24230'2'
          LinePen.Color = clGreen
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object chrtGyro: TChart
        Left = 0
        Top = 16
        Width = 449
        Height = 154
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'Gyro')
        Title.Visible = False
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.Maximum = 10
        LeftAxis.Minimum = -10
        Legend.Alignment = laLeft
        Legend.Font.Charset = ANSI_CHARSET
        Legend.Font.Color = clBlack
        Legend.Font.Height = -11
        Legend.Font.Name = 'Bookman Old Style'
        Legend.Font.Style = []
        Legend.ShadowSize = 0
        Legend.TopPos = 3
        View3D = False
        TabOrder = 1
        object srsGyroY: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clGreen
          Title = #38464#34746'X'
          LinePen.Color = clGreen
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object srsGyroX: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          Title = #38464#34746'Y'
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object srsGyroZ: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clYellow
          Title = #38464#34746'Z'
          LinePen.Color = clYellow
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object chrtHeight: TChart
        Left = 72
        Top = 352
        Width = 379
        Height = 161
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.Maximum = 200
        Legend.Alignment = laLeft
        Legend.ShadowSize = 0
        Legend.TopPos = 0
        Legend.Visible = False
        View3D = False
        TabOrder = 2
        object srsHeight: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          Title = 'srsHgt1'
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object btCharClear: TButton
        Left = 8
        Top = 416
        Width = 49
        Height = 25
        Caption = #28165#31354
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnClick = btCharClearClick
      end
    end
  end
  object MSCommOut: TMSComm
    Left = 424
    Top = 0
    Width = 32
    Height = 32
    ControlData = {
      2143341208000000ED030000ED03000001568A64000006000000010000040000
      00020000600900000000080000000000000000003F00000011000000}
  end
  object MSCommIn: TMSComm
    Left = 392
    Top = 0
    Width = 32
    Height = 32
    ControlData = {
      2143341208000000ED030000ED03000001568A64000006000000010000040000
      00020000600900000000080000000000000000003F00000011000000}
  end
  object tmrSerialIn: TTimer
    Enabled = False
    Interval = 350
    OnTimer = tmrSerialInTimer
    Left = 368
  end
  object dlgSaveData: TSaveDialog
    Filter = '*.txt('#25991#26412#31867#22411')|*.txt|*.*'#65288#25152#26377#31867#22411#65289'|*.*'
    Left = 336
  end
  object dlgLoadTrace: TOpenDialog
    Filter = '*.txt('#25991#26412#31867#22411')|*.txt|*.*'#65288#25152#26377#31867#22411#65289'|*.*'
    Left = 272
  end
  object dlgSaveTrace: TSaveDialog
    Filter = '*.txt('#25991#26412#31867#22411')|*.txt|*.*'#65288#25152#26377#31867#22411#65289'|*.*'
    Left = 304
  end
end
