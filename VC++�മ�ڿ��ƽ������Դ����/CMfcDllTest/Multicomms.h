// Multicomms.h: interface for the CMulticomms class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MULTICOMMS_H__493D88D8_FB4D_44CB_BE00_47D47EE58C3B__INCLUDED_)
#define AFX_MULTICOMMS_H__493D88D8_FB4D_44CB_BE00_47D47EE58C3B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define ID_COMMCTRL1 2001  // Ctrl ID for comm control
#define ID_COMMCTRL2 2002
#define ID_COMMCTRL3 2003
#define ID_COMMCTRL4 2004
#define ID_COMMCTRL5 2005

#define MaxPort  5  
#define uidisplaylength 24


class  CMSComm : public CWnd
{
protected:
	DECLARE_DYNCREATE(CMSComm)
public:
	CLSID const& GetClsid()
	{
		static CLSID const clsid
			= { 0x648a5600, 0x2c6e, 0x101b, { 0x82, 0xb6, 0x0, 0x0, 0x0, 0x0, 0x0, 0x14 } };
		return clsid;
	}
	virtual BOOL Create(LPCTSTR lpszClassName,
		LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect,
		CWnd* pParentWnd, UINT nID,
		CCreateContext* pContext = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID); }

    BOOL Create(LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect, CWnd* pParentWnd, UINT nID,
		CFile* pPersist = NULL, BOOL bStorage = FALSE,
		BSTR bstrLicKey = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID,
		pPersist, bStorage, bstrLicKey); }

// Attributes
public:
// Operations
public:
	void OnSendData(CString m_strInputData,int kind); 
	void InitComm(short nInBufferSize);
	void SetCDHolding(BOOL bNewValue);
	BOOL GetCDHolding();
	void SetCommID(long nNewValue);
	long GetCommID();
	void SetCommPort(short nNewValue);
	short GetCommPort();
	void SetCTSHolding(BOOL bNewValue);
	BOOL GetCTSHolding();
	void SetDSRHolding(BOOL bNewValue);
	BOOL GetDSRHolding();
	void SetDTREnable(BOOL bNewValue);
	BOOL GetDTREnable();
	void SetHandshaking(long nNewValue);
	long GetHandshaking();
	void SetInBufferSize(short nNewValue);
	short GetInBufferSize();
	void SetInBufferCount(short nNewValue);
	short GetInBufferCount();
	void SetBreak(BOOL bNewValue);
	BOOL GetBreak();
	void SetInputLen(short nNewValue);
	short GetInputLen();
	void SetNullDiscard(BOOL bNewValue);
	BOOL GetNullDiscard();
	void SetOutBufferSize(short nNewValue);
	short GetOutBufferSize();
	void SetOutBufferCount(short nNewValue);
	short GetOutBufferCount();
	void SetParityReplace(LPCTSTR lpszNewValue);
	CString GetParityReplace();
	void SetPortOpen(BOOL bNewValue);
	BOOL GetPortOpen();
	void SetRThreshold(short nNewValue);
	short GetRThreshold();
	void SetRTSEnable(BOOL bNewValue);
	BOOL GetRTSEnable();
	void SetSettings(LPCTSTR lpszNewValue);
	CString GetSettings();
	void SetSThreshold(short nNewValue);
	short GetSThreshold();
	void SetOutput(const VARIANT& newValue);
	VARIANT GetOutput();
	void SetInput(const VARIANT& newValue);
	VARIANT GetInput();
	void SetCommEvent(short nNewValue);
	short GetCommEvent();
	void SetEOFEnable(BOOL bNewValue);
	BOOL GetEOFEnable();
	void SetInputMode(long nNewValue);
	long GetInputMode();
};


//AFX_EXT_CLASS must be added! 2002.10.8
class AFX_EXT_CLASS CMulticomms  : public CWnd
{
public:
	CMulticomms();
	virtual ~CMulticomms();
public:
	BOOL bComExist[MaxPort];
	CMSComm m_Ports[MaxPort];
	
    CUIntArray ports,portse,portsu;
	char temp[100];
	BOOL bCommDisplay;
	unsigned short uirlength1[MaxPort];  //串口开始时的长度
	unsigned short uirlength2[MaxPort];  //串口定时判断中的长度

	BOOL OnCommsCreate(CWnd* pParentWnd);
	void OnCommSend(unsigned int uiCom, CString sSendData, unsigned int uiKind); 
	void OnMscommsSet();
	void OnHangUp(unsigned int uiCom); 
	void OnPortClose(unsigned int uiCom); 
	void OnPortOpen(unsigned int uiCom); 
	void OnRefreshDialogModeless(CString m_strReceive, CString m_strSend);
	void EnumerateSerialPorts(CUIntArray& ports,CUIntArray& portse,CUIntArray& portsu);
	BOOL SerialPortsCheck(int i);
	BOOL OnComportOpen();
	BOOL OnReadCommSets();
	void OnCloseAllPorts();
	void OnOpenAllPorts();
	VARIANT GetInput(unsigned int uiCom);
    short GetInBufferCount(unsigned int uiCom);
	BOOL OnComportTimerProcess(CString strProcess[MaxPort]);
	void OnCommportBinaryRead(BYTE data[], unsigned short uicom);
	BOOL OnPopupDialog(); 	

	BOOL OnGetPortUse(unsigned int uiCom);
	BOOL OnGetPortExit(unsigned int uiCom); 
};



#endif // !defined(AFX_MULTICOMMS_H__493D88D8_FB4D_44CB_BE00_47D47EE58C3B__INCLUDED_)
