// CMfcDllTestDoc.cpp : implementation of the CCMfcDllTestDoc class
//

#include "stdafx.h"
#include "CMfcDllTest.h"

#include "CMfcDllTestDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCMfcDllTestDoc

IMPLEMENT_DYNCREATE(CCMfcDllTestDoc, CDocument)

BEGIN_MESSAGE_MAP(CCMfcDllTestDoc, CDocument)
	//{{AFX_MSG_MAP(CCMfcDllTestDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCMfcDllTestDoc construction/destruction

CCMfcDllTestDoc::CCMfcDllTestDoc()
{
	// TODO: add one-time construction code here

}

CCMfcDllTestDoc::~CCMfcDllTestDoc()
{
}

BOOL CCMfcDllTestDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCMfcDllTestDoc serialization

void CCMfcDllTestDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCMfcDllTestDoc diagnostics

#ifdef _DEBUG
void CCMfcDllTestDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCMfcDllTestDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCMfcDllTestDoc commands
