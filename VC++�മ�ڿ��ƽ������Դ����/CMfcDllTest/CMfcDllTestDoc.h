// CMfcDllTestDoc.h : interface of the CCMfcDllTestDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CMFCDLLTESTDOC_H__1698C94B_B29A_48E8_ABCE_FE8AFA4450A8__INCLUDED_)
#define AFX_CMFCDLLTESTDOC_H__1698C94B_B29A_48E8_ABCE_FE8AFA4450A8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCMfcDllTestDoc : public CDocument
{
protected: // create from serialization only
	CCMfcDllTestDoc();
	DECLARE_DYNCREATE(CCMfcDllTestDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCMfcDllTestDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCMfcDllTestDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCMfcDllTestDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CMFCDLLTESTDOC_H__1698C94B_B29A_48E8_ABCE_FE8AFA4450A8__INCLUDED_)
