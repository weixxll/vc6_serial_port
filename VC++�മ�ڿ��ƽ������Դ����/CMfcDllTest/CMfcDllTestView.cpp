// CMfcDllTestView.cpp : implementation of the CCMfcDllTestView class
//

#include "stdafx.h"
#include "CMfcDllTest.h"

#include "CMfcDllTestDoc.h"
#include "CMfcDllTestView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCMfcDllTestView

IMPLEMENT_DYNCREATE(CCMfcDllTestView, CView)

BEGIN_MESSAGE_MAP(CCMfcDllTestView, CView)
	//{{AFX_MSG_MAP(CCMfcDllTestView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCMfcDllTestView construction/destruction

CCMfcDllTestView::CCMfcDllTestView()
{
	// TODO: add construction code here

}

CCMfcDllTestView::~CCMfcDllTestView()
{
}

BOOL CCMfcDllTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CCMfcDllTestView drawing

void CCMfcDllTestView::OnDraw(CDC* pDC)
{
	CCMfcDllTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CCMfcDllTestView printing

BOOL CCMfcDllTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CCMfcDllTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CCMfcDllTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CCMfcDllTestView diagnostics

#ifdef _DEBUG
void CCMfcDllTestView::AssertValid() const
{
	CView::AssertValid();
}

void CCMfcDllTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CCMfcDllTestDoc* CCMfcDllTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCMfcDllTestDoc)));
	return (CCMfcDllTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCMfcDllTestView message handlers
