// CMfcDllTest.h : main header file for the CMFCDLLTEST application
//

#if !defined(AFX_CMFCDLLTEST_H__50F37725_2E66_42C6_8916_8AD9C270EF3F__INCLUDED_)
#define AFX_CMFCDLLTEST_H__50F37725_2E66_42C6_8916_8AD9C270EF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCMfcDllTestApp:
// See CMfcDllTest.cpp for the implementation of this class
//

class CCMfcDllTestApp : public CWinApp
{
public:
	CCMfcDllTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCMfcDllTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCMfcDllTestApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CMFCDLLTEST_H__50F37725_2E66_42C6_8916_8AD9C270EF3F__INCLUDED_)
