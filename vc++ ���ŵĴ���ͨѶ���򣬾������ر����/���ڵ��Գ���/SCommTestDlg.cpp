// SCommTestDlg.cpp : implementation file
//
  
  
#include "stdafx.h"
#include "SCommTest.h"
#include "SCommTestDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//初始化串口，波特率，校验位，数据位，停止位分别为：k，i，j，m，n

int k=1; 
CString tempt=",";
CString i="9600";
CString j="N";
CString m="8";
CString n="1";
CString L=i+tempt+j+tempt+m+tempt+n;
CString Setings=_T(L);
int Timer=1000;//初始化自动发送时间间隔为1s
BOOL Light;//串口状态指示

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSCommTestDlg dialog

CSCommTestDlg::CSCommTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSCommTestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSCommTestDlg)
	m_strRXData = _T("");
	m_strTXData = _T("");
	m_Timer = 0;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSCommTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSCommTestDlg)
	DDX_Control(pDX, IDC_COMBO_ADJUST, m_Adjust);
	DDX_Control(pDX, IDC_COMBO_STOP, m_Stop);
	DDX_Control(pDX, IDC_COMBO_DATA, m_Data);
	DDX_Control(pDX, IDC_COMBO_BOTE, m_Bote);
	DDX_Control(pDX, IDC_COMBO_PORT, m_Port);
	DDX_Control(pDX, IDC_MSCOMM1, m_ctrlComm);
	DDX_Text(pDX, IDC_EDIT_RXDATA, m_strRXData);
	DDX_Text(pDX, IDC_EDIT_TXDATA, m_strTXData);
	DDX_Text(pDX, IDC_EDIT_Timer, m_Timer);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSCommTestDlg, CDialog)
	//{{AFX_MSG_MAP(CSCommTestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_MANUALSEND, OnButtonManualsend)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR_SEND, OnButtonClearSend)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR_RECEIVE, OnButtonClearReceive)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_AUTOSEND, OnCheckAutosend)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSCommTestDlg message handlers

BOOL CSCommTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here  
//参数添加	
m_Port.AddString("COM1");	
m_Port.AddString("COM2");	
m_Port.AddString("COM3");	
m_Port.AddString("COM4");	
m_Bote.AddString("300");
m_Bote.AddString("600");
m_Bote.AddString("1200");
m_Bote.AddString("2400");
m_Bote.AddString("4800");
m_Bote.AddString("9600");
m_Bote.AddString("19200");
m_Bote.AddString("38400");
m_Bote.AddString("43000");
m_Bote.AddString("56000");
m_Bote.AddString("57600");
m_Bote.AddString("115200");
m_Adjust.AddString("NULL");
m_Adjust.AddString("奇校验");
m_Adjust.AddString("偶校验");
m_Data.AddString("4");
m_Data.AddString("5");
m_Data.AddString("6");
m_Data.AddString("7");
m_Data.AddString("8");
m_Stop.AddString("1");
m_Stop.AddString("2");
m_Port.SetCurSel(0);   
m_Bote.SetCurSel(5);
m_Adjust.SetCurSel(0);
m_Data.SetCurSel(4);
m_Stop.SetCurSel(0);
m_bAutoSend=FALSE;// 初始化自动发送不允许
m_Timer=Timer;
Light=TRUE;
UpdateData(FALSE);	
return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSCommTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSCommTestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSCommTestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

BEGIN_EVENTSINK_MAP(CSCommTestDlg, CDialog)
    //{{AFX_EVENTSINK_MAP(CSCommTestDlg)
	ON_EVENT(CSCommTestDlg, IDC_MSCOMM1, 1 /* OnComm */, OnComm, VTS_NONE)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CSCommTestDlg::OnComm() 
{
	// TODO: Add your control notification handler code here
    VARIANT variant_inp;
    COleSafeArray safearray_inp;
    LONG len,k;
    BYTE rxdata[2048]; //设置BYTE数组 An 8-bit integer that is not signed.
    CString strtemp;
    if(m_ctrlComm.GetCommEvent()==2) //事件值为2表示接收缓冲区内有字符
    {             ////////以下你可以根据自己的通信协议加入处理代码
        variant_inp=m_ctrlComm.GetInput(); //读缓冲区
        safearray_inp=variant_inp; //VARIANT型变量转换为ColeSafeArray型变量
        len=safearray_inp.GetOneDimSize(); //得到有效数据长度
        for(k=0;k<len;k++)
            safearray_inp.GetElement(&k,rxdata+k);//转换为BYTE型数组
        for(k=0;k<len;k++) //将数组转换为Cstring型变量
        {
            BYTE bt=*(char*)(rxdata+k); //字符型
            strtemp.Format("%c",bt); //将字符送入临时变量strtemp存放
            m_strRXData+=strtemp; //加入接收编辑框对应字符串 
        }
    }
    UpdateData(FALSE); //更新编辑框内容
	
}

void CSCommTestDlg::OnButtonManualsend() 
{
	// TODO: Add your control notification handler code here
    //参数设定部分
	//设定端口
	if(m_Port.GetCurSel()==0)
		k=1;
	else if(m_Port.GetCurSel()==1)
		k=2;
    else if(m_Port.GetCurSel()==2)
		k=3;
    else k=4;
  //设定波特率
	if(m_Bote.GetCurSel()==0)
		i="300";
	else if(m_Bote.GetCurSel()==1)
		i="600";
    else if(m_Bote.GetCurSel()==2)
		i="1200";
	else if(m_Bote.GetCurSel()==3)
		i="2400";
    else if(m_Bote.GetCurSel()==4)
	    i="4800";
    else if(m_Bote.GetCurSel()==5)
		i="9600";
	else if(m_Bote.GetCurSel()==6)
		i="19200";
    else if(m_Bote.GetCurSel()==7)
		i="38400";
    else if(m_Bote.GetCurSel()==8)
		i="43000";
	else if(m_Bote.GetCurSel()==9)
	    i="56000";
    else if(m_Bote.GetCurSel()==10)
		i="57600";
	else i="15200";
//设定奇偶校验
	if(m_Adjust.GetCurSel()==0)
		j="N";
	else if(m_Adjust.GetCurSel()==1)
		j="O";
    else 
		j="E";
//设定数据位
	if(m_Data.GetCurSel()==0)
		m="4";
	else if(m_Data.GetCurSel()==1)
		m="5";
    else if(m_Data.GetCurSel()==2)
		m="6";
	else if(m_Data.GetCurSel()==3)
		m="7";
    else if(m_Data.GetCurSel()==4)
	    m="8";
//设定停止位
	if(m_Stop.GetCurSel()==0)
		n="1";
	else if(m_Stop.GetCurSel()==1)
		n="2";
    
CString L=i+tempt+j+tempt+m+tempt+n;
CString Setings=_T(L);

UpdateData(TRUE); //读取编辑框内容
UpdateData(FALSE);//显示编辑框中的内容，并将延时时间传给计时器

if(m_ctrlComm.GetPortOpen()) 
m_ctrlComm.SetPortOpen(FALSE);
m_ctrlComm.SetCommPort(k); //选择com(K)
if( !m_ctrlComm.GetPortOpen())
{
	m_ctrlComm.SetPortOpen(TRUE);//打开串口
    Light=TRUE;//指示灯亮
}
else
AfxMessageBox("不能够打开端口!");

m_ctrlComm.SetSettings(Setings); //波特率9600，无校验，8个数据位，1个停止位 

m_ctrlComm.SetInputMode(1); //1：表示以二进制方式检取数据
m_ctrlComm.SetRThreshold(1); 
//参数1表示每当串口接收缓冲区中有多于或等于1个字符时将引发一个接收数据的OnComm事件
m_ctrlComm.SetInputLen(0); //设置当前接收区数据长度为0
m_ctrlComm.GetInput();//先预读缓冲区以清除残留数据
m_ctrlComm.SetOutput(COleVariant(m_strTXData));//发送数据

}


void CSCommTestDlg::OnButtonClearSend() 
{
	// TODO: Add your control notification handler code here
    m_strTXData=" ";
    UpdateData(FALSE); //更新编辑框内容
}

void CSCommTestDlg::OnButtonClearReceive() 
{
	// TODO: Add your control notification handler code here
	m_strRXData=" ";
	UpdateData(FALSE); //更新编辑框内容
}

void CSCommTestDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
    OnButtonManualsend(); 
	CDialog::OnTimer(nIDEvent);
}


void CSCommTestDlg::OnCheckAutosend() 
{
	// TODO: Add your control notification handler code here
	m_bAutoSend=!m_bAutoSend;
    
	if(m_bAutoSend)
	{
     UpdateData(TRUE);
	 SetTimer(1,m_Timer,NULL);
	}
	else
	{
		KillTimer(1);
	}

}

HBRUSH CSCommTestDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH   hbr   =   CDialog::OnCtlColor(pDC,   pWnd,   nCtlColor);   
    
  if   ((pWnd->GetDlgCtrlID()   == IDC_Light)&&(Light==TRUE))   
  {   
     ::DeleteObject(hbr);   
     hbr   =   ::CreateSolidBrush(RGB(255,0,0));//控件底色   
     pDC->SetBkMode(TRANSPARENT);   
     pDC->SetTextColor(RGB(255,0,0));   
  }   
  
  return   hbr;   
}


