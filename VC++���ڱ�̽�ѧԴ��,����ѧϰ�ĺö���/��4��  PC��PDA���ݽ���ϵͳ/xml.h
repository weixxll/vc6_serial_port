#ifndef __XML__
#define __XML__

#include <afx.h>
#include <afxtempl.h>

//class COut_XML : public CObject
class COut_XML
{
public:
	COut_XML();
	~COut_XML();
	void Open(const CString xmlFileName, int &nOpened, CString rootName=_T("PdaComm"));
	int Is_open();
	void Close();

	void PutHead(const CString name);						
	void PutItem(const CString name, const CString value);	
	void PutTail(const CString name);						
	void PutRecordHead();									
	void PutRecordTail();									
	void PutFile(const CString fileName);					
private:
	CString tab();
	HANDLE m_hFile;
	int	m_level;
	CString m_rootName;
	void WriteLn(const CString s);
};

class CInFile : public CObject
{
public:
	CString		m_line;
	HANDLE m_hFile;

	BOOL		eof();

	CInFile(CString fileName);
	~CInFile();
	BOOL        isOpen();
	CString		getItem();
private:
	BOOL m_bEof;
	void getline();
};

class CXmlNode;

class CFindItem
{
public:
	CFindItem() {m_path=_T("");m_pNode=NULL;};
	CFindItem(CString Path, CXmlNode *PNode) { m_path=Path; m_pNode=PNode;};
	CString m_path;
	CXmlNode *m_pNode;
}; 

typedef CArray<CFindItem, CFindItem&> CXmlFindTable;

typedef CArray<CXmlNode, CXmlNode&> CSubNodes;

class CXmlNode : public CObject
{
public:
	// Constructors
	CXmlNode();
	CXmlNode(CString name, CString value=_T(""));
	CXmlNode(const CXmlNode& other);
	CXmlNode& operator = (const CXmlNode& other);

	// Destructor
	~CXmlNode();

	// Member functions
	BOOL Open(CString xmlFilename, CString rootName=_T("PdaComm"));
	void Close();
	CString GetFldValue(const CString fldName);
	void GetNodesTable(CXmlFindTable& table, const CString infoType, const CString recType);
	void GetNodesTable(CXmlFindTable& table, const CString infoType);
public:
	CString m_nodeName;
	CString m_nodeValue;
	CSubNodes m_subNodes;

private:
	void CopyFrom(const CXmlNode& other);
	BOOL LoadXMLNode(CInFile &inFile, CString head);
	CXmlNode* Locate(CString what);
	void FindNodes(CXmlFindTable& table, CString what, CString path, int level); 
	void RemoveAll();
};


typedef CArray<CString, CString&> CStringTable;

class CIn_XML : public CObject
{
public:
	CIn_XML();
	~CIn_XML();
	void	Open(const CString xmlFile, int &nOpened, CString rootName=_T("PdaComm"));
	int		Is_open();
	void	Close();
	BOOL	SaveTo(const CString xmlFile);

	int		FindRecords(const CString infoType, const CString recordType, int &RecordsCount);
	int		FindRecords(const CString infoType, int &RecordsCount);
	int		GetRecordsCount(int &RecordsCount);
	CString GetRecordPath(int recordNo, LPTSTR RecordPath);
	CString GetRecordPath(int recordNo);
	CString GetItemValue(int recordNo, const CString itemName, LPTSTR ItemValue);
	CString GetItemValue(int recordNo, const CString itemName);
	int		GetInfoTable(CStringTable &InfoTable);

	int		FindRecords(const CString infoType, const CString recordType);
	int		FindRecords(const CString infoType);
	CString GetFile(int recordNo);
	CString GetPath(int recordNo);
private:
	CXmlFindTable m_table;
	BOOL m_isOpen;
	void WriteNode(CXmlNode &Node, COut_XML *pOutXML);
public:
	CXmlNode m_root;
};

#endif __XML__