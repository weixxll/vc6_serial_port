#ifndef __COMMBASE__
#define __COMMBASE__


#include <afx.h>

#define RXQUEUE         8192
#define TXQUEUE         8192
// Flow control flags
#define FC_XONXOFF      0x04
// ascii definitions
#define ASCII_XON       0x11
#define ASCII_XOFF      0x13
#define STATUSMSG       WM_USER+1

#define PDAFILENAME		"MobileData.bin"
#define MAXBLOCK        0x1000

#define CSM_RUNNING		0
#define CSM_DONE		1
#define CSM_TIMEOUT		2
#define CSM_EXCEPTION	3
#define CSM_INITFAILED	4
#define CSM_DATAERROR	5
#define CSM_KILLED		6
#define CSM_SYNERROR    7

#define SWAPWORD(x)		MAKEWORD(HIBYTE((x)), LOBYTE((x)))
#define SWAPDWORD(x)	MAKELONG(HIWORD((x)), LOWORD((x)))

class CCommBase			
{
public:
	CCommBase();
	~CCommBase();

	BOOL	SetupDevice(HWND hMsgWnd, CString DeviceFile);
	void	Kill();
	BOOL	Read(CString Path,CString XmlFileName);
    BOOL	Write(CString Path,CString XmlFileName);

	virtual BOOL Device2Xml();
	virtual BOOL Xml2Device();

	HANDLE	m_idComDev;

	CString m_Path;
    CString m_PdaFileName; 
    CString m_XmlFileName; 
    BOOL    m_bConnected;
//    WORD    m_nOrder;

	DCB		m_dcb;

protected: 
	virtual BOOL Device2Bin();
	virtual BOOL Bin2Device();
	virtual BOOL Xml2Bin();
    virtual BOOL Bin2Xml();
	virtual BOOL OpenDevice();
	virtual BOOL CloseDevice();
	void SendCommMsg(WPARAM wStatus, LPARAM lRecNum);
    int		ReadCommBlock(LPSTR lpszBlock, int nMaxLength);
    BOOL	WriteCommBlock(LPSTR lpByte, DWORD dwBytesToWrite);
    BOOL	SetupConnection();
	void    SetPath(CString Path,CString XmlFileName);
    OVERLAPPED	m_osWrite, m_osRead ;
private:
    HWND	m_CallBackWnd;
	HANDLE  m_hThread;
	static DWORD ReadThread(LPVOID lpParameter);
	static DWORD WriteThread(LPVOID lpParameter);
public:
	CString m_DeviceFile;
};

#endif __COMMBASE__
