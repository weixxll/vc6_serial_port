#include "commBase.h"

CCommBase::CCommBase()
{
	m_Path="";
	m_PdaFileName="";
    m_XmlFileName=""; 
	m_DeviceFile="";
    m_hThread=NULL;
    m_CallBackWnd=NULL;

	memset((void *)&m_osWrite, 0, sizeof m_osWrite);
	memset((void *)&m_osRead, 0, sizeof m_osRead);
	
	m_dcb.DCBlength		= sizeof(m_dcb);
	m_dcb.BaudRate		= 9600;
	m_dcb.ByteSize		= 8 ;
	m_dcb.Parity		= NOPARITY ;
	m_dcb.StopBits		= ONESTOPBIT ;
	m_dcb.fOutxDsrFlow	= FALSE ;
	m_dcb.fOutxCtsFlow	= FALSE ;
	m_dcb.fDtrControl	= 0x1;
	m_dcb.fInX			= FALSE;
	m_dcb.fOutX			= FALSE;
	m_dcb.XonChar		= ASCII_XON ;
	m_dcb.XoffChar		= ASCII_XOFF ;
	m_dcb.XonLim		= 100 ;
	m_dcb.XoffLim		= 100 ;
	m_dcb.fBinary		= TRUE ;
	m_dcb.fParity		= TRUE ;
}

CCommBase::~CCommBase()
{
}

BOOL CCommBase::SetupDevice(HWND hMsgWnd, CString DeviceFile)
{
	m_CallBackWnd = hMsgWnd;
	m_DeviceFile = DeviceFile;
	return TRUE;   
}

BOOL CCommBase::SetupConnection(  )
{
   BOOL       fRetVal ;
   DCB        dcb ;

   dcb.DCBlength = sizeof( dcb ) ;
   GetCommState( m_idComDev, &dcb ) ;
   dcb.BaudRate		= m_dcb.BaudRate;
   dcb.ByteSize		= m_dcb.ByteSize;
   dcb.Parity		= m_dcb.Parity;
   dcb.StopBits		= m_dcb.StopBits;

   // setup hardware flow control
   dcb.fOutxDsrFlow = m_dcb.fOutxDsrFlow;
   dcb.fOutxCtsFlow = m_dcb.fOutxCtsFlow;
   dcb.fDtrControl  = m_dcb.fDtrControl;

   dcb.fInX			= m_dcb.fInX;
   dcb.fOutX		= m_dcb.fOutX;
   dcb.XonChar		= m_dcb.XonChar;
   dcb.XoffChar		= m_dcb.XoffChar;
   dcb.XonLim		= m_dcb.XonLim;
   dcb.XoffLim		= m_dcb.XoffLim;

   // other various settings
   dcb.fBinary		= m_dcb.fBinary;
   dcb.fParity		= m_dcb.fParity;
//	dcb.fDtrControl=DTR_CONTROL_ENABLE;

   fRetVal = SetCommState( m_idComDev, &dcb ) ;

   return fRetVal ;

}

void CCommBase::Kill()
{
	m_bConnected = FALSE;
//	Sleep(10);
//	while(m_hThread!=NULL);
	CloseDevice();
	SendCommMsg(CSM_KILLED,0);
}

void CCommBase::SetPath(CString Path,CString XmlFileName)
{
	m_Path=Path;
	if (m_Path.IsEmpty()) {
		m_PdaFileName=PDAFILENAME;
		m_XmlFileName=XmlFileName;
	} else {
		m_PdaFileName=m_Path+"\\"+PDAFILENAME;
		m_XmlFileName=m_Path+"\\"+XmlFileName;
	}
}

BOOL CCommBase::Read(CString Path,CString XmlFileName)
{
	DWORD ID;

	if (m_hThread!=NULL) return FALSE;

	SetPath(Path, XmlFileName);

	m_hThread = CreateThread(
		         (LPSECURITY_ATTRIBUTES)NULL,			// pointer to security attributes
		         0,										// initial thread stack size
		         (LPTHREAD_START_ROUTINE)ReadThread,	// pointer to thread function
		         (LPVOID)this,							// argument for new thread
		         0,										// creation flags
		         &ID									// pointer to receive thread ID
	);

	if(NULL==m_hThread)
	{
		DWORD err=GetLastError();
		return FALSE;
	}
	return TRUE;
}

BOOL CCommBase::Write(CString Path,CString XmlFileName)
{
	DWORD ID;

	if (m_hThread!=NULL) return FALSE;

	SetPath(Path, XmlFileName);

	m_hThread = CreateThread(
		         (LPSECURITY_ATTRIBUTES)NULL,			// pointer to security attributes
		         0,										// initial thread stack size
		         (LPTHREAD_START_ROUTINE)WriteThread,	// pointer to thread function
		         (LPVOID)this,							// argument for new thread
		         0,										// creation flags
		         &ID									// pointer to receive thread ID
	);

	if(NULL==m_hThread)
	{
		DWORD err=GetLastError();
		return FALSE;
	}
	
	return TRUE;
}

BOOL CCommBase::Device2Xml()
{
    if(TRUE==Device2Bin())
		return Bin2Xml();
	else return FALSE;
}

BOOL CCommBase::Xml2Device()
{
    if(TRUE==Xml2Bin())
		return Bin2Device();
	else return FALSE;
}

BOOL CCommBase::Device2Bin()
{
	return TRUE;
}

BOOL CCommBase::Bin2Device()
{
	return TRUE;
}

BOOL CCommBase::Xml2Bin()
{
	return TRUE;
}

BOOL CCommBase::Bin2Xml()
{
	return TRUE;
}	

int	CCommBase::ReadCommBlock(LPSTR lpszBlock, int nMaxLength)
{
   BOOL       fReadStat ;
   COMSTAT    ComStat ;
   DWORD      dwErrorFlags;
   DWORD      dwLength;
   DWORD      dwError;

   // only try to read number of bytes in queue
   BOOL ok=ClearCommError( m_idComDev, &dwErrorFlags, &ComStat ) ;
   dwLength = min( (DWORD) nMaxLength, ComStat.cbInQue ) ;

   if (dwLength > 0)
   {
	   fReadStat = ::ReadFile(m_idComDev,lpszBlock,dwLength,&dwLength,&m_osRead) ;
      if (!fReadStat)
      {
         if ((dwError=GetLastError()) == ERROR_IO_PENDING)
         {

            while(!GetOverlappedResult( m_idComDev,
               &m_osRead, &dwLength, TRUE ))
            {
               dwError = GetLastError();
               if(dwError == ERROR_IO_INCOMPLETE)
                  // normal result if not finished
                  continue;
               else
               {
                  // an error occurred, try to recover
                 ClearCommError( m_idComDev, &dwErrorFlags, &ComStat ) ;
                  break;
               }
            }
	      }
         else
         {
            // some other error occurred
            dwLength = 0 ;
            ClearCommError( m_idComDev, &dwErrorFlags, &ComStat ) ;
          }
      }
   }
   return dwLength ;
}

BOOL CCommBase::WriteCommBlock(LPSTR lpByte , DWORD dwBytesToWrite)
{
   BOOL        fWriteStat ;
   DWORD       dwBytesWritten ;
   DWORD       dwErrorFlags;
   DWORD       dwError;
   DWORD       dwBytesSent=0;
   COMSTAT     ComStat;

   fWriteStat = WriteFile( m_idComDev, lpByte, dwBytesToWrite,
                           &dwBytesWritten, &m_osWrite ) ;
   if (!fWriteStat)
   {
      if(GetLastError() == ERROR_IO_PENDING)
      {
         while(!GetOverlappedResult( m_idComDev,
            &m_osWrite, &dwBytesWritten, TRUE ))
         {
            dwError = GetLastError();
            if(dwError == ERROR_IO_INCOMPLETE)
            {
               // normal result if not finished
               continue;
            }
            else
            {
               // an error occurred, try to recover
               ClearCommError( m_idComDev, &dwErrorFlags, &ComStat ) ;
               break;
            }
         }
      }
      else
      {
         // some other error occurred
         ClearCommError( m_idComDev, &dwErrorFlags, &ComStat ) ;
         return FALSE ;
      }
   }
   return TRUE ;
}

DWORD CCommBase::ReadThread(LPVOID pComm)
{
	CCommBase *commBase=(CCommBase *)pComm;
	if(!commBase->OpenDevice())
	{
		commBase->CloseDevice();
		return FALSE;
	}
	if(!commBase->Device2Xml())
	{
		commBase->CloseDevice();
		return FALSE;
	}
	commBase->CloseDevice();
	commBase->SendCommMsg(CSM_DONE,0);
	commBase->m_hThread=NULL;
	return TRUE;
}

DWORD CCommBase::WriteThread(LPVOID pComm)
{
	CCommBase *commBase=(CCommBase *)pComm;
	if(!commBase->OpenDevice())
	{
		return FALSE;
	}
		commBase->CloseDevice();
	if(!commBase->Xml2Device())
	{
		commBase->CloseDevice();
		return FALSE;
	}
	commBase->CloseDevice();
	commBase->SendCommMsg(CSM_DONE,0);
	commBase->m_hThread=NULL;
	return TRUE;
}

BOOL CCommBase::OpenDevice()
{
   COMMTIMEOUTS  CommTimeOuts ;
   // open COMM device
   if ((m_idComDev =
      CreateFile( m_DeviceFile, GENERIC_READ | GENERIC_WRITE,
                  0,                    // exclusive access
                  NULL,                 // no security attrs
                  OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL |
                  FILE_FLAG_OVERLAPPED, // overlapped I/O
                  NULL )) == (HANDLE) -1 )
   {
	   SendCommMsg(CSM_INITFAILED,0);
       return (FALSE) ;
   }
   else
   {
	  m_osWrite.Offset = 0 ;
      m_osWrite.OffsetHigh = 0 ;
      m_osRead.Offset  = 0 ;
      m_osRead.OffsetHigh = 0 ;

      // setup device buffers
      SetupComm(m_idComDev, RXQUEUE, TXQUEUE ) ;

      // purge any information in the buffer
      PurgeComm(m_idComDev, PURGE_TXABORT | PURGE_RXABORT |
                            PURGE_TXCLEAR | PURGE_RXCLEAR ) ;
      // set up for overlapped I/O
      CommTimeOuts.ReadIntervalTimeout = 0xFFFFFFFF ;
      CommTimeOuts.ReadTotalTimeoutMultiplier = 0 ;
      CommTimeOuts.ReadTotalTimeoutConstant = 0;
      CommTimeOuts.WriteTotalTimeoutMultiplier = 0;
      CommTimeOuts.WriteTotalTimeoutConstant = 0 ;
      SetCommTimeouts(m_idComDev, &CommTimeOuts ) ;
   }
  
   m_bConnected=SetupConnection();
   if(!m_bConnected)
	   SendCommMsg(CSM_INITFAILED,0);
   return m_bConnected;
}

BOOL CCommBase::CloseDevice()
{

	   // set connected flag to FALSE
		m_bConnected = FALSE ;

	  // disable event notification 
	   SetCommMask(m_idComDev, 0 ) ;

	  // purge any outstanding reads/writes and close device handle
	   PurgeComm(m_idComDev, PURGE_TXABORT | PURGE_RXABORT |
                             PURGE_TXCLEAR | PURGE_RXCLEAR ) ;
	   CloseHandle( m_idComDev ) ;
	   m_idComDev=NULL;
	   return TRUE;
}

void CCommBase::SendCommMsg(WPARAM wStatus, LPARAM lRecNum)
{
	if(m_CallBackWnd)
		PostMessage(m_CallBackWnd,STATUSMSG,wStatus,lRecNum);
}
