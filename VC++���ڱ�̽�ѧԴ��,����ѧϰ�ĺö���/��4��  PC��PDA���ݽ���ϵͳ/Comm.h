#ifndef __COMM__
#define __COMM__
 
#include "commbase.h"
#include "xml.h"

#define MAXTIME			30000
#define SYNTIME			30000		//for send SYN
#define RSDTIME			3000		//for resend
#define PACKMAX         1000

#define PHONE_TYPE      0x0101
#define NOTE_TYPE       0x0102
#define MEMO_TYPE       0x0103
#define SCHEDULE_TYPE   0x0104

#define NoteFileInfo "#file#"

typedef struct _RecHead_part1
{
	BYTE NoUse1[2];
    BYTE RecLen[2];
	BYTE CheckSum[2];
} RECHEAD1;

typedef struct _RecHead_part2
{
  BYTE CreateTime[4];
  BYTE NoUse1[6];
  BYTE TypeInfo[2];
  BYTE NoUse2[2];
  BYTE nContentLength[2];
} RECHEAD2;  

#define SYMNUMBER 12

static unsigned char PdaSym[]=
{
	'\xab','\xbb','\xa5','\xd7','\xf7','\x80',
	'\x93','\x94','\x9d','\x9e','\x18','\x19'
};

static unsigned short pcChar[]=
{
	0xb6a1,0xb7a1,0xa4a3,0xc1a1,0xc2a1,0xa2a1,
	0xb0a1,0xb1a1,0xfba1,0xfaa1,0xfca1,0xfda1
};

class CComm : public CCommBase
{
public:
	CComm();
	~CComm();
	virtual BOOL Device2Bin();
	virtual BOOL Bin2Device();
	virtual BOOL Xml2Bin();
    virtual BOOL Bin2Xml();
private:
    BYTE PcSymbolToPda(BYTE pcSym[2]);
    WORD PdaSymbolToPc(BYTE PdaSymbol);
    WORD PdaToPC(BYTE *target,BYTE *source,WORD RecLen);
    WORD PCToPda(BYTE *target,BYTE *source,WORD RecLen);

	BOOL WaitSYN(DWORD dwSleep);
	BOOL SendSYN(DWORD dwSleep);
//    void  WritePdaItem(CString ItemValue,FILE* PdaFile);
	

	BOOL BinPhone2Xml(BYTE *pPdaRec);
    BOOL BinMemo2Xml(BYTE *pPdaRec);
	BOOL BinNote2Xml(BYTE *pPdaRec);
	BOOL BinSche2Xml(BYTE *pPdaRec);

    BOOL XmlPhone2Bin(FILE *PdaFile);
    BOOL XmlMemo2Bin(FILE *PdaFile);
	BOOL XmlNote2Bin(FILE *PdaFile);
	BOOL XmlSche2Bin(FILE *PdaFile);

public:
	COut_XML *m_pOutXML;
	CIn_XML *m_pInXML;

private:
	WORD PhoneCataFlag,MemoCataFlag,ScheCataFlag,NoteCataFlag;
//	char InforClass[10];
//	char MemoDir[16];


};

#endif __COMM__