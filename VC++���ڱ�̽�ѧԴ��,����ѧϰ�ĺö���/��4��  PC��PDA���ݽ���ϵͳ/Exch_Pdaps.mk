
Exch_Pdaps.dll: dlldata.obj Exch_Pda_p.obj Exch_Pda_i.obj
	link /dll /out:Exch_Pdaps.dll /def:Exch_Pdaps.def /entry:DllMain dlldata.obj Exch_Pda_p.obj Exch_Pda_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del Exch_Pdaps.dll
	@del Exch_Pdaps.lib
	@del Exch_Pdaps.exp
	@del dlldata.obj
	@del Exch_Pda_p.obj
	@del Exch_Pda_i.obj
