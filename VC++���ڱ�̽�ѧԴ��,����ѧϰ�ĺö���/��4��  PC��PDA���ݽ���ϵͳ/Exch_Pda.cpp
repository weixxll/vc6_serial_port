// Exch_Pda.cpp : Implementation of DLL Exports.


// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f Exch_Pdaps.mk in the project directory.

#include "stdafx.h"
#include "resource.h"
#include <initguid.h>
#include "Exch_Pda.h"

#include "Exch_Pda_i.c"
#include "Exch.h"


CComModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
OBJECT_ENTRY(CLSID_CExch, CExch)
END_OBJECT_MAP()

//注册表项名称和数值对
typedef struct{
   LPSTR lpszValueName;
   LPSTR lpszData;
}REGSTRUCT;

//设备驱动程序（即本COM库）信息，对不同的设备需设置不同的名称和数值对，这些信息
//将被写入注册表。
REGSTRUCT deviceInfo[]={"Version","1.0",
						"TypeInfos","电话,记事,便笺,日程",
						"Ports","COM",
						"Input","1",
						"Output","1"
						};

//设备名称，可能支持多种设备，该信息也将被写入注册表。
CString DeviceName[]={"Moonlight-2.x","Moonlight-3.x"};


class CExch_PdaApp : public CWinApp
{
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExch_PdaApp)
	public:
    virtual BOOL InitInstance();
    virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CExch_PdaApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CExch_PdaApp, CWinApp)
	//{{AFX_MSG_MAP(CExch_PdaApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CExch_PdaApp theApp;

BOOL CExch_PdaApp::InitInstance()
{
    _Module.Init(ObjectMap, m_hInstance, &LIBID_EXCH_PDALib);
    return CWinApp::InitInstance();
}

int CExch_PdaApp::ExitInstance()
{
    _Module.Term();
    return CWinApp::ExitInstance();
}

/////////////////////////////////////////////////////////////////////////////
// Used to determine whether the DLL can be unloaded by OLE

STDAPI DllCanUnloadNow(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    return (AfxDllCanUnloadNow()==S_OK && _Module.GetLockCount()==0) ? S_OK : S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Returns a class factory to create an object of the requested type

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _Module.GetClassObject(rclsid, riid, ppv);
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
    // registers object, typelib and all interfaces in typelib
    BOOL   bRet; 
    DWORD  dwReg;
    HKEY   hExchKeyNames;
    int  i;

    //上一节实现的IExch接口的IID
    char   KeyValue[]= "{ BBA90071-A96C-4076-8691-B1FA77C0342F}";	

    bRet = _Module.RegisterServer(TRUE);

    //创建HKEY_LOCAL_MACHINE\\SOFTWARE\\PdaSoft\\PdaComm\\Exchanges项，

    dwReg = RegCreateKeyEx(
				HKEY_LOCAL_MACHINE,				// handle to open key
				"SOFTWARE\\PdaSoft\\PdaComm\\Exchanges",	// subkey name
				0,									// reserved
				"",									// class string
				REG_OPTION_NON_VOLATILE,              // special options
				KEY_WRITE,							// desired security access
				NULL,								// inheritance
				&hExchKeyNames,						// key handle 
				NULL								// disposition value buffer
			);
    if(dwReg != ERROR_SUCCESS ) return false;  

	//设置COM库所支持的PDA设备名值对，值均为该COM库IExch接口GUID。
	for(i=0;i<2;i++)
	{
		dwReg = RegSetValueEx(
					hExchKeyNames,					// handle to key
					DeviceName[i],						// value name
					0,								// reserved
					REG_SZ,							// value type
					(const unsigned char *)&KeyValue[0],		// value data
					sizeof(KeyValue)						// size of value data
					);
		if(dwReg != ERROR_SUCCESS ) return false;  
	}
	RegCloseKey(hExchKeyNames);

	//在HKEY_CLASSES_ROOT\\ CLSID\\{ BBA90071-A96C-4076-8691-B1FA77C0342F}
	//键下建立Attributes子键
	dwReg = RegCreateKeyEx(
				HKEY_CLASSES_ROOT,					// handle to open key
				"CLSID\\{ BBA90071-A96C-4076-8691-B1FA77C0342F}\\Attributes",
				0,									// reserved
				"",									// class string
				REG_OPTION_NON_VOLATILE,              // special options
				KEY_WRITE,							// desired security access
				NULL,								// inheritance
				&hExchKeyNames,						// key handle 
				NULL								// disposition value buffer
			);
    if(dwReg != ERROR_SUCCESS ) return false; 

    //在Attributes子键下设置设备驱动程序（该COM库）信息（名值对）。
    for(i=0;i<5;i++)
	{
		dwReg = RegSetValueEx(
					hExchKeyNames,					// handle to key
					deviceInfo[i].lpszValueName,	          // value name
					0,								// reserved
					REG_SZ,							// value type
					(const unsigned char *)deviceInfo[i].lpszData,	// value data
					1024								// size of value data
					);
		if(dwReg != ERROR_SUCCESS ) return false;
	}

    RegCloseKey(hExchKeyNames);

	return bRet;

}

/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
	BOOL   bRet; 
	DWORD  dwReg;
	HKEY   hExchKeyNames;
	int    i;
     char   KeyValue[]= "{ BBA90071-A96C-4076-8691-B1FA77C0342F}";

    //获取HKEY_LOCAL_MACHINE\\SOFTWARE\\PdaSoft\\PdaComm\\Exchanges项句柄
    dwReg =  RegOpenKeyEx(
					HKEY_LOCAL_MACHINE,         			// handle to open key
					" SOFTWARE\\PdaSoft\\PdaComm\\Exchanges ",   // subkey name
					0,   									// reserved
					KEY_WRITE, 							// security access mask
					&hExchKeyNames    // handle to open key
			);
    if(dwReg != ERROR_SUCCESS ) return false;  

    	//删除该COM库所支持的PDA设备名值对。
	for(i=0;i<2;i++)
	{
		dwReg = RegDeleteValue(
					hExchKeyNames,									
					DeviceName[i]							
				);
		if(dwReg != ERROR_SUCCESS ) return false;  
	}
	RegCloseKey(hExchKeyNames);

	dwReg = RegOpenKeyEx( HKEY_CLASSES_ROOT,			// handle to open key
				"CLSID\\{BBA90071-A96C-4076-8691-B1FA77C0342F}\\Attributes",							0,									// reserved
				KEY_WRITE,							// desired security access
				&hExchKeyNames						// key handle 
			);
    if(dwReg != ERROR_SUCCESS ) return false; 

	//删除在Attributes子键下的设备驱动程序（该COM库）信息（名值对）。
     for(i=0;i<5;i++)
	{
		dwReg = RegDeleteValue(hExchKeyNames,				// handle to key
						deviceInfo[i].lpszValueName			// value name
			);
		if(dwReg != ERROR_SUCCESS ) return false;

	}
     RegCloseKey(hExchKeyNames);

	//删除HKEY_CLASSES_ROOT\\ CLSID\\{ 5DB8ED2C-5ECD-4488-B84D-54BF096543B0}
	//下的Attributes子键	
	dwReg = RegOpenKeyEx(
				HKEY_CLASSES_ROOT,					// handle to open key
				"CLSID\\{ BBA90071-A96C-4076-8691-B1FA77C0342F}",// subkey name
				0,									// reserved
				KEY_WRITE,							// desired security access
				&hExchKeyNames						// key handle 
			);
    if(dwReg != ERROR_SUCCESS ) return false; 
    RegDeleteKey(hExchKeyNames,"Attributes");
    RegCloseKey(hExchKeyNames);

    bRet = _Module.UnregisterServer(TRUE);
	return bRet;

}


