/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Thu Oct 31 02:01:25 2002
 */
/* Compiler settings for E:\develop\MobileX\Exch_Pda\Exch_Pda.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IExch = {0x5DB8ED2C,0x5ECD,0x4488,{0xB8,0x4D,0x54,0xBF,0x09,0x65,0x43,0xB0}};


const IID LIBID_EXCH_PDALib = {0xFC6BB134,0x8E6A,0x4398,{0xA8,0x04,0xBD,0x72,0xFB,0xCF,0xB2,0xCD}};


const CLSID CLSID_CExch = {0xBBA90071,0xA96C,0x4076,{0x86,0x91,0xB1,0xFA,0x77,0xC0,0x34,0x2F}};


#ifdef __cplusplus
}
#endif

