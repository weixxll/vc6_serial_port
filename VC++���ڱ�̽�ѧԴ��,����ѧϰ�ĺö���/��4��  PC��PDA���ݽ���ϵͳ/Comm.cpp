#include <afxwin.h> 
#include "comm.h"
#include <io.h>


int IsAllSame(const unsigned char* abIn,int nLength) 
{
	for(int i=1;i<nLength;i++)
	{
		if(abIn[i]!=abIn[0]) return 0;
	}
	return abIn[0];
}

/*把src中的高字节和低字节调换后写入dst所指地址*/
void ExchangeHILO(unsigned short src,unsigned short *dst)
{
	unsigned short tmp;
	tmp=src>>8;
	*dst=src<<8;
	*dst|=tmp;
}



CComm::CComm()
{
	m_dcb.BaudRate = 56000;
	m_pOutXML=NULL;
	m_pInXML=NULL;
}

CComm::~CComm()
{

}

	
BOOL CComm::Device2Bin()
{
	
   DWORD    start;
   int      nLength,ci;

   BYTE     abIn[ MAXBLOCK],*buf,*bufp,*bufp1,*old_bufp;

   BYTE     SYN[1]={0x1};
   BYTE		ACK[1]={0x2};
   BYTE     RESEND[1]={0x4};
   BYTE		ASK[6]={0,0,0,1,0,1};
   BYTE		BYE[3]={6,0,6};
   BYTE		BUSY[1]={0x7};
   int		step;
   FILE		*RSF;
   long		filelen,readlen,totallen,templen;
   WORD		check;
   int		packf;
   int      ReSendSum;
   BOOL     bRet=TRUE; 
   WORD     nRecord=0;

   	RSF=fopen(m_PdaFileName,"wb");
	if(!RSF)
	{
		SendCommMsg(CSM_EXCEPTION,0);
		return FALSE;
	}
	buf=(BYTE*)malloc(MAXBLOCK*10);
	if(!buf)
	{
		SendCommMsg(CSM_EXCEPTION,0);
		fclose(RSF);
		return FALSE;
	}

	nLength=0;
	start=GetTickCount();

    step=0;totallen=0;packf=0;
	
	bufp=buf;

	if (!WaitSYN(100))
	{
		bRet=FALSE;
		return bRet;
	}
	do
	{
		if(!m_bConnected) 
		{
			bRet=FALSE;
			break;
		}
		nLength =ReadCommBlock( (LPSTR) abIn, MAXBLOCK );
		if((GetTickCount()-start)>MAXTIME)
		{
				SendCommMsg(CSM_TIMEOUT,0);  
				bRet=FALSE;
				break;
		}
	    switch(step)
		{
		case 0:
		{
			   if((nLength>=1)&&(IsAllSame(abIn,nLength)==SYN[0])) 
			   {
				   step=1;
				   ReSendSum=3; 
				   start=GetTickCount();
				   WriteCommBlock( (LPSTR)ACK, 1);
			   }
			   else if(nLength==0)
				   break;
			   else
			   {
				   SendCommMsg(CSM_SYNERROR,0);  
				   bRet=FALSE;
			   }
			   break;
		}
		case 1:
		{
			   if((nLength>=1)&&(IsAllSame(abIn,nLength)==SYN[0])) 
			   {
				   WriteCommBlock( (LPSTR)ACK, 1);
				   break;
			   }
			   if(nLength==0)
				   break;
			   
			   if((nLength==6)&&(abIn[0]==0)&&(abIn[1]==0)) 
			   {	
				    start=GetTickCount();	
					check=0;
					
					if (ReSendSum==0) 
					{
						SendCommMsg(CSM_DATAERROR,0);  
						bRet=FALSE;
						break;
                    }

					check+=abIn[2]+(abIn[2]&0x80?0xff00:0);
					check+=abIn[3]+(abIn[3]&0x80?0xff00:0);
					
					if((BYTE(check>>8)!=abIn[4])||(BYTE(check&0xff)!=abIn[5]))
					{
						step=1;
						ReSendSum--;
						WriteCommBlock( (LPSTR)RESEND, 1);
					}
					else
					{
						ReSendSum=3; 
						memcpy(bufp,abIn,6);
						bufp+=6;
						if((abIn[3]==1)&&(abIn[4]==0)&&(abIn[5]==1)) 
						{
							step=4;
							fwrite("\0\0\0\x1\0\x1",1,6,RSF);
						}
						else
						{	
							step=3;
							filelen=(((long)abIn[2]<<8)+(long)abIn[3])+(long)2;
						}
						WriteCommBlock( (LPSTR)ACK, 1);
					}
			   }
			   else
			   {
				   SendCommMsg(CSM_DATAERROR,0);  
				   bRet=FALSE;
			   }

			   break;
		}
		case 2:
		{
			   if((nLength>=1)&&(IsAllSame(abIn,nLength)==SYN[0]))
			   {	
				   start=GetTickCount();
				   ReSendSum=3;
				   step=1;
				   if(packf==0) nRecord++;
				   if(nRecord)				   
					   SendCommMsg(CSM_RUNNING,nRecord);
				   WriteCommBlock( (LPSTR)ACK, 1);
			   }
			   if(nLength==0) break;
			   if((nLength>0)&&(abIn[0]==BUSY[0]))
			   {	
				   start=GetTickCount();
				   WriteCommBlock( (LPSTR)ACK, 1);
			   }
			   break;
		}
		case 3:
		{
			   step=2;
			   check=0;
			   readlen=0;
			   old_bufp=bufp;
			   start=GetTickCount();
			   do
			   {	
					nLength = ReadCommBlock( (LPSTR) abIn, MAXBLOCK );
					if(nLength>0)
					{	
						start=GetTickCount();
						memcpy(bufp,abIn,nLength);
						bufp+=(long)nLength;
						for(ci=0;ci<nLength;ci++) 
							check+=abIn[ci]+(abIn[ci]&0x80?0xff00:0);
						readlen+=(long)nLength;
					}
					if((GetTickCount()-start)>RSDTIME)
						break;
			   }while(readlen<filelen);

				check-=(*(bufp-1)+(*(bufp-1)&0x80?0xff00:0));
				check-=(*(bufp-2)+(*(bufp-2)&0x80?0xff00:0));

				if((readlen<filelen)||
				      ((check>>8)!=*(bufp-2))||((check&0xff)!=*(bufp-1)))
				{	
					bufp=old_bufp;
					step=3;
					if (ReSendSum>0)
					{
						ReSendSum--;
						WriteCommBlock( (LPSTR)RESEND, 1);
					} 
					else
					{
						SendCommMsg(CSM_DATAERROR,0);  
						bRet=FALSE;
					}
					break;
				}
				else
				{
					nRecord++;
					SendCommMsg(CSM_RUNNING,nRecord);
				}
					

					totallen+=filelen-16-2;
					templen=((long)buf[6+14]<<8)+buf[6+15];

					switch(packf)
					{
					case 0: if(totallen<templen)
							{	
								packf=1;
								buf[2]=(templen+16)>>8;
								buf[3]=(templen+16)&0xff;
								bufp1=bufp;
							}
							else
							{	
								fwrite(buf,1,(long)(bufp-buf),RSF);
								bufp=buf;
								totallen=0;
							}
							break;
					case 1: if(totallen<templen)
							{
								memcpy(bufp1-2,bufp1+6+16,(long)(bufp-bufp1-6-16));
								bufp1=bufp-6-16-2;bufp=bufp1;
							}
							else
							{	
								packf=0;
								totallen=0;
								memcpy(bufp1-2,bufp1+6+16,(long)(bufp-bufp1-6-16));
								fwrite(buf,1,(long)(bufp-6-16-2-buf),RSF);
								bufp=buf;
							}
							break;
					default: break;
					}
			
				
				WriteCommBlock( (LPSTR)ACK, 1);
			    break;
		}
		case 4:
		{
			if((nLength==3)&&(abIn[0]==6)&&(abIn[1]==0)&&(abIn[2]==6))
			   {
				    start=GetTickCount();
					step=5;
				   	WriteCommBlock( (LPSTR)ACK, 1);
			   };
			   break;
		}
		default:
			   break;
		};
	}while((step<5)&&(bRet));

	if(step==5)	
		;
	else 	
		fwrite("\0\0\0\x1\0\x1",1,6,RSF);

	free(buf);
	fclose(RSF);
   	return 	bRet;
}

  
BOOL CComm::Bin2Device()
{
   DWORD       start;
   int         nLength;

   BYTE        abIn[ MAXBLOCK + 1],*buf,*bufp,*old_bufp,*tempbufp;

   BYTE		SYN[1]={0x1};
   BYTE		ACK[1]={0x2};
   BYTE		RESEND[1]={0x4};
   BYTE		ASK[6]={0,0,0,1,0,1};
   BYTE		BYE[3]={6,0,6};
   BYTE		BUSY[1]={0x7};
   int		step,old_step;
   FILE		*RSF;
   long		filelen,templen,totallen,old_len;
   WORD		check,ci;
   int		packf,packf1;
   BOOL     bRet=TRUE;
   WORD     RecordNum=0;

    RSF=fopen(m_PdaFileName,"rb");
	if(!RSF)
	{
		SendCommMsg(CSM_EXCEPTION,0);
		return FALSE;
	}
	filelen=_filelength(_fileno(RSF));
	buf=(BYTE*)malloc(filelen);
	if(!buf)
	{
		SendCommMsg(CSM_EXCEPTION,0);
		fclose(RSF);
		return FALSE;
	}
	fread(buf,1,filelen,RSF);
	fclose(RSF);

	step=0;
	bufp=buf;
	old_step=step;
	old_bufp=bufp;
	totallen=0;
	packf=0;
	packf1=0;

	nLength=0;
	start=GetTickCount();
	
	if (!SendSYN(100))
	{
		SendCommMsg(CSM_SYNERROR,0);
		goto finish;
	}
	do{
		if(!m_bConnected) 
		{
			SendCommMsg(CSM_KILLED,0);
			bRet=FALSE;
			break;
		}
		nLength = ReadCommBlock( (LPSTR) abIn, MAXBLOCK);
	
	    if((nLength>=1) && (IsAllSame(abIn,nLength)==RESEND[0]))					
		{
			bufp=old_bufp;
			step=old_step;
		}

		if((nLength>=1) && ((IsAllSame(abIn,nLength)==ACK[0])
						   ||(IsAllSame(abIn,nLength)==RESEND[0])))
		{
		start=GetTickCount();
		switch(step)
		{
		case 0:
			
			if(abIn[0]!=RESEND[0])
			{
				old_step=step;
				old_bufp=bufp;
				templen=(((long)bufp[2]<<8)+bufp[3])+2;

					if(packf==0)
					{	
						packf1=0;
						totallen=templen-2;
						if(totallen>0x1000)
						{	
							packf=1;
							totallen-=0x1000;
							bufp[2]=0x10;
							bufp[3]=0;
							templen=0x1000+2;
						}
						check=bufp[2]+(bufp[2]&0x80?0xff00:0)+bufp[3]+(bufp[3]&0x80?0xff00:0);
						bufp[4]=check>>8;
						bufp[5]=check&0xff;
						tempbufp=bufp;
						bufp+=6;
					}
					else if(packf==1)
					{	
						packf1=1;
						if(totallen>0x1000)
						{	
							totallen-=(0x1000-16);
							templen=0x1000+2;
						}
						else
						{	
							packf=0;
							tempbufp[2]=(totallen+16)>>8;tempbufp[3]=(totallen+16)&0xff;
							check=tempbufp[2]+(tempbufp[2]&0x80?0xff00:0);
							check+=tempbufp[3]+(tempbufp[3]&0x80?0xff00:0);
							tempbufp[4]=check>>8;tempbufp[5]=check&0xff;
							templen=totallen+2+16;
						}
					}
			}

 			else if(packf1==0) bufp+=6;
			WriteCommBlock((LPSTR)tempbufp,6);

			step=1;
			if(templen==3) step=4;   
				   
		   break;
		case 1:
			if(abIn[0]==RESEND[0]) 
				templen=old_len;
			else 
				old_len=templen;

		    old_step=step;
			old_bufp=bufp;
			check=0;
			if(packf1==1)
			{	
				WriteCommBlock((LPSTR)tempbufp+6,16);
				for(ci=6;ci<6+16;ci++) 
					check+=tempbufp[ci]+(tempbufp[ci]&0x80?0xff00:0);
				templen-=16;
			}
			while((templen-2)>=MAXBLOCK)
			{
				WriteCommBlock((LPSTR)bufp,MAXBLOCK);
				for(ci=0;ci<MAXBLOCK;ci++) 
					check+=bufp[ci]+(bufp[ci]&0x80?0xff00:0);
				templen-=MAXBLOCK;bufp+=MAXBLOCK;
			};
			if((templen-2)>0)
			{
				WriteCommBlock((LPSTR)bufp,templen-2);
				for(ci=0;ci<templen-2;ci++) 
					check+=bufp[ci]+(bufp[ci]&0x80?0xff00:0);
			}
			bufp+=templen-2;

			abIn[0]=check>>8;
			abIn[1]=check&0xff;
	
			WriteCommBlock((LPSTR)abIn,2);
			if(packf==0)
				bufp+=2;

			step=2;
			break;
		case 2: 
			old_step=step;
		    old_bufp=bufp;
			if(packf==0)
			{
				RecordNum++;
				SendCommMsg(CSM_RUNNING,RecordNum);
			}
			if(SendSYN(100))
				step=0;
			break;

		case 4: 
			old_step=step;
		    old_bufp=bufp;
			WriteCommBlock((LPSTR)BYE,3);
			step=5;
			break;
		case 5:			
			break;
		default:break;
		};
		}
		if((GetTickCount()-start)>MAXTIME)
		{
			SendCommMsg(CSM_TIMEOUT,0);  
			bRet=FALSE;
			goto finish;
		}
	}while((step<5)&&(bRet));

finish:
	free(buf);
    if(step==5)
	{
		bRet=TRUE;
	}
	else
		bRet=FALSE;
	return bRet;
}



BOOL CComm::Xml2Bin()
{

    FILE    *PdaFile;
    BOOL    bRet,bOpened;

	PdaFile=fopen(m_PdaFileName,"wb");
    if(PdaFile==NULL)
	{
		bRet=FALSE;
		goto finish;
	}

	m_pInXML=new CIn_XML;
	m_pInXML->Open(m_XmlFileName, bOpened);
	if (!bOpened)
	{
		fclose(PdaFile);
		delete m_pInXML;
		SendCommMsg(CSM_INITFAILED,0);
		return FALSE;
	}

    
	XmlPhone2Bin(PdaFile);
	XmlMemo2Bin(PdaFile);
	XmlNote2Bin(PdaFile);
	XmlSche2Bin(PdaFile);
	

	fwrite("\0\0\0\x1\0\x1",1,6,PdaFile);
	fclose(PdaFile);

finish:
	m_pInXML->Close();

	delete m_pInXML;
	m_pInXML = NULL;

	return TRUE;
}


BOOL CComm::XmlPhone2Bin(FILE* PdaFile)
{
	return TRUE;
}


BOOL CComm::XmlSche2Bin(FILE* PdaFile)
{
	return TRUE;
}

BOOL CComm::XmlNote2Bin(FILE* PdaFile)
{
	return TRUE;
}


BOOL CComm::XmlMemo2Bin(FILE* PdaFile)
{
	return TRUE;
}



BOOL CComm::Bin2Xml()
{
    FILE               *PdaFile;
	RECHEAD1		   Rechead1;
	RECHEAD2           Rechead2;
	unsigned short     nRecLen;
	bool               bRet=TRUE;

	m_pOutXML=new COut_XML;

	int nStatus;
	m_pOutXML->Open(m_XmlFileName, nStatus);


	if (!nStatus) 
	{
		SendCommMsg(CSM_INITFAILED,0);
		delete m_pOutXML;
		return FALSE;
	}

	PdaFile=fopen(m_PdaFileName,"rb");
	if(PdaFile==NULL)
	{
		SendCommMsg(CSM_INITFAILED,0);
		delete m_pOutXML;
		return FALSE;
	}

	PhoneCataFlag=0;
	MemoCataFlag=0;
	ScheCataFlag=0;
	NoteCataFlag=0;

	do
	{
		fread(&Rechead1,sizeof(RECHEAD1),1,PdaFile);
		if(memcmp(Rechead1.NoUse1,"\0\0\0\01\0\01",6)==0)
		{
			break;
		}
		ExchangeHILO(*(unsigned short*)(Rechead1.RecLen),
			(unsigned short*)(Rechead1.RecLen));
		*(unsigned short*)(Rechead1.RecLen)+=2;
		BYTE * const pPdaRec=new BYTE[*(WORD*)(Rechead1.RecLen)];
		nRecLen=fread(pPdaRec,1,*(WORD*)(Rechead1.RecLen),PdaFile);
		if(*(WORD*)(Rechead1.RecLen)!=nRecLen)
		{
			bRet=FALSE;
			break;
		}
		Rechead2=*(RECHEAD2 *)pPdaRec;
		BYTE* pTemp=pPdaRec+sizeof(RECHEAD2);
		ExchangeHILO(*(unsigned short*)(Rechead2.TypeInfo),
			(unsigned short*)(Rechead2.TypeInfo));
		switch(*(unsigned short*)(Rechead2.TypeInfo))
		{
		//电话
		case PHONE_TYPE:
			BinPhone2Xml(pTemp);
			break;
			
		//记事
		case MEMO_TYPE:
			BinMemo2Xml(pTemp);
			break;
			
		//便笺
		case NOTE_TYPE:
			BinNote2Xml(pTemp);
			break;

		//日程
		case SCHEDULE_TYPE:
			BinSche2Xml(pTemp);
			break;

		//该款Pda不支持其他格式
		default:
			bRet=FALSE;
			break;

		}
		delete []pPdaRec;

	}while(bRet);
	
	fclose(PdaFile);

	m_pOutXML->Close();

	delete m_pOutXML;
	m_pOutXML = NULL;

	return bRet;
}	


BOOL CComm::BinMemo2Xml(BYTE *pPdaRec)
{	
	return TRUE;
}




BOOL CComm::BinPhone2Xml(BYTE* pPdaRec)
{
	return TRUE;
}

BOOL CComm::BinSche2Xml(BYTE *pPdaRec)
{
	return TRUE;
}


BOOL CComm::BinNote2Xml(BYTE *pPdaRec)
{
	return TRUE;
}


BYTE CComm::PcSymbolToPda(BYTE pcSym[2])
{
	unsigned char *changeSet;
	short i;
	
	changeSet=PdaSym;
	
	if(pcSym[0]==0xa1)
	{
		for(i=0;i<SYMNUMBER;i++)
			if(*((unsigned short *)pcSym)==pcChar[i])
				return changeSet[i];
		if((pcSym[1]==0xa1)||(pcSym[1]==0xab))
			return 0x20;
		return 0xff;
	}
    if(pcSym[0]==0xa3)
	{
		if(pcSym[1]==0xa4)
		    return 0xa5;
        else if((pcSym[1]>=0xb0)&&(pcSym[1]<=0xb9))
			return pcSym[1]-0xb0+0x30;
		else if((pcSym[1]>=0xe1)&&(pcSym[1]<=0xfa))
            return pcSym[1]-0xe1+0x61;
		else if(pcSym[1]==0xa8)
			return 0x28;
		else if(pcSym[1]==0xa9)
			return 0x29;
		else if(pcSym[1]==0xba)
			return 0x3a;
		else if(pcSym[1]==0xbf)
			return 0x3f;
		else if(pcSym[1]==0xac)
			return 0x2c;
        else if(pcSym[1]==0xa1)
			return 0x21;
		else if((pcSym[1]==0xfb)||(pcSym[1]==0xfd)||(pcSym[1]==0xbb)||
				(pcSym[1]==0xfc)||(pcSym[1]==0xe0))
			return 0x20;
		else
			return pcSym[1]-161+'!';
	}
    if((pcSym[0]==0xa6)&&(pcSym[1]==0xe4))
		return 0x20;

	return 0xff;
}

unsigned short CComm::PdaSymbolToPc(BYTE PdaSymbol)
{
	BYTE  *changeSet;
	short i;

	changeSet=PdaSym;

	for(i=0;i<SYMNUMBER;i++)
		if(changeSet[i]==PdaSymbol)
			return pcChar[i];
	return 0xffff;
}

WORD CComm::PdaToPC( BYTE *target, BYTE *source,WORD RecLen)
{
	WORD  cLen,k,tmp;
	
	cLen=0;
    for(k=0;k<RecLen;k+=2)
	{
		if(source[k]==0)
		{
			if(source[k+1]==0x0a)
			{
				target[cLen]='\x0d';
                target[cLen+1]='\x0a';
				cLen+=2;
			}
            else if((tmp=PdaSymbolToPc(source[k+1]))!=0xffff)
			{
				*((unsigned short *)(target+cLen))=tmp;
				cLen+=2;
			}
			else
			{
				target[cLen]=source[k+1];
				cLen++;
			}
		}
		else
		{
			target[cLen]=source[k];
			target[cLen+1]=source[k+1];
			cLen+=2;
		}
	}
	return cLen;
}

WORD CComm::PCToPda( BYTE *target, BYTE *source,WORD RecLen)
{
	WORD  cLen,k;
	BYTE  c;

	cLen=0;
    for(k=0;k<RecLen;)
	{
		if(source[k]&0x80)
		{//汉字
			c=PcSymbolToPda(source+k);
			if(c!=0xff)
			{
				target[cLen]=0;
				target[cLen+1]=c;
			}
			else
			{
				target[cLen]=source[k];
				target[cLen+1]=source[k+1];
			}
			k+=2;
		}
		else if((source[k]==0x0d) && (source[k+1]==0x0a))
		{
				target[cLen]=0;
				target[cLen+1]=0x0a;
				k=k+2;
		}
		else
		{
				target[cLen]=0;
				target[cLen+1]=source[k];
				k++;
		 }
		cLen+=2;
	}
	return cLen;
}




BOOL CComm::WaitSYN(DWORD dwSleep)
{
	DWORD		nLength;
	DWORD       start;
	DWORD       dwErrorFlags;
	COMSTAT     ComStat;
	BOOL		bTimeout;

	nLength=0;
	start=GetTickCount();

	bTimeout=FALSE;

	while((nLength==0)&&(!bTimeout)&&(m_bConnected))
	{
		if(dwSleep) Sleep(dwSleep);
		ClearCommError(m_idComDev, &dwErrorFlags, &ComStat);
		nLength=ComStat.cbInQue;
		bTimeout = (GetTickCount()-start>=SYNTIME);
	}

	if (bTimeout) {
		SendCommMsg(CSM_TIMEOUT,0);
		return FALSE;
	}

	if (!m_bConnected) {                   
		return FALSE;
	}
	return TRUE;
}

BOOL CComm::SendSYN(DWORD dwSleep)
{
	BYTE        SYN[1]={0x1}; 
	DWORD		nLength;
	DWORD       start;
	DWORD       dwErrorFlags;
	COMSTAT     ComStat;
	BOOL		bTimeout;

	nLength=0;
	start=GetTickCount();
	bTimeout=FALSE;
	while((nLength==0) && (!bTimeout)&&(m_bConnected))
	{
		WriteCommBlock((LPSTR)SYN,1);
		ClearCommError( m_idComDev, &dwErrorFlags, &ComStat ) ;
		nLength=ComStat.cbInQue;
		bTimeout = (GetTickCount()-start>=SYNTIME);
		if((dwSleep)&&(!nLength))
			Sleep(dwSleep);
	}
	if (bTimeout) {
		SendCommMsg(CSM_TIMEOUT,0);
		return FALSE;
	}

	if (!m_bConnected)
	{
		return FALSE;
	}
	return TRUE;
}

