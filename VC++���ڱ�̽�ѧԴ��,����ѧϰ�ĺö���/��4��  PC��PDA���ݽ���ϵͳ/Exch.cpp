// Exch.cpp : Implementation of CExch
#include "stdafx.h"
#include "Exch_Pda.h"
#include "Exch.h"

/////////////////////////////////////////////////////////////////////////////
// CExch


STDMETHODIMP CExch::SetupDevice(int hMsgWnd, BSTR DeviceFile, BOOL *bResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	// TODO: Add your implementation code here
	*bResult = m_com.SetupDevice((HWND)hMsgWnd, DeviceFile);

	return S_OK;
}

STDMETHODIMP CExch::Write(BSTR Path, BSTR XmlFileName, BOOL *bResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	// TODO: Add your implementation code here
	*bResult = m_com.Write(Path, XmlFileName);

	return S_OK;
}

STDMETHODIMP CExch::Read(BSTR Path, BSTR XmlFileName, BOOL *bResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	// TODO: Add your implementation code here
	*bResult = m_com.Read(Path, XmlFileName);

	return S_OK;
}

STDMETHODIMP CExch::Kill()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	// TODO: Add your implementation code here
	m_com.Kill();

	return S_OK;
}
