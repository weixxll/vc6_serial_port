// Exch.h : Declaration of the CExch

#ifndef __EXCH_H_
#define __EXCH_H_

#include "resource.h"       // main symbols
#include "comm.h"

/////////////////////////////////////////////////////////////////////////////
// CExch
class ATL_NO_VTABLE CExch : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CExch, &CLSID_CExch>,
	public IDispatchImpl<IExch, &IID_IExch, &LIBID_EXCH_PDALib>
{
public:
	CExch()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_EXCH)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CExch)
	COM_INTERFACE_ENTRY(IExch)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IExch
public:
	STDMETHOD(Kill)();
	STDMETHOD(Read)(BSTR Path, BSTR XmlFileName, BOOL *bResult);
	STDMETHOD(Write)(BSTR Path, BSTR XmlFileName, BOOL *bResult);
	STDMETHOD(SetupDevice)(int hMsgWnd, BSTR DeviceFile, BOOL *bResult);
private:
	CComm m_com;
};

#endif //__CEXCH_H_
