; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSerialComDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SerialCom.h"

ClassCount=3
Class1=CSerialComApp
Class2=CSerialComDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_SERIALCOM_DIALOG

[CLS:CSerialComApp]
Type=0
HeaderFile=SerialCom.h
ImplementationFile=SerialCom.cpp
Filter=N

[CLS:CSerialComDlg]
Type=0
HeaderFile=SerialComDlg.h
ImplementationFile=SerialComDlg.cpp
Filter=D
LastObject=CSerialComDlg
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=SerialComDlg.h
ImplementationFile=SerialComDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_SERIALCOM_DIALOG]
Type=1
Class=CSerialComDlg
ControlCount=21
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342308352
Control3=IDC_SEL_COM,combobox,1344339970
Control4=IDC_STATIC,static,1342308352
Control5=IDC_SEL_BAUT,combobox,1344339970
Control6=IDC_STATIC,static,1342308352
Control7=IDC_SEL_DATA,combobox,1344339970
Control8=IDC_STATIC,static,1342308352
Control9=IDC_SEL_STOP,combobox,1344339970
Control10=IDC_STATIC,static,1342308352
Control11=IDC_SEL_PARITY,combobox,1344339970
Control12=IDC_EDIT_SEND,edit,1352728644
Control13=IDC_OPENCOM,button,1342242816
Control14=IDC_SEND,button,1342242816
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_EDIT_REC,edit,1486946372
Control18=IDC_STATIC,button,1342177287
Control19=IDC_DISP,static,1342308352
Control20=IDC_CLR_SEND,button,1342242816
Control21=IDC_CLR_REC,button,1342242816

