//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SerialCom.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SERIALCOM_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDC_SEL_COM                     1000
#define IDC_SEL_BAUT                    1001
#define IDC_SEL_DATA                    1002
#define IDC_SEL_STOP                    1003
#define IDC_SEL_PARITY                  1004
#define IDC_EDIT_SEND                   1005
#define IDC_OPENCOM                     1006
#define IDC_SEND                        1007
#define IDC_EDIT_REC                    1008
#define IDC_DISP                        1009
#define IDC_CLR_SEND                    1010
#define IDC_CLR_REC                     1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
