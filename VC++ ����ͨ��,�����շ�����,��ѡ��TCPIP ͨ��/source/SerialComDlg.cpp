// SerialComDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SerialCom.h"
#include "SerialComDlg.h"
#include "PubSub.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CPubSub *m_PubSub;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerialComDlg dialog

CSerialComDlg::CSerialComDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSerialComDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSerialComDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	
	m_ComNo=1;
	m_ComBaut=9600;
	m_ComData=8;
	m_ComStop=0;
	m_ComParity=0;
	m_PubSub=NULL;

	m_PubSub=new CPubSub;
}

void CSerialComDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSerialComDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSerialComDlg, CDialog)
	//{{AFX_MSG_MAP(CSerialComDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_OPENCOM, OnOpencom)
	ON_BN_CLICKED(IDC_SEND, OnSend)
	ON_WM_CLOSE()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CLR_SEND, OnClrSend)
	ON_BN_CLICKED(IDC_CLR_REC, OnClrRec)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerialComDlg message handlers

BOOL CSerialComDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	ComDlgInit();
	SetTimer(100,500,NULL);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSerialComDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSerialComDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSerialComDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSerialComDlg::ComDlgInit()
{
	//串口号
	int i;
	char pp[20];
	for(i=1;i<=256;i++)
	{
		sprintf(pp,"串口%d",i);
		SendDlgItemMessage(IDC_SEL_COM,CB_ADDSTRING,0,LPARAM(pp));
	}
	SendDlgItemMessage(IDC_SEL_COM,CB_SETCURSEL,m_ComNo-1,0);

	//波特率
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("110"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("300"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("600"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("1200"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("2400"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("4800"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("9600"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("14400"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("19200"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("38400"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("56000"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("57600"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("115200"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("128000"));
	SendDlgItemMessage(IDC_SEL_BAUT,CB_ADDSTRING,0,LPARAM("256000"));
	SetDlgItemInt(IDC_SEL_BAUT,m_ComBaut);

	//数据位
	SendDlgItemMessage(IDC_SEL_DATA,CB_ADDSTRING,0,LPARAM("6"));
	SendDlgItemMessage(IDC_SEL_DATA,CB_ADDSTRING,0,LPARAM("7"));
	SendDlgItemMessage(IDC_SEL_DATA,CB_ADDSTRING,0,LPARAM("8"));
	SendDlgItemMessage(IDC_SEL_DATA,CB_SETCURSEL,m_ComData-6,0);
	
	//停止位
	SendDlgItemMessage(IDC_SEL_STOP,CB_ADDSTRING,0,LPARAM("1位"));
	SendDlgItemMessage(IDC_SEL_STOP,CB_ADDSTRING,0,LPARAM("1.5位"));
	SendDlgItemMessage(IDC_SEL_STOP,CB_ADDSTRING,0,LPARAM("2位"));
	SendDlgItemMessage(IDC_SEL_STOP,CB_SETCURSEL,m_ComStop,0);
	
	//校验
	SendDlgItemMessage(IDC_SEL_PARITY,CB_ADDSTRING,0,LPARAM("无校验"));
	SendDlgItemMessage(IDC_SEL_PARITY,CB_ADDSTRING,0,LPARAM("奇校验"));
	SendDlgItemMessage(IDC_SEL_PARITY,CB_ADDSTRING,0,LPARAM("偶校验"));
	SendDlgItemMessage(IDC_SEL_PARITY,CB_SETCURSEL,m_ComParity,0);

	GetDlgItem(IDC_SEND)->EnableWindow(FALSE);
}

void CSerialComDlg::EnableCom()
{
	GetDlgItem(IDC_SEL_COM)->EnableWindow(TRUE);
	GetDlgItem(IDC_SEL_BAUT)->EnableWindow(TRUE);
	GetDlgItem(IDC_SEL_DATA)->EnableWindow(TRUE);
	GetDlgItem(IDC_SEL_STOP)->EnableWindow(TRUE);
	GetDlgItem(IDC_SEL_PARITY)->EnableWindow(TRUE);
	GetDlgItem(IDC_SEND)->EnableWindow(FALSE);
}

void CSerialComDlg::DisableCom()
{
	GetDlgItem(IDC_SEL_COM)->EnableWindow(FALSE);
	GetDlgItem(IDC_SEL_BAUT)->EnableWindow(FALSE);
	GetDlgItem(IDC_SEL_DATA)->EnableWindow(FALSE);
	GetDlgItem(IDC_SEL_STOP)->EnableWindow(FALSE);
	GetDlgItem(IDC_SEL_PARITY)->EnableWindow(FALSE);
	GetDlgItem(IDC_SEND)->EnableWindow(TRUE);
}

void CSerialComDlg::GetComPara()
{
	m_ComNo=SendDlgItemMessage(IDC_SEL_COM,CB_GETCURSEL)+1;
	m_ComBaut=GetDlgItemInt(IDC_SEL_BAUT);
	m_ComData=SendDlgItemMessage(IDC_SEL_DATA,CB_GETCURSEL)+6;
	m_ComStop=SendDlgItemMessage(IDC_SEL_STOP,CB_GETCURSEL);
	m_ComParity=SendDlgItemMessage(IDC_SEL_PARITY,CB_GETCURSEL);
}

//===========================================================
// 功能：打开资源
// 参数：ComNo：串口号，Baut：波特率，Data：数据位，Stop：停止位
//       Parity：校验位，cbInBuf：输入队列大小，cbOutBuf：输出队列大小
//===========================================================
void CSerialComDlg::OnOpencom() 
{
	GetComPara();
	if(m_PubSub->m_hCom==INVALID_HANDLE_VALUE)
	{
		if(m_PubSub->OpenCom(m_ComNo,m_ComBaut,m_ComData,m_ComStop,m_ComParity,1024,1024))
		{
			SetDlgItemText(IDC_OPENCOM,"关闭");
			SetDlgItemText(IDC_DISP,"串口打开成功!");
			DisableCom();
		}
		else
			SetDlgItemText(IDC_DISP,"串口打开失败!");
	}
	else
	{
		if(m_PubSub->CloseCom())
		{
			SetDlgItemText(IDC_OPENCOM,"打开");
			SetDlgItemText(IDC_DISP,"串口关闭成功!");
			EnableCom();
		}
		else
			SetDlgItemText(IDC_DISP,"串口关闭失败!");
	}
}

//发送数据
void CSerialComDlg::OnSend() 
{
	CString SendText;
	BYTE SendBuf[1024];
	GetDlgItemText(IDC_EDIT_SEND,SendText);
	int len=SendText.GetLength();
	if(len==0)
	{
		SetDlgItemText(IDC_DISP,"请输入发送数据!");
		return;
	}
	for(int i=0;i<len;i++)
		SendBuf[i]=SendText.GetAt(i);
	if(m_PubSub->WriteCom(SendBuf,len))
		SetDlgItemText(IDC_DISP,"数据发送成功!");
	else
		SetDlgItemText(IDC_DISP,"数据发送失败!");
}

//接收数据
void CSerialComDlg::OnTimer(UINT nIDEvent)
{
	BYTE RecBuf[1024];
	CString RecText;
	int Rlen,oldLen;

	if(nIDEvent==100)
	{
		if(m_PubSub->m_hCom!=INVALID_HANDLE_VALUE)
		{
			Rlen=m_PubSub->GetDataLen();
			if(Rlen>0)
			{
				m_PubSub->ReadCom(RecBuf,Rlen);
				GetDlgItemText(IDC_EDIT_REC,RecText);
				oldLen=RecText.GetLength();
				for(int i=0;i<Rlen;i++)
				{
					if(RecBuf[i]<32)
						RecText.Insert(oldLen+i,'.');
					else
						RecText.Insert(oldLen+i,RecBuf[i]);
				}
				SetDlgItemText(IDC_EDIT_REC,RecText);
			}
		}
	}
	CDialog::OnTimer(nIDEvent);
}

//清空发送数据
void CSerialComDlg::OnClrSend()
{
	SetDlgItemText(IDC_EDIT_SEND,"");
}

//清空接收数据
void CSerialComDlg::OnClrRec() 
{
	SetDlgItemText(IDC_EDIT_REC,"");
}

//程序退出处理
void CSerialComDlg::ExitPrg()
{
	if(m_PubSub->m_hCom!=INVALID_HANDLE_VALUE)
		m_PubSub->CloseCom();
	
	if(m_PubSub!=NULL)
	{
		delete m_PubSub;
		m_PubSub=NULL;
	}
}

void CSerialComDlg::OnOK() 
{
	ExitPrg();
	CDialog::OnOK();
}

void CSerialComDlg::OnClose() 
{
	ExitPrg();
	CDialog::OnClose();
}
