// SerialComDlg.h : header file
//

#if !defined(AFX_SERIALCOMDLG_H__DA469BE6_5318_11D7_9960_00C0DF246602__INCLUDED_)
#define AFX_SERIALCOMDLG_H__DA469BE6_5318_11D7_9960_00C0DF246602__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



/////////////////////////////////////////////////////////////////////////////
// CSerialComDlg dialog

class CSerialComDlg : public CDialog
{
// Construction
public:
	void ExitPrg();
	void DisableCom();
	void EnableCom();
	void GetComPara();
	void ComDlgInit();
	int m_ComNo;
	int m_ComBaut;
	int m_ComData;
	int m_ComStop;
	int m_ComParity;
	HANDLE m_hCom;
	CSerialComDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSerialComDlg)
	enum { IDD = IDD_SERIALCOM_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialComDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSerialComDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnOpencom();
	afx_msg void OnSend();
	virtual void OnOK();
	afx_msg void OnClose();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClrSend();
	afx_msg void OnClrRec();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALCOMDLG_H__DA469BE6_5318_11D7_9960_00C0DF246602__INCLUDED_)
