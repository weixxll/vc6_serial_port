// serialcomDoc.h : interface of the CSerialcomDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERIALCOMDOC_H__76527A1B_233F_49ED_B513_4D215D192940__INCLUDED_)
#define AFX_SERIALCOMDOC_H__76527A1B_233F_49ED_B513_4D215D192940__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSerialcomDoc : public CDocument
{
protected: // create from serialization only
	CSerialcomDoc();
	DECLARE_DYNCREATE(CSerialcomDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialcomDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSerialcomDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSerialcomDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALCOMDOC_H__76527A1B_233F_49ED_B513_4D215D192940__INCLUDED_)
