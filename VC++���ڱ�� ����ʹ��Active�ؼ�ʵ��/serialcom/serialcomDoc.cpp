// serialcomDoc.cpp : implementation of the CSerialcomDoc class
//

#include "stdafx.h"
#include "serialcom.h"

#include "serialcomDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSerialcomDoc

IMPLEMENT_DYNCREATE(CSerialcomDoc, CDocument)

BEGIN_MESSAGE_MAP(CSerialcomDoc, CDocument)
	//{{AFX_MSG_MAP(CSerialcomDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerialcomDoc construction/destruction

CSerialcomDoc::CSerialcomDoc()
{
	// TODO: add one-time construction code here

}

CSerialcomDoc::~CSerialcomDoc()
{
}

BOOL CSerialcomDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSerialcomDoc serialization

void CSerialcomDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSerialcomDoc diagnostics

#ifdef _DEBUG
void CSerialcomDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSerialcomDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSerialcomDoc commands
