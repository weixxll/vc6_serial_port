// serialcom.h : main header file for the SERIALCOM application
//

#if !defined(AFX_SERIALCOM_H__1D67B1E9_8A07_4AD3_A103_B0C2C5A2E439__INCLUDED_)
#define AFX_SERIALCOM_H__1D67B1E9_8A07_4AD3_A103_B0C2C5A2E439__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CSerialcomApp:
// See serialcom.cpp for the implementation of this class
//

class CSerialcomApp : public CWinApp
{
public:
	CSerialcomApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialcomApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CSerialcomApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALCOM_H__1D67B1E9_8A07_4AD3_A103_B0C2C5A2E439__INCLUDED_)
