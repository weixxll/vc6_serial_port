// serialcomView.cpp : implementation of the CSerialcomView class
//

#include "stdafx.h"
#include "serialcom.h"

#include "serialcomDoc.h"
#include "serialcomView.h"
#include "MainFrm.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSerialcomView

IMPLEMENT_DYNCREATE(CSerialcomView, CView)

BEGIN_MESSAGE_MAP(CSerialcomView, CView)
	//{{AFX_MSG_MAP(CSerialcomView)
	ON_COMMAND(ID_Serial_SendData, OnSerialSendData)
	ON_COMMAND(ID_SetSerialCom, OnSetSerialCom)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
	ON_MESSAGE(ON_COM_RECEIVE,OnCommRecv)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerialcomView construction/destruction

CSerialcomView::CSerialcomView()
{
	// TODO: add construction code here

}

CSerialcomView::~CSerialcomView()
{
}

BOOL CSerialcomView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSerialcomView drawing

void CSerialcomView::OnDraw(CDC* pDC)
{
	CSerialcomDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	char a[7];
	a[0]='1';
	a[1]='2';
	a[2]='3';
	a[3]=13; //回车换行
	a[4]=10;
	a[5]='\0';
	CString temp;
	temp+=a;
	pDC->TextOut(100,100,temp);
	pDC->TextOut(50,50,m_comrecestr);
}

/////////////////////////////////////////////////////////////////////////////
// CSerialcomView printing

BOOL CSerialcomView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CSerialcomView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CSerialcomView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CSerialcomView diagnostics

#ifdef _DEBUG
void CSerialcomView::AssertValid() const
{
	CView::AssertValid();
}

void CSerialcomView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CSerialcomDoc* CSerialcomView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSerialcomDoc)));
	return (CSerialcomDoc*)m_pDocument;
}
#endif //_DEBUG
void CSerialcomView::OnCommRecv(WPARAM wParam, LPARAM lParam)  //中断消息响应
{
	//读取串口上的字符
	char str[100];
	com.ReadString(str, 100);//函数返回带结束符的str
	m_comrecestr += str;//此时str没有结束符
	Invalidate(true);
}
/////////////////////////////////////////////////////////////////////////////
// CSerialcomView message handlers

void CSerialcomView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	bool bopencom=com.Open(1); //打开串口1并使用默认设置   "19200, 8, n, 1"
//	com.SetWnd(AfxGetMainWnd()->m_hWnd); //设置消息处理窗口
	com.SetWnd(this->m_hWnd); //设置消息处理窗口
// 	int baudrate,verifybyte,databyte,stopbyte;
// 	baudrate=19200;
// 	verifybyte=0;
// 	databyte=8;
// 	stopbyte=1;
	//支持设置字符串 "9600, 8, n, 1"
// 	int i1,i2,i3;
// 	CString temp;
// 	char*state=new char;
// 	temp.Format("%d",baudrate);
// 	for (i1=0;i1<temp.GetLength();i1++)
// 	{
// 		state[i1]=temp[i1];
// 	}
// 	state[i1]=',';
// //	state[i1+1]=''
// 
//  	temp.Format("%d",databyte);
// 	for (i2=0;i2<temp.GetLength();i2++)
// 	{
// 		state[i1+1+i2]=temp[i2];
// 	}
// 	state[i1+1+i2]=',';
// 	state[i1+2+i2]='n';
// 	state[i1+3+i2]=',';
// 
//  	temp.Format("%d",stopbyte);
// 	for (i3=0;i3<temp.GetLength();i3++)
// 	{
// 		state[i1+4+i2+i3]=temp[i3];
// 	}
// 	state[i1+5+i2+i3]='\0';
// 	bool bstate=com.SetState(state);

// 	CString temp,state;
// 	temp.Format("%d",baudrate);
// 	state+=temp;
// 	state+=",";
// 	temp.Format("%d",databyte);
// 	state+=temp;
// 	state+=",";
// 	state+="n";//需修改
// 	state+=",";
// 	temp.Format("%d",stopbyte);
// 	state+=temp;
// 
// 	int i=_ttoi(state.GetBuffer(0));
// 	char *s1=(LPSTR)(LPCTSTR)state ; 
// 	bool bstate=com.SetState(s1);
}

void CSerialcomView::OnSerialSendData() 
{
	// TODO: Add your command handler code here
//	m_senddata="1234";
	m_senddata[0]='0';
	m_senddata[1]='1';
	m_senddata[2]='2';
	m_senddata[3]='3';
//	char t='9';
	DWORD writeformat=com.Write(m_senddata,4); //发送字符串
}

void CSerialcomView::OnSetSerialCom() 
{
	// TODO: Add your command handler code here
	bool bopencom=com.Open(1); //打开串口1并使用默认设置   "19200, 8, n, 1"
	HWND hwnd1=AfxGetMainWnd()->m_hWnd;
	HWND hwnd2=this->m_hWnd;
	com.SetWnd(AfxGetMainWnd()->m_hWnd); //设置消息处理窗口
//	com.SetWnd(this->m_hWnd); //设置消息处理窗口

}
