// serialcomView.h : interface of the CSerialcomView class
//
#include"cnComm.h"
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERIALCOMVIEW_H__361099BF_9DF4_4009_AEEA_DBF4BD6155B5__INCLUDED_)
#define AFX_SERIALCOMVIEW_H__361099BF_9DF4_4009_AEEA_DBF4BD6155B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSerialcomView : public CView
{
protected: // create from serialization only
	CSerialcomView();
	DECLARE_DYNCREATE(CSerialcomView)

// Attributes
public:
	CSerialcomDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialcomView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL


// Implementation
public:
	virtual ~CSerialcomView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSerialcomView)
	afx_msg void OnCommRecv(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSerialSendData();
	afx_msg void OnSetSerialCom();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	cnComm com;
	CString m_comrecestr;
//	char*m_senddata;
	char m_senddata[4];
};

#ifndef _DEBUG  // debug version in serialcomView.cpp
inline CSerialcomDoc* CSerialcomView::GetDocument()
   { return (CSerialcomDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALCOMVIEW_H__361099BF_9DF4_4009_AEEA_DBF4BD6155B5__INCLUDED_)
