// SerialCheckView.cpp : implementation of the CSerialCheckView class
//

#include "stdafx.h"
#include "SerialCheck.h"

#include "SerialCheckDoc.h"
#include "SerialCheckView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSerialCheckView

IMPLEMENT_DYNCREATE(CSerialCheckView, CFormView)

BEGIN_EVENTSINK_MAP(CSerialCheckView, CFormView)
	ON_EVENT(CSerialCheckView, ID_COMMCTRL, 1, OnCommMscomm, VTS_NONE)
END_EVENTSINK_MAP()

BEGIN_MESSAGE_MAP(CSerialCheckView, CFormView)
	//{{AFX_MSG_MAP(CSerialCheckView)
	ON_BN_CLICKED(IDC_CHANGE, OnChange)
	ON_BN_CLICKED(IDC_AUTO, OnAuto)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerialCheckView construction/destruction

CSerialCheckView::CSerialCheckView()
	: CFormView(CSerialCheckView::IDD)
{
	//{{AFX_DATA_INIT(CSerialCheckView)
	m_strReceiveChar = _T("");
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	bValid = FALSE;
	N = 0;
}

CSerialCheckView::~CSerialCheckView()
{
}

void CSerialCheckView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSerialCheckView)
	DDX_Control(pDX, IDC_AUTO, m_ctrlAuto);
	DDX_Control(pDX, IDC_CHANGE, m_ctrlChange);
	DDX_Text(pDX, IDC_RESULT, m_strReceiveChar);
	//}}AFX_DATA_MAP
}

BOOL CSerialCheckView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CSerialCheckView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	DWORD style=WS_VISIBLE|WS_CHILD;
	if (!m_Com.Create(NULL,style,CRect(0,0,0,0),this,ID_COMMCTRL))
	{
		TRACE0("Failed to create OLE Communications Control\r\n");
	    return; //fail to create　　
	}
	
	// TODO: Add extra initialization here
	m_Com.SetCommPort(1); //选择COM1
	m_Com.SetInBufferSize(1024); //设置输入缓冲区的大小，Bytes
	m_Com.SetOutBufferSize(512); //设置输入缓冲区的大小，Bytes
	m_Com.SetRThreshold(1); 
	m_Com.SetInputMode(1);

	m_Com.SetSettings("9600,n,8,1");

	if(!m_Com.GetPortOpen()) //打开串口   
	{
		m_Com.SetPortOpen(TRUE);
	}
	else
		AfxMessageBox("串口1已被占用，请选择其他串口");

}

/////////////////////////////////////////////////////////////////////////////
// CSerialCheckView printing

BOOL CSerialCheckView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CSerialCheckView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CSerialCheckView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CSerialCheckView::OnPrint(CDC* pDC, CPrintInfo* /*pInfo*/)
{
	// TODO: add customized printing code here
}

/////////////////////////////////////////////////////////////////////////////
// CSerialCheckView diagnostics

#ifdef _DEBUG
void CSerialCheckView::AssertValid() const
{
	CFormView::AssertValid();
}

void CSerialCheckView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CSerialCheckDoc* CSerialCheckView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSerialCheckDoc)));
	return (CSerialCheckDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSerialCheckView message handlers
//MSComm控件的OnComm事件处理过程。和其他的OnComm处理过程一样，这里也采用分支事 件//方式，图过是接收时间，则显示到接收显示文本框中，这些信息包括本机Modem返回信息，以及远程计算//机传送的数据。
void CSerialCheckView::OnCommMscomm()
{
    int k, nEvent, i;
	
    nEvent = m_Com.GetCommEvent();

	switch(nEvent)
	{
	case 2:  //收到大于RTHresshold个字符
		switch(N)
		{
		case 1:
			OutputResult("bps = 9600, data = 8:");
			break;
		case 2:
			OutputResult("bps = 9600, data = 7:");
			break;
		case 3:
			OutputResult("bps = 9600, data = 6:");
			break;
		case 4:
			OutputResult("bps = 9600, data = 5:");
			break;
		case 5:
			OutputResult("bps = 4800, data = 8:");
			break;
		case 6:
			OutputResult("bps = 4800, data = 7:");
			break;
		case 7:
			OutputResult("bps = 4800, data = 6:");
			break;
		case 8:
			OutputResult("bps = 4800, data = 5:");
			break;
		case 9:
			OutputResult("bps = 2400, data = 8:");
			break;
		case 10:
			OutputResult("bps = 2400, data = 7:");
			break;
		case 11:
			OutputResult("bps = 2400, data = 6:");
			break;
		case 12:
			OutputResult("bps = 2400, data = 5:");
			break;
		case 13:
			OutputResult("bps = 1200, data = 8:");
			break;
		case 14:
			OutputResult("bps = 1200, data = 7:");
			break;
		case 15:
			OutputResult("bps = 1200, data = 6:");
			break;
		case 16:
			OutputResult("bps = 1200, data = 5:");
			break;
		}
		break;
	case 3:  //CTS线状态发生了变化
		break;
	case 4:  //DSR线状态发生了变化
		break;
	case 5:  //CD线状态发生了变化
		break;
	case 6:  //Ring Indicator发生变化
		break;
	}    

	
	
	UpdateData(FALSE);
}

void CSerialCheckView::OutputResult(CString strTemp)
{
	VARIANT variant_inp;
	COleSafeArray safearray_inp;
	LONG len,k;
	BYTE rxdata[2048]; //设置BYTE数组 An 8-bit integerthat is not signed.
	CString strtemp, str = _T("");

	
	variant_inp = m_Com.GetInput();   //读缓冲区
	safearray_inp = variant_inp;  //VARIANT型变量转换为ColeSafeArray型变量
	len=safearray_inp.GetOneDimSize(); //得到有效数据长度
	for(k=0;k<len;k++)
		safearray_inp.GetElement(&k,rxdata+k);//转换为BYTE型数组
	for(k=0;k<len;k++)             //将数组转换为Cstring型变量
	{
		BYTE bt=*(char*)(rxdata+k);      //字符型
							
		strtemp.Format("%c",bt); //将字符送入临时变量strtemp存放

		str = str + strtemp;    //加入接收编辑框对应字符串    
	}
		
	m_strReceiveChar = m_strReceiveChar + strTemp + " 输入字符 " + str + "\r\n";
}

void CSerialCheckView::OnChange() 
{
	// TODO: Add your control notification handler code here
	if(m_Com.GetPortOpen()) //打开串口   
		m_Com.SetPortOpen(FALSE);	
	
	m_Com.SetCommPort(1); //选择COM1
	m_Com.SetInBufferSize(1024); //设置输入缓冲区的大小，Bytes
	m_Com.SetOutBufferSize(512); //设置输入缓冲区的大小，Bytes
	m_Com.SetRThreshold(1); 
	m_Com.SetInputMode(1);

	N++;

	switch(N)
	{
	case 1:
		m_Com.SetSettings("9600,n,8,1");
		break;
	case 2:
		m_Com.SetSettings("9600,n,7,1");
		break;
	case 3:
		m_Com.SetSettings("9600,n,6,1");
		break;
	case 4:
		m_Com.SetSettings("9600,n,5,1");
		break;
	case 5:
		m_Com.SetSettings("4800,n,8,1");
		break;
	case 6:
		m_Com.SetSettings("4800,n,7,1");
		break;
	case 7:
		m_Com.SetSettings("4800,n,6,1");
		break;
	case 8:
		m_Com.SetSettings("4800,n,5,1");
		break;
	case 9:
		m_Com.SetSettings("2400,n,8,1");
		break;
	case 10:
		m_Com.SetSettings("2400,n,7,1");
		break;
	case 11:
		m_Com.SetSettings("2400,n,6,1");
		break;
	case 12:
		m_Com.SetSettings("2400,n,5,1");
		break;
	case 13:
		m_Com.SetSettings("1200,n,8,1");
		break;
	case 14:
		m_Com.SetSettings("1200,n,7,1");
		break;
	case 15:
		m_Com.SetSettings("1200,n,6,1");
		break;
	case 16:
		m_Com.SetSettings("1200,n,5,1");
		break;
	case 17:
		N = 0;
		m_strReceiveChar = "";
		break;
	}	

	m_Com.SetPortOpen(TRUE);
	UpdateData(FALSE);
		
	CString strTemp;
	if(N == 0)
		strTemp.Format("人工更改配置");
	else
		strTemp.Format("人工更改(模式%d)", N);
	m_ctrlChange.SetWindowText(strTemp);	
}

void CSerialCheckView::OnAuto() 
{
	// TODO: Add your control notification handler code here
	N = 0;
	bValid = !bValid;
	m_strReceiveChar = "";
	UpdateData(FALSE);
	
	if(bValid)
	{
		m_ctrlAuto.SetWindowText("停止自动");
		SetTimer(1, 5000, NULL);
	}
	else
	{
		m_ctrlAuto.SetWindowText("自动模式(1s)");
		KillTimer(1);
	}		
	
	m_ctrlChange.SetWindowText("人工更改配置");	
}

void CSerialCheckView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(m_Com.GetPortOpen()) //打开串口   
		m_Com.SetPortOpen(FALSE);	
	
	m_Com.SetCommPort(1); //选择COM1
	m_Com.SetInBufferSize(1024); //设置输入缓冲区的大小，Bytes
	m_Com.SetOutBufferSize(512); //设置输入缓冲区的大小，Bytes
	m_Com.SetRThreshold(1); 
	m_Com.SetInputMode(1);


	N++;	
	switch(N)
	{
	case 1:
		m_Com.SetSettings("9600,n,8,1");
		break;
	case 2:
		m_Com.SetSettings("9600,n,7,1");
		break;
	case 3:
		m_Com.SetSettings("9600,n,6,1");
		break;
	case 4:
		m_Com.SetSettings("9600,n,5,1");
		break;
	case 5:
		m_Com.SetSettings("4800,n,8,1");
		break;
	case 6:
		m_Com.SetSettings("4800,n,7,1");
		break;
	case 7:
		m_Com.SetSettings("4800,n,6,1");
		break;
	case 8:
		m_Com.SetSettings("4800,n,5,1");
		break;
	case 9:
		m_Com.SetSettings("2400,n,8,1");
		break;
	case 10:
		m_Com.SetSettings("2400,n,7,1");
		break;
	case 11:
		m_Com.SetSettings("2400,n,6,1");
		break;
	case 12:
		m_Com.SetSettings("2400,n,5,1");
		break;
	case 13:
		m_Com.SetSettings("1200,n,8,1");
		break;
	case 14:
		m_Com.SetSettings("1200,n,7,1");
		break;
	case 15:
		m_Com.SetSettings("1200,n,6,1");
		break;
	case 16:
		m_Com.SetSettings("1200,n,5,1");
		break;
	case 17:		
		N = 0;
		break;
	}	

	m_Com.SetPortOpen(TRUE);

	CString strTemp;
	strTemp.Format("停止自动(模式%d)", N);
	m_ctrlAuto.SetWindowText(strTemp);

	CFormView::OnTimer(nIDEvent);
}
