//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SerialCheck.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_SERIALCHECK_FORM            101
#define IDR_MAINFRAME                   128
#define IDR_SERIALTYPE                  129
#define IDC_RESULT                      1000
#define ID_COMMCTRL                     1001
#define IDC_CHANGE                      1002
#define IDC_AUTO                        1003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
