; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSerialCheckView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SerialCheck.h"
LastPage=0

ClassCount=5
Class1=CSerialCheckApp
Class2=CSerialCheckDoc
Class3=CSerialCheckView
Class4=CMainFrame

ResourceCount=7
Resource1=IDR_MAINFRAME
Resource2=IDD_ABOUTBOX
Class5=CAboutDlg
Resource7=IDD_SERIALCHECK_FORM

[CLS:CSerialCheckApp]
Type=0
HeaderFile=SerialCheck.h
ImplementationFile=SerialCheck.cpp
Filter=N

[CLS:CSerialCheckDoc]
Type=0
HeaderFile=SerialCheckDoc.h
ImplementationFile=SerialCheckDoc.cpp
Filter=N

[CLS:CSerialCheckView]
Type=0
HeaderFile=SerialCheckView.h
ImplementationFile=SerialCheckView.cpp
Filter=D
LastObject=CSerialCheckView
BaseClass=CFormView
VirtualFilter=VWC


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T




[CLS:CAboutDlg]
Type=0
HeaderFile=SerialCheck.cpp
ImplementationFile=SerialCheck.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_MRU_FILE1
Command9=ID_APP_EXIT
Command10=ID_EDIT_UNDO
Command11=ID_EDIT_CUT
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_VIEW_TOOLBAR
Command15=ID_VIEW_STATUS_BAR
Command16=ID_APP_ABOUT
CommandCount=16

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[DLG:IDD_SERIALCHECK_FORM]
Type=1
Class=CSerialCheckView
ControlCount=5
Control1=IDC_RESULT,edit,1350635652
Control2=IDC_STATIC,button,1342177287
Control3=ID_COMMCTRL,{648A5600-2C6E-101B-82B6-000000000014},1342242816
Control4=IDC_CHANGE,button,1342242816
Control5=IDC_AUTO,button,1342242816

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

