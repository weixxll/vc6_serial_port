// SerialCheckDoc.h : interface of the CSerialCheckDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERIALCHECKDOC_H__48C6865F_4460_4A7B_A2D4_B6BF66FF800A__INCLUDED_)
#define AFX_SERIALCHECKDOC_H__48C6865F_4460_4A7B_A2D4_B6BF66FF800A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSerialCheckDoc : public CDocument
{
protected: // create from serialization only
	CSerialCheckDoc();
	DECLARE_DYNCREATE(CSerialCheckDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialCheckDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSerialCheckDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSerialCheckDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALCHECKDOC_H__48C6865F_4460_4A7B_A2D4_B6BF66FF800A__INCLUDED_)
