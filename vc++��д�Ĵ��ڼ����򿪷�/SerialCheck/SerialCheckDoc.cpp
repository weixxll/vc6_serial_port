// SerialCheckDoc.cpp : implementation of the CSerialCheckDoc class
//

#include "stdafx.h"
#include "SerialCheck.h"

#include "SerialCheckDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSerialCheckDoc

IMPLEMENT_DYNCREATE(CSerialCheckDoc, CDocument)

BEGIN_MESSAGE_MAP(CSerialCheckDoc, CDocument)
	//{{AFX_MSG_MAP(CSerialCheckDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerialCheckDoc construction/destruction

CSerialCheckDoc::CSerialCheckDoc()
{
	// TODO: add one-time construction code here

}

CSerialCheckDoc::~CSerialCheckDoc()
{
}

BOOL CSerialCheckDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSerialCheckDoc serialization

void CSerialCheckDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSerialCheckDoc diagnostics

#ifdef _DEBUG
void CSerialCheckDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSerialCheckDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSerialCheckDoc commands
