// SerialCheckView.h : interface of the CSerialCheckView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERIALCHECKVIEW_H__81C96336_876E_4CA4_90FB_60E4453C514E__INCLUDED_)
#define AFX_SERIALCHECKVIEW_H__81C96336_876E_4CA4_90FB_60E4453C514E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "mscomm.h"

class CSerialCheckView : public CFormView
{
protected: // create from serialization only
	BOOL bValid;
	CSerialCheckView();
	DECLARE_DYNCREATE(CSerialCheckView)

public:
	//{{AFX_DATA(CSerialCheckView)
	enum { IDD = IDD_SERIALCHECK_FORM };
	CButton	m_ctrlAuto;
	CButton	m_ctrlChange;
	CString	m_strReceiveChar;
	//}}AFX_DATA

// Attributes
public:
	int N;
	CMSComm m_Com;
	void OutputResult(CString str);
	CSerialCheckDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialCheckView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSerialCheckView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	afx_msg void OnCommMscomm();
    DECLARE_EVENTSINK_MAP()

// Generated message map functions
protected:
	//{{AFX_MSG(CSerialCheckView)
	afx_msg void OnChange();
	afx_msg void OnAuto();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in SerialCheckView.cpp
inline CSerialCheckDoc* CSerialCheckView::GetDocument()
   { return (CSerialCheckDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALCHECKVIEW_H__81C96336_876E_4CA4_90FB_60E4453C514E__INCLUDED_)
